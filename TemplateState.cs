using System;
using UnityEngine;

public class STATENAME : IState<GCSolar> {
    private static STATENAME instance;
    StateNotifications notifTexts;
    float stateEntryTime;
    int stateNum = 0;


    private STATENAME(int num) {
        stateNum = num;
    }

    internal static STATENAME GetInstance(int num) {
        if (instance == null) {
            instance = new STATENAME(num);
        }

        return instance;
    }

    public void StateEnter(GCSolar context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Solar, context.loc);


    }

    public void StateExit(GCSolar context) {
        string msg = notifTexts.STATE_EXIT_FI;
        context.uiController.AddNotification(msg);

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCSolar context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            context.uiController.AddNotification(notifTexts.STATE_ENTRY_FI);
            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCSolar context, int num) {
        int stateProgress = num - stateNum;

    }
}

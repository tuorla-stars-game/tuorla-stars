﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataHandler : MonoBehaviour {
    public static DataHandler instance;

    HashSet<int> tuorlaCardsFound;
    HashSet<int> planetCardsFound;
    HashSet<int> miscCardsFound;
    HashSet<int> telescopeCardsFound;
    bool[] telescopePartsFound;
    bool[] tutorialsPlayed;

    internal LocalizedSharedStrings locSharedStrings;

    internal delegate void AfterSave();
    internal static event AfterSave OnSaveReady;

    internal delegate void AfterAddCard();
    internal event AfterAddCard OnCardAddReady;

    internal delegate void AfterAddPart();
    internal event AfterAddPart OnPartAddReady;

    internal delegate void AfterDelete();
    internal event AfterDelete OnDeleteReady;

    int numTuorlaCards = 0;
    int numPlanetCards = 0;
    int numMiscCards = 0;
    int numTelescopeCards = 0;

    string lastSceneName = "GameScene";
    string initials = "";
    bool isTeacher = false;
    bool isSolarUnlocked = false;
    int solarProgress = 0;
    Loc loc = Loc.English;

    float musicVol = 0.5f;
    float sfxVol = 0.5f;

    internal string LastSceneName {
        get {
            return lastSceneName;
        }
        set {
            lastSceneName = value;
        }
    }

    internal string Initials {
        get {
            return initials;
        }
        set {
            initials = value;
        }
    }

    internal bool IsTeacher {
        get {
            return isTeacher;
        }
        set {
            isTeacher = value;
        }
    }

    internal Loc Localization {
        get {
            return loc;
        }
        set {
            loc = value;
            locSharedStrings = LocalizationDataLoader.LoadLocalizedSharedData(loc);
        }
    }

    internal bool IsSolarGameUnlocked {
        get {
            return isSolarUnlocked;
        }
        set {
            if (value != isSolarUnlocked) {
                isSolarUnlocked = value;

                for (int i = 0; i < numPlanetCards; i++) {
                    AddCard(CardLibrary.planetsString, i + 1);
                }

                SaveData();
            }
        }
    }

    internal int SolarGameProgress {
        get {
            return solarProgress;
        }
        set {
            if (value > solarProgress) {
                solarProgress = value;
                SaveData();
            }
        }
    }

    internal float MusicVolume {
        get {
            return musicVol;
        }
        set {
            musicVol = value;
        }
    }

    internal float SFXVolume {
        get {
            return sfxVol;
        }
        set {
            sfxVol = value;
        }
    }

    void Awake () {
        if (instance == null) {
            instance = this;

            tuorlaCardsFound = new HashSet<int>();
            planetCardsFound = new HashSet<int>();
            miscCardsFound = new HashSet<int>();
            telescopeCardsFound = new HashSet<int>();
            telescopePartsFound = new bool[5];
            tutorialsPlayed = new bool[4];

            LoadData();

            switch (Application.systemLanguage) {
                case SystemLanguage.Finnish:
                    loc = Loc.Finnish;
                    break;
                case SystemLanguage.Swedish:
                    loc = Loc.Swedish;
                    break;
                default:
                    loc = Loc.English;
                    break;
            }

            locSharedStrings = LocalizationDataLoader.LoadLocalizedSharedData(loc);

            DontDestroyOnLoad(gameObject);
        }
        else {
            Destroy(gameObject);
        }
	}

    void Start() {
        numTuorlaCards = CardLibrary.instance.GetNumCards(CardLibrary.tuorlaString);
        numPlanetCards = CardLibrary.instance.GetNumCards(CardLibrary.planetsString);
        numMiscCards = CardLibrary.instance.GetNumCards(CardLibrary.miscString);
        numTelescopeCards = CardLibrary.instance.GetNumCards(CardLibrary.telescopeString);
    }

    internal void SaveData() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;

        if (!File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
            file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        }
        else {
            file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
        }

        PlayerInfo pi = new PlayerInfo();
        pi.tuorlaCardsFound = new int[tuorlaCardsFound.Count];
        tuorlaCardsFound.CopyTo(pi.tuorlaCardsFound);
        pi.planetCardsFound = new int[planetCardsFound.Count];
        planetCardsFound.CopyTo(pi.planetCardsFound);
        pi.miscCardsFound = new int[miscCardsFound.Count];
        miscCardsFound.CopyTo(pi.miscCardsFound);
        pi.telescopeCardsFound = new int[telescopeCardsFound.Count];
        telescopeCardsFound.CopyTo(pi.telescopeCardsFound);
        pi.telescopePartsFound = telescopePartsFound;
        pi.tutorialsPlayed = tutorialsPlayed;
        pi.isSolarUnlocked = isSolarUnlocked;
        pi.solarProgress = solarProgress;

        bf.Serialize(file, pi);
        file.Close();

        if (OnSaveReady != null) {
            OnSaveReady();
        }
    }

    internal bool LoadData() {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

            PlayerInfo pi = (PlayerInfo)bf.Deserialize(file);
            bool nullFound = false;

            if (!pi.Equals(new PlayerInfo())) {
                if (pi.tuorlaCardsFound != null) {
                    tuorlaCardsFound = new HashSet<int>(pi.tuorlaCardsFound);
                }
                else {
                    nullFound = true;
                }

                if (pi.planetCardsFound != null) {
                    planetCardsFound = new HashSet<int>(pi.planetCardsFound);
                }
                else {
                    nullFound = true;
                }

                if (pi.miscCardsFound != null) {
                    miscCardsFound = new HashSet<int>(pi.miscCardsFound);
                }
                else {
                    nullFound = true;
                }

                if (pi.telescopeCardsFound != null) {
                    telescopeCardsFound = new HashSet<int>(pi.telescopeCardsFound);
                }
                else {
                    nullFound = true;
                }

                if (pi.telescopePartsFound != null) {
                    telescopePartsFound = pi.telescopePartsFound;
                }
                else {
                    nullFound = true;
                }

                if (pi.tutorialsPlayed != null) {
                    tutorialsPlayed = pi.tutorialsPlayed;
                }
                else {
                    nullFound = true;
                }

                isSolarUnlocked = pi.isSolarUnlocked;
                solarProgress = pi.solarProgress;
            }
            else {
                nullFound = true;
            }

            if (nullFound) {
                Debug.Log("Player save data corrupted. Deleting player save data.");
                File.Delete(Application.persistentDataPath + "/playerInfo.dat");
            }

            file.Close();

            return true;
        }
        return false;
    }

    internal void DeleteData() {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
            File.Delete(Application.persistentDataPath + "/playerInfo.dat");
        }

        tuorlaCardsFound.Clear();
        planetCardsFound.Clear();
        miscCardsFound.Clear();
        telescopeCardsFound.Clear();

        for (int i = 0; i < telescopePartsFound.Length; i++) {
            telescopePartsFound[i] = false;
        }

        for (int i = 0; i < tutorialsPlayed.Length; i++) {
            tutorialsPlayed[i] = false;
        }

        solarProgress = 0;
        isSolarUnlocked = false;

        if (OnDeleteReady != null) {
            OnDeleteReady();
        }
    }

    internal void AddCard(string category, int cardID) {
        bool addedCard = false;

        switch (category) {
            case CardLibrary.tuorlaString:
                if (!tuorlaCardsFound.Contains(cardID)) {
                    tuorlaCardsFound.Add(cardID);
                    addedCard = true;
                }
                break;
            case CardLibrary.planetsString:
                if (!planetCardsFound.Contains(cardID)) {
                    planetCardsFound.Add(cardID);
                    addedCard = true;
                }
                break;
            case CardLibrary.miscString:
                if (!miscCardsFound.Contains(cardID)) {
                    miscCardsFound.Add(cardID);
                    addedCard = true;
                }
                break;
            case CardLibrary.telescopeString:
                if (!telescopeCardsFound.Contains(cardID)) {
                    telescopeCardsFound.Add(cardID);
                    addedCard = true;
                }
                break;
        }

        if (OnCardAddReady != null && addedCard) {
            OnCardAddReady();
        }
    }

    internal bool HasCard(string category, int cardID) {
        switch (category) {
            case CardLibrary.tuorlaString:
                return tuorlaCardsFound.Contains(cardID);
            case CardLibrary.planetsString:
                return planetCardsFound.Contains(cardID);
            case CardLibrary.miscString:
                return miscCardsFound.Contains(cardID);
            case CardLibrary.telescopeString:
                return telescopeCardsFound.Contains(cardID);
            default:
                return false;
        }
    }

    internal void AddTelescopePart(int partNum) {
        telescopePartsFound[partNum - 2] = true;
        AddCard(CardLibrary.telescopeString, partNum);

        if (OnPartAddReady != null) {
            OnPartAddReady();
        }
    }

    internal void SetTutorialPlayed(Scenes scene) {
        Debug.Log("setting " + (int)scene + " tutorial status to played");
        Debug.Log(tutorialsPlayed);
        tutorialsPlayed[(int)scene] = true;
    }

    internal bool WasTutorialPlayed(Scenes scene) {
        Debug.Log("checking if " + (int)scene + " has been played");
        //Debug.Log(tutorialsPlayed);
        return tutorialsPlayed[(int)scene];
    }

    internal bool HasAllTelescopeParts() {
        return !Array.Exists<bool>(telescopePartsFound, x => x == false);
    }

    internal float GetProgress() {
        float progress = 0f;

        progress += tuorlaCardsFound.Count;
        progress += planetCardsFound.Count;
        progress += telescopeCardsFound.Count;

        progress = progress / (float)CardLibrary.instance.TotalNumberOfCardsWithoutMisc;

        return progress;
    }

    internal bool HasTelescopePart(int partNum) {
        return telescopePartsFound[partNum - 2];
    }

    public void CheatAddCards(string cat) {
        switch (cat) {
            case CardLibrary.tuorlaString:
                for (int i = 0; i < numTuorlaCards; i++) {
                    tuorlaCardsFound.Add(i + 1);
                }
                break;
            case CardLibrary.planetsString:
                for (int i = 0; i < numPlanetCards; i++) {
                    planetCardsFound.Add(i + 1);
                }
                break;
            case CardLibrary.miscString:
                for (int i = 0; i < numMiscCards; i++) {
                    miscCardsFound.Add(i + 1);
                }
                break;
            case CardLibrary.telescopeString:
                for (int i = 0; i < numTelescopeCards; i++) {
                    telescopeCardsFound.Add(i + 1);
                }
                break;
        }

        if (OnCardAddReady != null) {
            OnCardAddReady();
        }
    }
}

enum Scenes {
    Map,
    Card,
    Telescope,
    Solar
}

[Serializable]
public struct PlayerInfo {
    public int[] tuorlaCardsFound;
    public int[] planetCardsFound;
    public int[] miscCardsFound;
    public int[] telescopeCardsFound;
    public bool[] telescopePartsFound;
    public bool[] tutorialsPlayed;
    public bool isSolarUnlocked;
    public int solarProgress;
    public float musicVol;
    public float sfxVol;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewNotificationsController : MonoBehaviour {
    [SerializeField]
    float notificationLifetime = 4f;

    [SerializeField]
    NewNotification mainNotif;

    float timeSinceLatestNotif = 0f;

    Queue<string> notificationQueue = new Queue<string>();
    List<NewNotification> notifications = new List<NewNotification>();

    internal float NotificationLifetime {
        get {
            return notificationLifetime;
        }
        set {
            notificationLifetime = value;
        }
    }

    void Update() {
        int notAmount = notifications.Count;

        if (notAmount > 0) {
            if (Time.realtimeSinceStartup - timeSinceLatestNotif > notificationLifetime) {
                string queued = notificationQueue.Dequeue();
                mainNotif.Data = queued;
            }
        }
    }

    internal void AddNotification(string text) {
        notificationQueue.Enqueue(text);
    }

    internal void AddNotification(object obj) {
        string serializedObject = obj.ToString();

        if (serializedObject == null) {
            serializedObject = "Object could not be serialized.";
        }

        AddNotification(serializedObject);
    }
}

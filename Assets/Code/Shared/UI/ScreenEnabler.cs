﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenEnabler : MonoBehaviour {
    internal bool isVisible = false;
    bool destroyAfterHide = false;
    float targetAlpha = 0f;
    CanvasGroup myCG;

    [SerializeField]
    float fadeSpeed = 1f;
    [SerializeField]
    GameObject[] alsoEnable;

    void Awake() {
        myCG = GetComponent<CanvasGroup>();
    }

    void OnEnable() {
        if (isVisible) {
            targetAlpha = 1f;

            foreach (GameObject go in alsoEnable) {
                go.SetActive(isVisible);
            }
        }
        else {
            targetAlpha = 0f;
        }
    }

    void Update() {
        if (myCG.alpha != targetAlpha) {
            myCG.alpha = Mathf.MoveTowards(myCG.alpha, targetAlpha, Time.deltaTime * fadeSpeed);
        }
        else {
            enabled = false;
            foreach (GameObject go in alsoEnable) {
                go.SetActive(isVisible);
            }

            if (destroyAfterHide && !isVisible) {
                Destroy(gameObject);
            }
        }
    }

    public void ElementDisplay() {
        isVisible = true;
        enabled = true;
    }

    public void ElementHide(bool destroy) {
        GameController.instance.uiController.PlayButtonSFX();
        destroyAfterHide = destroy;
        isVisible = false;
        enabled = true;
    }
}

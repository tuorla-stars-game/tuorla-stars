﻿using UnityEngine;

public class HexMenuController : MonoBehaviour {
    GameObject[] menus;
    CanvasGroup[] menuCGs;

    bool fadeOut;
    int menuToFade = -1;
    float fadeSpeed = 1f;

    void Awake() {
        menus = new GameObject[transform.childCount];
        menuCGs = new CanvasGroup[transform.childCount];
        for (int i = 0; i < menus.Length; i++) {
            menus[i] = transform.GetChild(i).gameObject;

            menuCGs[i] = menus[i].GetComponent<CanvasGroup>();
            menus[i].SetActive(false);
        }
    }

    void Update() {
        if (menuToFade != -1) {
            if (!fadeOut && menuCGs[menuToFade].alpha < 1f) {
                menuCGs[menuToFade].alpha += fadeSpeed * Time.deltaTime;
            }
            else if (fadeOut && menuCGs[menuToFade].alpha > 0f) {
                menuCGs[menuToFade].alpha -= fadeSpeed * Time.deltaTime;

                if (menuCGs[menuToFade].alpha <= 0f) {
                    fadeOut = false;
                    menuToFade = -1;
                }
            }
        }
    }

    internal void ToggleMenu(int menuId) {
        menus[menuId - 1].SetActive(!menus[menuId - 1].activeSelf);
        menuToFade = menuId - 1;

        if (menus[menuId - 1].activeSelf) {
            menuCGs[menuId - 1].alpha += 0.1f;
            fadeOut = false;
        }
        else {
            fadeOut = true;
        }
    }
}

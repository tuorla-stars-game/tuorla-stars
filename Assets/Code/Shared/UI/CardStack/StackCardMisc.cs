﻿using UnityEngine;
using UnityEngine.UI;

public class StackCardMisc : StackCard {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;

    protected override void SubLoadCard(int cardID) {
        MiscCardLocalized myCard = library.GetMiscCardLocalizedByID(cardID);

        header.text = myCard.header;
        body.text = myCard.body;
    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }
}

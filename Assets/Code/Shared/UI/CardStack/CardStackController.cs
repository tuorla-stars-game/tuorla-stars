﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardStackController : MonoBehaviour {
    CardLibrary library;
    UIController uiController;
    int cardStackBuffer = 0;

    [SerializeField]
    GameObject tuorlaCard;

    [SerializeField]
    GameObject miscCard;

    [SerializeField]
    GameObject telescopeCard;

    [SerializeField]
    GameObject planetCard;

	void Start () {
        library = CardLibrary.instance;
        uiController = GetComponentInParent<UIController>();
        cardStackBuffer = transform.childCount;
	}

    void MakeCard(GameObject cardToMake, string cat, int ID, bool justDiscovered) {
        GameObject newCardObj = Instantiate(cardToMake.gameObject, transform);
        newCardObj.SetActive(true);
        newCardObj.transform.SetSiblingIndex(cardStackBuffer);
        StackCard newCard = newCardObj.GetComponent<StackCard>();

        if (newCard == null) {
            newCard = newCardObj.GetComponentInChildren<StackCard>();
        }

        newCard.enabled = true;
        newCard.LoadCard(cat, ID, justDiscovered);
    }

    internal void AddCardToStack(string cat, int ID, bool justDiscovered) {
        switch (cat) {
            case CardLibrary.tuorlaString:
                MakeCard(tuorlaCard, cat, ID, justDiscovered);
                break;
            case CardLibrary.miscString:
                MakeCard(miscCard, cat, ID, justDiscovered);
                break;
            case CardLibrary.telescopeString:
                MakeCard(telescopeCard, cat, ID, justDiscovered);
                break;
            case CardLibrary.planetsString:
                MakeCard(planetCard, cat, ID, justDiscovered);
                break;
            default:
                return;
        }

        if (!uiController.IsDimActive) {
            uiController.ToggleDim();
        }

        uiController.IsUIElementActive = true;
    }

    public void CloseCardOnTop() {
        int indexOnTop = transform.childCount - 1;

        if (indexOnTop == cardStackBuffer) {
            if (uiController.IsDimActive) {
                uiController.ToggleDim();
                uiController.IsUIElementActive = false;
            }
        }
        StackCard cardToClose = transform.GetChild(indexOnTop).GetComponent<StackCard>();

        if (cardToClose == null) {
            cardToClose = transform.GetChild(indexOnTop).GetComponentInChildren<StackCard>();
        }

        if (cardToClose.gameObject.activeSelf) {
            cardToClose.CloseCard();
        }
    }
}

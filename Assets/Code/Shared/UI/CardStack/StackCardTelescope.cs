﻿using UnityEngine;
using UnityEngine.UI;

public class StackCardTelescope : StackCard {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;
    [SerializeField]
    Image image;

    Sprite defaultSprite;

    protected override void SubLoadCard(int cardID) {
        TelescopeCardLocalized myCard = library.GetTelescopeCardLocalizedByID(cardID);

        header.text = myCard.header;
        body.text = myCard.body;

        if (myCard.imagePath != "") {
            image.sprite = Resources.Load<Sprite>(myCard.imagePath);
        }
        else {
            image.sprite = defaultSprite;
        }
    }

    protected override void SubStart() {
        defaultSprite = image.sprite;
    }

    protected override void SubUpdate() {

    }
}

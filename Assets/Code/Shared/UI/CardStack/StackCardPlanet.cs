﻿using UnityEngine;
using UnityEngine.UI;

public class StackCardPlanet : StackCard {
    [SerializeField]
    Text header;

    [SerializeField]
    RawImage planetSlot;

    [SerializeField]
    Text rotationPeriod;

    [SerializeField]
    Text orbitalPeriod;

    [SerializeField]
    Text diameter;

    [SerializeField]
    Text moons;

    [SerializeField]
    Text mass;

    [SerializeField]
    Text axialTilt;

    [SerializeField]
    Text body;

    [SerializeField]
    PlanetCardTemperatureController pcTempController;

    [SerializeField]
    PlanetCardOrderController pcOrderController;

    [SerializeField]
    PlanetAnimController pAnimController;

    protected override void SubLoadCard(int cardID) {
        PlanetCardLocalized thisData = library.GetPlanetCardLocalizedByID(cardID);

        header.text = thisData.header;
        body.text = thisData.body;

        rotationPeriod.text = thisData.rotationPeriod;
        orbitalPeriod.text = thisData.orbitalPeriod;
        diameter.text = thisData.diameter;
        moons.text = thisData.satellites.ToString();
        mass.text = thisData.mass;
        axialTilt.text = thisData.axialTilt;
        pcTempController.SetTempTexts(thisData.temperatureMin.ToString(),
                                      thisData.temperatureMax.ToString());

        pcTempController.SetTempGraphics(thisData.temperatureMin,
                                         thisData.temperatureMax);

        pcOrderController.ResetPlanetGraphic();
        pcOrderController.SetPlanetGraphic(thisData.planetOrder,
                                           thisData.distance);

        pAnimController.SetTextureAndRotation(thisData.imagePath, thisData.axialTiltNum);
    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }
}

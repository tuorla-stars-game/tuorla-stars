﻿using UnityEngine;
using UnityEngine.UI;

public class StackCardTuorla : StackCard {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;
    [SerializeField]
    Image image;

    protected override void SubLoadCard(int cardID) {
        //Debug.Log(library);
        TuorlaCardLocalized myCard = library.GetTuorlaCardLocalizedByID(cardID);
        Sprite defaultSprite = image.sprite;

        if (myCard.imagePath != "") {
            image.sprite = Resources.Load<Sprite>(myCard.imagePath);
        }
        else {
            image.sprite = defaultSprite;
        }

        header.text = myCard.header;
        body.text = myCard.body;
    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }
}

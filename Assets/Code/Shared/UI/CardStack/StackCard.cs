﻿using UnityEngine;

public abstract class StackCard : MonoBehaviour {
    protected string thisCategory = "";
    protected int thisID = 0;
    protected CardLibrary library;

    bool wasDiscovered = false;

    internal string CardCategory {
        get {
            return thisCategory;
        }
        set {
            thisCategory = value;
        }
    }

    internal int CardID {
        get {
            return thisID;
        }
        set {
            thisID = value;
        }
    }

	void Start () {
        //library = CardLibrary.instance;
        //Debug.Log("library assigned");

        SubStart();
	}

    void OnEnable() {
        library = CardLibrary.instance;
        //Debug.Log("library assigned");
    }

    protected abstract void SubStart();
	
	void Update () {

        SubUpdate();
	}

    protected abstract void SubUpdate();

    internal void LoadCard(string cat, int cardID, bool justDiscovered) {
        thisID = cardID;
        thisCategory = cat;
        wasDiscovered = justDiscovered;
        SubLoadCard(cardID);
    }

    internal void CloseCard() {
        //Debug.Log("Closing card (" + thisCategory + ", " + thisID + ") + to library");

        if (wasDiscovered) {
            //Debug.Log("Adding card (" + thisCategory + ", " + thisID + ") + to library");
            GameController.instance.AddCard(thisCategory, thisID);
        }

        if (transform.parent.gameObject.name != "CardStack") {
            Destroy(transform.parent.gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }

    protected abstract void SubLoadCard(int cardID);
}

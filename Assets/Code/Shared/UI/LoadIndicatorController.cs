﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadIndicatorController : MonoBehaviour {
    Image[] lines;

    int currentLine = 0;
    float lastLineChange;
    float timePerUpdate = 0.08f;
    float fadeSpeed = 2.5f;

    void Awake() {
        lines = new Image[6];

        for (int i = 1; i < 7; i++) {
            lines[i - 1] = transform.GetChild(i).GetComponent<Image>();
        }

        lastLineChange = 0f;
    }

    void Start() {
        ResetState();
        gameObject.SetActive(false);
    }

    void Update() {
        //lines[CalcPrevLine(currentLine)].GetComponent<Image>().color.a *= 0.8f;
        for (int i = 0; i < lines.Length; i++) {
            if (lines[i].color.a > 0f) {
                Color newColor = lines[i].color;
                newColor.a -= Time.deltaTime * fadeSpeed;
                lines[i].color = newColor;
            }
        }


        if (Time.realtimeSinceStartup - lastLineChange > timePerUpdate) {
            Color newColor = lines[currentLine].color;
            newColor.a = 1f;
            lines[currentLine].color = newColor;

            lastLineChange = Time.realtimeSinceStartup;
            currentLine++;

            if (currentLine > 5) {
                currentLine = 0;
            }
        }
    }

    int CalcPrevLine(int line) {
        if (line == 0) {
            return 5;
        }

        return line - 1;
    }

    void ResetState() {
        Color alpha = new Color(1f, 1f, 1f, 0f);
        foreach (Image line in lines) {
            line.color = alpha;
        }

        currentLine = 0;
        lastLineChange = Time.realtimeSinceStartup;
    }

    internal void Display() {
        ResetState();
        gameObject.SetActive(true);
    }
}

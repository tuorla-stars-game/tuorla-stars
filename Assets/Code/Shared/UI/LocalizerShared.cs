﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizerShared : MonoBehaviour {
    [SerializeField]
    Text TUTORIAL_CARD_LIBRARY;
    [SerializeField]
    Text TUTORIAL_MAIN_MENU;
    [SerializeField]
    Text TUTORIAL_NOTIFICATIONS;
    [SerializeField]
    Text TUTORIAL_CAMERA_RESET;
    [SerializeField]
    Text TUTORIAL_SCENE_MENU;

    [SerializeField]
    Text INFO_WINDOW_HEADER;
    [SerializeField]
    Text INFO_GAME_DESIGN;
    [SerializeField]
    Text INFO_GRAPHICS;
    [SerializeField]
    Text INFO_PROGRAMMING;
    [SerializeField]
    Text INFO_CONTENT_N_SOUND;
    [SerializeField]
    Text INFO_PRODUCER;
    [SerializeField]
    Text INFO_DIRECTOR;

    [SerializeField]
    Text SETTINGS_WINDOW_HEADER;
    [SerializeField]
    Text SETTINGS_VOLUME;
    [SerializeField]
    Text SETTINGS_VOLUME_MUSIC;
    [SerializeField]
    Text SETTINGS_VOLUME_SFX;
    [SerializeField]
    Text SETTINGS_LANGUAGE;
    [SerializeField]
    Text SETTINGS_GAME;
    [SerializeField]
    Text SETTINGS_GRAPHICS;
    [SerializeField]
    Text SETTINGS_GPS_RESET;
    [SerializeField]
    Text SETTINGS_GAME_RESET;

    void OnEnable() {
        LocalizedSharedStrings lss = DataHandler.instance.locSharedStrings;

        if (TUTORIAL_CARD_LIBRARY != null) {
            TUTORIAL_CARD_LIBRARY.text = lss.TUTORIAL_CARD_LIBRARY;
        }
        if (TUTORIAL_MAIN_MENU != null) {
            TUTORIAL_MAIN_MENU.text = lss.TUTORIAL_MAIN_MENU;
        }
        if (TUTORIAL_NOTIFICATIONS != null) {
            TUTORIAL_NOTIFICATIONS.text = lss.TUTORIAL_NOTIFICATIONS;
        }
        if (TUTORIAL_CAMERA_RESET != null) {
            TUTORIAL_CAMERA_RESET.text = lss.TUTORIAL_CAMERA_RESET;
        }
        if (TUTORIAL_SCENE_MENU != null) {
            TUTORIAL_SCENE_MENU.text = lss.TUTORIAL_SCENE_MENU;
        }


        if (INFO_WINDOW_HEADER != null) {
            INFO_WINDOW_HEADER.text = lss.INFO_WINDOW_HEADER;
        }
        if (INFO_GAME_DESIGN != null) {
            INFO_GAME_DESIGN.text = lss.INFO_GAME_DESIGN;
        }
        if (INFO_GRAPHICS != null) {
            INFO_GRAPHICS.text = lss.INFO_GRAPHICS;
        }
        if (INFO_PROGRAMMING != null) {
            INFO_PROGRAMMING.text = lss.INFO_PROGRAMMING;
        }
        if (INFO_CONTENT_N_SOUND != null) {
            INFO_CONTENT_N_SOUND.text = lss.INFO_CONTENT_N_SOUND;
        }
        if (INFO_PRODUCER != null) {
            INFO_PRODUCER.text = lss.INFO_PRODUCER;
        }
        if (INFO_DIRECTOR != null) {
            INFO_DIRECTOR.text = lss.INFO_DIRECTOR;
        }


        if (SETTINGS_WINDOW_HEADER != null) {
            SETTINGS_WINDOW_HEADER.text = lss.SETTINGS_WINDOW_HEADER;
        }
        if (SETTINGS_VOLUME != null) {
            SETTINGS_VOLUME.text = lss.SETTINGS_VOLUME;
        }
        if (SETTINGS_VOLUME_MUSIC != null) {
            SETTINGS_VOLUME_MUSIC.text = lss.SETTINGS_VOLUME_MUSIC;
        }
        if (SETTINGS_VOLUME_SFX != null) {
            SETTINGS_VOLUME_SFX.text = lss.SETTINGS_VOLUME_SFX;
        }
        if (SETTINGS_LANGUAGE != null) {
            SETTINGS_LANGUAGE.text = lss.SETTINGS_LANGUAGE;
        }
        if (SETTINGS_GAME != null) {
            SETTINGS_GAME.text = lss.SETTINGS_GAME;
        }
        if (SETTINGS_GRAPHICS != null) {
            SETTINGS_GRAPHICS.text = lss.SETTINGS_GRAPHICS;
        }
        if (SETTINGS_GPS_RESET != null) {
            SETTINGS_GPS_RESET.text = lss.SETTINGS_GPS_RESET;
        }
        if (SETTINGS_GAME_RESET != null) {
            SETTINGS_GAME_RESET.text = lss.SETTINGS_GAME_RESET;
        }

        enabled = false;
    }
}

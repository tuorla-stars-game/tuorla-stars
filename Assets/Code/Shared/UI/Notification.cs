﻿using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour {
    Text myText;
    Image myBG;

    Vector3 pos;
    string data;
    float createdAt;
    bool alive = true;
    float fadeSpeed = 2.5f;

    internal bool Alive {
        get { return alive; }
        set { alive = value; }
    }

    internal Vector3 Position {
        get { return pos; }
        set { pos = value; }
    }

    internal string Data {
        get { return data; }

        set {
            data = value;
            transform.GetChild(0).GetComponent<Text>().text = data;
        }
    }

    internal float CreatedAt {
        get { return createdAt; }
        set { createdAt = value; }
    }

    void Start () {
        myText = GetComponentInChildren<Text>();
        myBG = GetComponent<Image>();
    }

    void Update () {
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, pos, Time.deltaTime * 500f);

        if (!alive) {
            Color newColor = myText.color;
            newColor.a -= fadeSpeed * Time.deltaTime * 2.5f;
            myText.color = newColor;

            newColor = myBG.color;
            newColor.a -= fadeSpeed * Time.deltaTime;
            myBG.color = newColor;
        }
    }
}

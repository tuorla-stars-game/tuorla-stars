﻿using UnityEngine;

public class UIWindowController : MonoBehaviour {
    GameObject[] uiWindows;

	void Start () {
        uiWindows = new GameObject[transform.childCount];

        for (int i = 0; i < uiWindows.Length; i++) {
            uiWindows[i] = transform.GetChild(i).gameObject;
            uiWindows[i].SetActive(false);
        }
	}

    internal void ToggleWindow(int id) {
        int _id = id - 1;
        uiWindows[_id].SetActive(!uiWindows[_id].activeSelf);
    }
}

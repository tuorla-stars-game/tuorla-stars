﻿using System.Collections.Generic;
using UnityEngine;

public class NotificationsController : MonoBehaviour {
    Vector3[] notPoses = new Vector3[3];
    Vector3 leftNotPos;
    GameObject sampleNot;

    [SerializeField]
    NewNotification mainNotif;

    [SerializeField]
    float notificationLifetime = 1.75f;
    [SerializeField]
    float notificationDeathtime = 2f;

    float timeSinceLatestNotif = 0f;

    [SerializeField]
    bool useNewNotifs = false;

    bool acceptNotifs = true;

    Queue<string> notificationQueue = new Queue<string>();
    List<Notification> notifications = new List<Notification>();

    internal float NotificationLifetime {
        get {
            return notificationLifetime;
        }
        set {
            notificationLifetime = value;
        }
    }

    internal float NotificationDeathtime {
        get {
            return notificationDeathtime;
        }
        set {
            notificationDeathtime = value;
        }
    }

    internal bool AcceptNewNotifications {
        get {
            return acceptNotifs;
        }
        set {
            acceptNotifs = value;
        }
    }

    void Start () {
        if (!useNewNotifs) {
            RectTransform rectTransform = GetComponent<RectTransform>();
            float width = rectTransform.rect.width;
            float height = rectTransform.rect.height;

            notPoses[0] = new Vector3(0f, 0f + height / 3f, 0f);
            notPoses[1] = new Vector3(0f, 0f, 0f);
            notPoses[2] = new Vector3(0f, 0f - height / 3f, 0f);

            leftNotPos = new Vector3(-width, 0f, 0f);

            sampleNot = transform.GetChild(0).gameObject;
            sampleNot.GetComponent<Notification>().Position = sampleNot.transform.localPosition;
        }
    }

    void Update () {
        int notAmount = notifications.Count;

        if (useNewNotifs) {
            notAmount = notificationQueue.Count;
            if (notAmount > 0) {
                if (Time.realtimeSinceStartup - timeSinceLatestNotif > notificationLifetime) {
                    string queued = notificationQueue.Dequeue();
                    mainNotif.Data = queued;
                    timeSinceLatestNotif = Time.realtimeSinceStartup;
                }
            }
        }
        else {
            if (notAmount > 0) {
                for (int i = notAmount - 1; i >= 0; i--) {
                    if (Time.realtimeSinceStartup - notifications[i].CreatedAt > notificationLifetime) {
                        /*Vector3 pos = notifications[i].Position;
                        pos.x = leftNotPos.x;
                        notifications[i].Position = pos;*/
                        notifications[i].Alive = false;

                        Destroy(notifications[i].gameObject, notificationDeathtime);
                        notifications.RemoveAt(i);
                    }
                }
            }

            notAmount = notifications.Count;

            if (notAmount < 3 && notificationQueue.Count > 0) {
                Notification notification = Instantiate(sampleNot, transform, true)
                                            .GetComponent<Notification>();

                string queued = notificationQueue.Dequeue();

                notification.Data = queued;
                notification.CreatedAt = Time.realtimeSinceStartup;
                notification.Position = notPoses[0];

                for (int i = 0; i < notAmount; i++) {
                    notifications[i].Position = notPoses[i + 1];
                }

                notifications.Insert(0, notification);
                ;
            }
        }
    }

    internal void AddNotification(string text) {
        if (acceptNotifs) {
            notificationQueue.Enqueue(text);
        }
        //Debug.Log("notif enqueued: " + text);
    }

    internal void AddNotification(object obj) {
        string serializedObject = obj.ToString();

        if (serializedObject == null) {
            serializedObject = "Object could not be serialized.";
        }

        AddNotification(serializedObject);
    }
}
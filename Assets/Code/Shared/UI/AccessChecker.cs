﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccessChecker : MonoBehaviour {
    [SerializeField]
    GameObject lockGraphic;

    [SerializeField]
    Button[] button;

    [SerializeField]
    Scenes scene;

    [SerializeField]
    internal bool hasAccess = false;

	void Start () {
        if (scene == Scenes.Telescope) {
            if (DataHandler.instance.HasAllTelescopeParts() || hasAccess) {
                hasAccess = true;
                foreach (Button nButton in button) {
                    nButton.interactable = true;
                }

                lockGraphic.SetActive(false);
            }
            else {
                foreach (Button nButton in button) {
                    nButton.interactable = false;
                }

                lockGraphic.SetActive(true);
            }
        }
        else if (scene == Scenes.Solar) {
            if (DataHandler.instance.IsSolarGameUnlocked || hasAccess) {
                hasAccess = true;
                foreach (Button nButton in button) {
                    nButton.interactable = true;
                }

                lockGraphic.SetActive(false);
            }
            else {
                foreach (Button nButton in button) {
                    nButton.interactable = false;
                }

                lockGraphic.SetActive(true);
            }
        }
	}

    internal void EnableAccess(Scenes scene) {
        hasAccess = true;

        if (scene == Scenes.Telescope) {
            foreach (Button nButton in button) {
                nButton.interactable = true;
            }

            lockGraphic.SetActive(false);
        }
        else if (scene == Scenes.Solar) {
            foreach (Button nButton in button) {
                nButton.interactable = true;
            }

            lockGraphic.SetActive(false);
        }
    }
}

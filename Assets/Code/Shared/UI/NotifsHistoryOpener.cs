﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotifsHistoryOpener : MonoBehaviour {
    RectTransform myRect;

    [SerializeField]
    bool isOpen = false;
    [SerializeField]
    Text changeLetter;

    float speed = 10f;
    float openOffsetMinY = 2f;
    float closedOffsetMinY = 210f;

    internal bool IsHistoryOpen {
        get {
            return isOpen;
        }
        set {
            isOpen = value;
        }
    }
	void Start () {
        myRect = GetComponent<RectTransform>();
	}
	
	void Update () {
        Vector2 offsetMin = myRect.offsetMin;

        if (isOpen && offsetMin.y != openOffsetMinY) {
            offsetMin.y = Mathf.Lerp(offsetMin.y, openOffsetMinY, Time.deltaTime * speed);

            if (offsetMin.y - openOffsetMinY < 1f) {
                offsetMin.y = openOffsetMinY;
            }

            myRect.offsetMin = offsetMin;
        }
        else if (!isOpen && offsetMin.y != closedOffsetMinY) {
            offsetMin.y = Mathf.Lerp(offsetMin.y, closedOffsetMinY, Time.deltaTime * speed);

            if (closedOffsetMinY - offsetMin.y < 1f) {
                offsetMin.y = closedOffsetMinY;
            }

            myRect.offsetMin = offsetMin;
        }

    }

    public void ToggleOpenness() {
        isOpen = !isOpen;
        GameController.instance.uiController.PlayButtonSFX();
        string letter = isOpen ? "-" : "+";
        changeLetter.text = letter;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewNotification : MonoBehaviour {
    [SerializeField]
    Text myText;

    [SerializeField]
    NewNotification nextNotif;

    string data;

    internal string Data {
        get {
            return data;
        }

        set {
            SetNextData();
            data = myText.text = value;
        }
    }

    public float CreatedAt {
        get;
        internal set;
    }

    void Start() {
    }

    void Update() {
    }

    void SetNextData() {
        if (nextNotif != null) {
            nextNotif.Data = data;
        }
    }
}

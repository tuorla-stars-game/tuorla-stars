﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAddGraphicsController : MonoBehaviour {
    GameObject cardAddGraphic;

    [SerializeField]
    float cooldownTime = 0.3f;

    int numGraphicsToAdd = 0;
    int oldNumGraphics = 0;
    float timer;

	void Awake () {
        cardAddGraphic = transform.GetChild(0).gameObject;
	}
	
	void Update () {
        if (numGraphicsToAdd > 0) {
            if (oldNumGraphics == 0) {
                timer = cooldownTime;
            }

            if (cooldownTime - timer <= 0f) {
                GameObject obj = Instantiate(cardAddGraphic, transform);
                obj.SetActive(true);
                numGraphicsToAdd--;
                timer = 0f;
            }

            timer += Time.deltaTime;
        }
	}

    public void AddCardGraphic(int num) {
        oldNumGraphics = numGraphicsToAdd;
        numGraphicsToAdd += num;
    }
}

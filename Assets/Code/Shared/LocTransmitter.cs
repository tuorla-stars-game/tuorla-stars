﻿using UnityEngine;

internal class LocTransmitter : MonoBehaviour {
    bool locationEnabled = false;
    float timeCounter = 0f;
    string databaseID = "";
    string myInitials = "";
    bool isTeacher = false;
    Vector2 spoofRelative = Vector2.zero;

    [SerializeField]
    bool spoofGoodShit = false;
    [SerializeField]
    float locUpdateFrequency = 2f;
    [SerializeField]
    bool disableGPS = false;

    void Start() {
        WebHelper.OnPostReady += PostDoneHandler;
        LocationHelper.OnLocReady += LocDoneHandler;

        if (GameController.instance.dataHandler.Initials != "") {
            myInitials = GameController.instance.dataHandler.Initials;
            isTeacher = GameController.instance.dataHandler.IsTeacher;
        }

        if (!disableGPS) {
            StartCoroutine(LocationHelper.StartLocServices());
        }
    }

    void Update() {
        timeCounter += Time.deltaTime;
        Vector2 relativeLoc = LocationHelper.GetRelativeLocationData(NumberHelper.TuorlaMapBoundaries);

        if (spoofGoodShit) {
            relativeLoc = new Vector2(0.5f, 0.5f);
        }

        if (timeCounter > locUpdateFrequency) {
            if ((locationEnabled || spoofGoodShit) && IsPlayerInBounds(relativeLoc)) {
                LocationInfo loc = LocationHelper.GetLocationData();
                float longitude = loc.longitude;
                float latitude = loc.latitude;

                if (spoofGoodShit) {
                    float padding = 0.2f;
                    float height = NumberHelper.TuorlaMapBoundaries.topBoundary;
                    height -= NumberHelper.TuorlaMapBoundaries.bottomBoundary;

                    latitude = Random.Range(NumberHelper.TuorlaMapBoundaries.bottomBoundary + height * padding,
                                             NumberHelper.TuorlaMapBoundaries.topBoundary - height * padding);

                    float width = NumberHelper.TuorlaMapBoundaries.rightBoundary;
                    width -= NumberHelper.TuorlaMapBoundaries.leftBoundary;

                    longitude = Random.Range(NumberHelper.TuorlaMapBoundaries.leftBoundary + width * padding,
                                            NumberHelper.TuorlaMapBoundaries.rightBoundary - width * padding);

                    relativeLoc = LocationHelper.RealCoordsToRelativeCoords(new Vector2(longitude, latitude),
                                  NumberHelper.TuorlaMapBoundaries);
                    spoofRelative = relativeLoc;
                }

                //Debug.Log("dbid: " + databaseID);

                if (databaseID == "" && myInitials != "") {
                    string name = SystemInfo.deviceUniqueIdentifier;
                    //Debug.Log("posting: " + myInitials + ", " + isTeacher);
                    StartCoroutine(WebHelper.PostLocData(name, longitude, latitude, myInitials, isTeacher));
                    databaseID = "working";
                }
                else if (databaseID == "working") {

                }
                else if (databaseID != "working" && databaseID != "") {
                    //Debug.Log("Putting");
                    StartCoroutine(WebHelper.PutLocData(databaseID, longitude, latitude, isTeacher, myInitials));
                }
            }

            timeCounter = 0f;
        }

    }

    bool IsPlayerInBounds(Vector2 loc) {
        if (loc.x < 0f || loc.x > 1f) {
            return false;
        }

        if (loc.y < 0f || loc.y > 1f) {
            return false;
        }

        return true;
    }

    internal void LocDoneHandler(bool status) {
        locationEnabled = status;
        LocationHelper.OnLocReady -= LocDoneHandler;
    }

    internal void PostDoneHandler(string id) {
        databaseID = id;
        WebHelper.OnPostReady -= PostDoneHandler;
    }
}

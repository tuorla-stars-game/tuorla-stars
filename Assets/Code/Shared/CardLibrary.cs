﻿using System;
using UnityEngine;

public class CardLibrary : MonoBehaviour{
    public static CardLibrary instance;
    UnitLocalizations unitLocs;
    Loc loc;

    TuorlaMeta[] tuorlaMetas;
    TuorlaCard[] tuorlaCards;
    TuorlaCardLocalized[] tuorlaCardsLocalized;
    TuorlaCardLocalized[] tuorlaCardsLocCarousel;

    PlanetMeta[] planetMetas;
    PlanetCard[] planetCards;
    PlanetCardLocalized[] planetCardsLocalized;
    PlanetCardLocalized[] planetCardsLocCarousel;

    MiscMeta[] miscMetas;
    MiscCard[] miscCards;
    MiscCardLocalized[] miscCardsLocalized;
    MiscCardLocalized[] miscCardsLocCarousel;

    TelescopeMeta[] telescopeMetas;
    TelescopeCard[] telescopeCards;
    TelescopeCardLocalized[] telescopeCardsLocalized;
    TelescopeCardLocalized[] telescopeCardsLocCarousel;

    internal const string tuorlaString = "tuorla";
    internal const string planetsString = "planets";
    internal const string miscString = "misc";
    internal const string telescopeString = "telescope";

    int totalNumberCards = 0;

    internal TuorlaCard[] TuorlaCards {
        get { return tuorlaCards; }
    }

    internal int TotalNumberOfCards {
        get {
            return totalNumberCards;
        }
    }

    internal int TotalNumberOfCardsWithoutMisc {
        get {
            return totalNumberCards - miscCardsLocCarousel.Length;
        }
    }

    internal Loc Localization {
        get {
            return loc;
        }
        set {
            loc = value;
            InitializeLibrary();
        }
    }

    void Awake() {
        if (instance == null) {
            instance = this;
            InitializeLibrary();
            DontDestroyOnLoad(gameObject);

            totalNumberCards = tuorlaCardsLocCarousel.Length;
            totalNumberCards += planetCardsLocCarousel.Length;
            totalNumberCards += miscCardsLocCarousel.Length;
            totalNumberCards += telescopeCardsLocCarousel.Length;
            //Debug.Log("library initialized");
        }
        else {
            Destroy(gameObject);
        }
    }

    void InitializeLibrary() {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Cards/json/units_loc");
        unitLocs = JsonUtility.FromJson<UnitLocalizations>(jsonFile.text);

        string toLoad = tuorlaString;
        tuorlaMetas = CardHelper.loadMetadataCards<TuorlaMeta>(toLoad);
        TuorlaCardRaw[] tuorlaCardsRaw = CardHelper.loadData<TuorlaCardRaw>(toLoad);

        tuorlaCardsLocalized = new TuorlaCardLocalized[tuorlaMetas.Length];
        tuorlaCards = new TuorlaCard[tuorlaMetas.Length];

        for (int i = 0; i < tuorlaMetas.Length; i++) {
            TuorlaCardRaw cardRaw = Array.Find<TuorlaCardRaw>(tuorlaCardsRaw, x => x.ID == tuorlaMetas[i].ID);
            TuorlaCard card = new TuorlaCard(cardRaw);
            tuorlaCards[i] = card;
            tuorlaCardsLocalized[i] = new TuorlaCardLocalized(loc, tuorlaMetas[i], card);
        }

        tuorlaCardsLocCarousel = Array.FindAll<TuorlaCardLocalized>(tuorlaCardsLocalized, x => x.isIncluded);

        toLoad = planetsString;
        planetMetas = CardHelper.loadMetadataCards<PlanetMeta>(toLoad);
        PlanetCardRaw[] planetCardsRaw = CardHelper.loadData<PlanetCardRaw>(toLoad);

        planetCardsLocalized = new PlanetCardLocalized[planetMetas.Length];
        planetCards = new PlanetCard[planetMetas.Length];

        for (int i = 0; i < planetMetas.Length; i++) {
            PlanetCardRaw cardRaw = Array.Find<PlanetCardRaw>(planetCardsRaw, x => x.ID == planetMetas[i].ID);
            PlanetCard card = new PlanetCard(cardRaw);
            planetCards[i] = card;
            planetCardsLocalized[i] = new PlanetCardLocalized(loc, unitLocs, planetMetas[i], card);
        }

        planetCardsLocCarousel = Array.FindAll<PlanetCardLocalized>(planetCardsLocalized, x => x.isIncluded);

        toLoad = miscString;
        miscMetas = CardHelper.loadMetadataCards<MiscMeta>(toLoad);
        MiscCardRaw[] miscCardsRaw = CardHelper.loadData<MiscCardRaw>(toLoad);

        miscCardsLocalized = new MiscCardLocalized[miscMetas.Length];
        miscCards = new MiscCard[miscMetas.Length];

        for (int i = 0; i < miscMetas.Length; i++) {
            MiscCardRaw cardRaw = Array.Find<MiscCardRaw>(miscCardsRaw, x => x.ID == miscMetas[i].ID);
            MiscCard card = new MiscCard(cardRaw);
            miscCards[i] = card;
            miscCardsLocalized[i] = new MiscCardLocalized(loc, miscMetas[i], card);
        }

        miscCardsLocCarousel = Array.FindAll<MiscCardLocalized>(miscCardsLocalized, x => x.isIncluded);

        toLoad = telescopeString;
        telescopeMetas = CardHelper.loadMetadataCards<TelescopeMeta>(toLoad);
        TelescopeCardRaw[] telescopeCardsRaw = CardHelper.loadData<TelescopeCardRaw>(toLoad);

        telescopeCardsLocalized = new TelescopeCardLocalized[telescopeMetas.Length];
        telescopeCards = new TelescopeCard[telescopeMetas.Length];

        for (int i = 0; i < telescopeMetas.Length; i++) {
            TelescopeCardRaw cardRaw = Array.Find<TelescopeCardRaw>(telescopeCardsRaw, x => x.ID == telescopeMetas[i].ID);
            TelescopeCard card = new TelescopeCard(cardRaw);
            telescopeCards[i] = card;
            telescopeCardsLocalized[i] = new TelescopeCardLocalized(loc, telescopeMetas[i], card);
        }

        telescopeCardsLocCarousel = Array.FindAll<TelescopeCardLocalized>(telescopeCardsLocalized, x => x.isIncluded);
    }

    internal TuorlaCardLocalized GetTuorlaCardLocalizedByID(int ID) {
        return Array.Find<TuorlaCardLocalized>(tuorlaCardsLocalized, x => x.ID == ID);
    }

    internal TuorlaCardLocalized GetTuorlaCardLocalizedByIndex(int index) {
        return tuorlaCardsLocCarousel[index - 1];
    }

    internal PlanetCardLocalized GetPlanetCardLocalizedByID(int ID) {
        return Array.Find<PlanetCardLocalized>(planetCardsLocalized, x => x.ID == ID);
    }

    internal PlanetCardLocalized GetPlanetCardLocalizedByIndex(int index) {
        return planetCardsLocCarousel[index - 1];
    }

    internal MiscCardLocalized GetMiscCardLocalizedByID(int ID) {
        return Array.Find<MiscCardLocalized>(miscCardsLocalized, x => x.ID == ID);
    }

    internal MiscCardLocalized GetMiscCardLocalizedByIndex(int index) {
        return miscCardsLocCarousel[index - 1];
    }

    internal TelescopeCardLocalized GetTelescopeCardLocalizedByID(int ID) {
        return Array.Find<TelescopeCardLocalized>(telescopeCardsLocalized, x => x.ID == ID);
    }

    internal TelescopeCardLocalized GetTelescopeCardLocalizedByIndex(int index) {
        return telescopeCardsLocCarousel[index - 1];
    }

    internal int GetNumCards(string category, bool isCarousel = false) {
        switch (category) {
            case tuorlaString:
                if (isCarousel) {
                    return tuorlaCardsLocCarousel.Length;
                }

                return tuorlaCards.Length;
            case planetsString:
                if (isCarousel) {
                    return planetCardsLocCarousel.Length;
                }

                return planetCards.Length;
            case miscString:
                if (isCarousel) {
                    return miscCardsLocCarousel.Length;
                }

                return miscCards.Length;
            case telescopeString:
                if (isCarousel) {
                    return telescopeCardsLocCarousel.Length;
                }

                return telescopeCards.Length;
            default:
                break;
        }

        return -1;
    }
}
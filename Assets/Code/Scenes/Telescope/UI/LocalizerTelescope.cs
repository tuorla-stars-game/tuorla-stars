﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizerTelescope : MonoBehaviour {
    [SerializeField]
    Text TUTORIAL_SAMPLE_NOTIFICATION;
    [SerializeField]
    Text TUTORIAL_NOTIFICATION_EXPLANATION;
    [SerializeField]
    Text TUTORIAL_DRAG_PART_EXPLANATION;
    [SerializeField]
    Text UNLOCK_SOLAR_SYSTEM;
    [SerializeField]
    Text UNLOCK_SOLAR_SYSTEM_DIRECTIONS;

    void OnEnable() {
        LocalizedTelescopeStrings lts = GCTelescope.instance.localizedStrings;

        if (TUTORIAL_SAMPLE_NOTIFICATION != null) {
            TUTORIAL_SAMPLE_NOTIFICATION.text = lts.TUTORIAL_SAMPLE_NOTIFICATION;
        }
        if (TUTORIAL_NOTIFICATION_EXPLANATION != null) {
            TUTORIAL_NOTIFICATION_EXPLANATION.text = lts.TUTORIAL_NOTIFICATION_EXPLANATION;
        }
        if (TUTORIAL_DRAG_PART_EXPLANATION != null) {
            TUTORIAL_DRAG_PART_EXPLANATION.text = lts.TUTORIAL_DRAG_PART_EXPLANATION;
        }
        if (UNLOCK_SOLAR_SYSTEM != null) {
            UNLOCK_SOLAR_SYSTEM.text = lts.UNLOCK_SOLAR_SYSTEM;
        }
        if (UNLOCK_SOLAR_SYSTEM_DIRECTIONS != null) {
            UNLOCK_SOLAR_SYSTEM_DIRECTIONS.text = lts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS;
        }

        enabled = false;
    }
}

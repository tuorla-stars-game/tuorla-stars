﻿using System;
using UnityEngine;

internal class UIControllerTelescope : UIController {
    PartsPipeController ppController;

    [SerializeField]
    internal GameObject baseRotSlider;
    [SerializeField]
    internal GameObject doorsOpenButton;
    [SerializeField]
    internal GameObject moonRotControls;
    [SerializeField]
    internal GameObject nextControls;
    [SerializeField]
    internal GameObject secMirrorRotControls;
    [SerializeField]
    internal GameObject gameControlsOcular;

    protected override void SubAwake() {

    }

    protected override void SubStart() {
        gc = GCTelescope.instance;
        notifs = GetComponentInChildren<NotificationsController>();
        ppController = GetComponentInChildren<PartsPipeController>();
    }

    protected override void SubUpdate() {

    }

    internal void RemoveButton(string name) {
        ppController.DeleteButton(name);
    }

    internal void AddNotification(string msg) {
        notifs.AddNotification(msg);
    }

    protected override GameController GetCameController() {
        Debug.Log("returning telescope");
        return GCTelescope.instance;
    }

    internal override void ClearUIElements() {
    }
}

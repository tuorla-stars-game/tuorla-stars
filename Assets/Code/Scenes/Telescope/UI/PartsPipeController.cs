﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsPipeController : MonoBehaviour {
    List<PartButtonController> parts;
    Rect panelRect;
    Rect buttonRect;

    const float buttonGap = 10f;
    const int numButtons = 4;

    void Start() {
        parts = new List<PartButtonController>();
        panelRect = GetComponent<RectTransform>().rect;

        buttonRect = transform.GetChild(0).GetComponent<RectTransform>().rect;

        for (int i = 0; i < transform.childCount; i++) {
            parts.Add(transform.GetChild(i).GetComponent<PartButtonController>());
            
            parts[i].Active = true;

            if ((parts[i].Position.y + panelRect.height / 2f) / (buttonRect.height + buttonGap) > numButtons) {
                parts[i].gameObject.SetActive(false);
            }
        }
    }
	
    void Update() {
    	
    }

    internal void DeleteButton(string name) {
        PartButtonController delPart = parts.Find(x => x.name == name);
        int index = parts.IndexOf(delPart);
        parts.Remove(delPart);
        Destroy(delPart.gameObject);

        for (int i = index; i < parts.Count; i++) {
            if (parts[i].gameObject.activeSelf) {
                Vector3 pos = parts[i].Position;
                pos.y -= (buttonRect.height + buttonGap);
                parts[i].Position = pos;
            }
        }

        if (parts.Count >= numButtons) {
            parts[numButtons - 1].gameObject.SetActive(true);
            parts[numButtons - 1].Active = true;
        }
    }
}

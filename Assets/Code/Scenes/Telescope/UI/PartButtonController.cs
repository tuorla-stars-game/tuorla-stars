﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartButtonController : MonoBehaviour {
    const float speed = 150.0f;

    Vector3 pos;
    bool active;
    
    internal Vector3 Position {
        get {
            return pos;
        }
        set {
            pos = value;
        }
    }

    internal bool Active {
        get {
            return active;
        }
        set {
            active = value;
        }
    }

	void Awake() {
        pos = transform.localPosition;
        active = false;
	}
	
	void Update() {
        if (active && transform.localPosition != pos) {
            transform.localPosition = Vector3.MoveTowards(
                transform.localPosition,
                pos,
                speed * Time.deltaTime);
        }
	}
}

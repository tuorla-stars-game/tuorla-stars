﻿using UnityEngine;

public class InputHandlerTelescope : MonoBehaviour {
    NotificationsController notifs;
    GCTelescope gc;
    UIControllerTelescope uiController;
    TGCameraController cameraController;

    RectTransform carryingPart;
    float touchTimer = 0f;
    float touchTracker = 0f;
    float maxTapTime = 0.15f;
    bool touchStartedOverUIObject;
    bool isTwoTouch = false;
    Vector2 touchStartPos;

    [SerializeField]
    bool runThing = false;

    void Start() {
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();

        gc = GCTelescope.instance;
        cameraController = gc.cameraController;
        uiController = GetComponent<UIControllerTelescope>();
    }

    void Update() {
        if (Input.touchCount == 1) {
            TouchSingleHandle();
        }

        if (carryingPart != null && Input.touchCount == 0) {
            Destroy(carryingPart.gameObject);
            carryingPart = null;
        }
    }

    void TouchSingleHandle() {
        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began) {
            touchTimer = 0f;
            touchTracker = 0f;
            touchStartedOverUIObject = InputHelper.IsPointerOverUIObject();
            isTwoTouch = false;
            touchStartPos = touch.position;
        }
        else if (touch.phase == TouchPhase.Moved) {

        }
        else if (touch.phase == TouchPhase.Stationary) {

        }
        else if (touch.phase == TouchPhase.Ended) {
            if (!isTwoTouch) {
                TouchSingleEndHandle(touch.position);
            }

            isTwoTouch = false;
            touchStartedOverUIObject = false;
            return;
        }

        if (touchStartedOverUIObject) {
            if (carryingPart != null) {
                carryingPart.position = touch.position;
            }
        }
        else {
            //Vector3 camInput = touch.deltaPosition;
            cameraController.isTouching = true;
            Vector3 camInput = (Vector3)(touch.position - touchStartPos) / 200f;

            if (camInput.magnitude < 2f) {
                cameraController.PlayerCamAdjust = camInput; 
            }

            touchTimer += Time.deltaTime;
            touchTracker += touch.deltaPosition.magnitude;
        }
    }

    void TouchSingleEndHandle(Vector2 pos) {
        if (carryingPart != null) {
            Ray ray = Camera.current.ScreenPointToRay(pos);

            RaycastHit rayHit;
            if (Physics.Raycast(ray, out rayHit) &&
                touchTimer < maxTapTime &&
                touchTracker < 10f) {

                //notifs.AddNotification(rayHit.collider.name +
                //                  ", touchTimer: " + touchTimer +
                //                  ", touchTracker: " + touchTracker);
            }

            TelescopePartController touchee = rayHit.collider.GetComponent<TelescopePartController>();

            if (touchee != null) {
                touchee.OnTouch(carryingPart.gameObject.name);    
            }

            Destroy(carryingPart.gameObject);
            carryingPart = null;
        }

        cameraController.isTouching = false;
    }

    public void StartCarryingPart(RectTransform rect) {
        if (Input.touchCount > 0) {
            carryingPart = Instantiate(rect, transform);
            carryingPart.gameObject.name = rect.gameObject.name;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TGCameraController : MonoBehaviour {
    Transform pos;
    Transform lookAtTarget;
    Transform bezierPoint;
    float travelTime;

    [SerializeField]
    Vector3 playerAdjust;

    internal bool isTouching = false;
    bool allowPlayerAdjust = true;
    float flierTimer = 0f;
    Vector3 bezierStart;

    internal Vector3 PlayerCamAdjust {
        get {
            return playerAdjust;
        }
        set {
            playerAdjust = value;
        }
    }
    
    internal bool AllowPlayerAdjust {
        get {
            return allowPlayerAdjust;
        }
        set {
            allowPlayerAdjust = value;
        }
    }

    [SerializeField]
    internal float speed = 100f;
    [SerializeField]
    internal float rotSpeed = 100f;

    internal Transform LookAt {
        get {
            return lookAtTarget;
        }
        set {
            lookAtTarget = value;
        }
    }

    internal Transform Position {
        get {
            return pos;
        }
        set {
            if (pos != null) {
                bezierStart = pos.position;
            }
            pos = value;
        }
    }

    internal Transform BezierPoint {
        get {
            return bezierPoint;
        }
        set {
            bezierPoint = value;
        }
    }

    internal float TravelTime {
        get {
            return travelTime;
        }
        set {
            travelTime = value;
        }
    }

	void Start() {
		
	}
	
	void Update() {
        Vector3 newPos;

        if (allowPlayerAdjust) {
            newPos = pos.position + pos.right * playerAdjust.x + pos.up * playerAdjust.y;
        }
        else {
            newPos = pos.position;
        }

        if (bezierPoint != null && flierTimer < travelTime) {
            float _flierTimer = flierTimer / travelTime;
            Vector3 bezieredPos = Mathf.Pow(1f - _flierTimer, 2f) * bezierStart;
            bezieredPos += 2f * (1f - _flierTimer) * _flierTimer * bezierPoint.position;
            bezieredPos += Mathf.Pow(_flierTimer, 2f) * pos.position;
            newPos = bezieredPos;

            flierTimer += Time.deltaTime;
        }
        else if (flierTimer > travelTime && bezierPoint != null) {
            bezierPoint = null;
            flierTimer = 0f;
            travelTime = 0f;
        }

        if (transform.position != newPos) {
            transform.position = Vector3.MoveTowards(transform.position,
                                                     newPos,
                                                     speed * Time.deltaTime);
        }

        if (!isTouching) {
            playerAdjust *= 0.4f;
        }

        Vector3 forwardLook = lookAtTarget.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(forwardLook);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotSpeed);
	}
}

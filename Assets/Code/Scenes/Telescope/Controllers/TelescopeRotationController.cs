﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TelescopeRotationController : MonoBehaviour {
    [SerializeField]
    float rotSpeed = 5f;

    [SerializeField]
    Text angleDisplayText;
    [SerializeField]
    RotController pipeRotController;

    float rightnessTimer = 0f;
    float playerInput = 0f;

    void Update () {
        Vector3 rot = transform.localRotation.eulerAngles;
        rot.y += playerInput;
        Quaternion targetRotation = Quaternion.Euler(rot);

        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, rotSpeed * Time.deltaTime);
        rot = transform.localEulerAngles;

        if (rot.y > 329f && rot.y < 331f) {
            if (rightnessTimer == 0f) {
                GCTelescope.instance.SendMsgToState(999);
            }
            rightnessTimer += Time.deltaTime;
        }
        else {
            rightnessTimer = 0f;
        }


        if (rightnessTimer > 1.25f) {
            GCTelescope.instance.PlaceTelescopePart();
            this.enabled = false;
        }
        else if (rightnessTimer >= 0.5f) {
            rot.y = 330f;
            targetRotation = Quaternion.Euler(rot);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, (rotSpeed / 100f) * Time.deltaTime);

            pipeRotController.enabled = true;
            Vector3 pipeRot = pipeRotController.LocalRotation;
            pipeRot.y = 90f;
            pipeRotController.TargetRotation = pipeRot;
        }
        else if (rightnessTimer >= 0.25f) {
            playerInput = 0f;
        }

        float angle = rot.y >= 180f ? 90f - (360f - rot.y) : 90f + rot.y;
        string angleString = (Mathf.Round(angle * 100f) / 100f).ToString();

        if (angleString.Length == 2 && angle < 100f
            || angleString.Length == 3 && angle >= 100f) {
            angleString = angleString.Insert(angleString.Length, ".");
        }

        while (angleString.Length < 5 && angle < 100f
            || angleString.Length < 6 && angle >= 100f) {
            angleString = angleString.Insert(angleString.Length, "0");
        }

        if (angleDisplayText != null) {
            angleDisplayText.text = angleString;
        }
	}

    public void PlayerInput(float value) {
        if (rightnessTimer < .25f) {
            playerInput = value;
        }
    }
}

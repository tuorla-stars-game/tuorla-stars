﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelescopePartController : MonoBehaviour {
    MeshRenderer partRenderer;
    GCTelescope gc;

    bool wasFinished = false;

    [SerializeField]
    Material transparentMat;
    [SerializeField]
    Material opaqueMat;

    void Start() {
        Initialize();
    }

    void OnEnable() {
        Initialize();
    }

    void Update() {
        if (!wasFinished) {
            Color myEmission;
            myEmission.a = myEmission.b = myEmission.g = myEmission.r = myEmission.a = (Mathf.Sin(3f * Time.realtimeSinceStartup) + 1f) / 2f;
            partRenderer.material.SetColor("_EmissionColor", myEmission);
        }
    }

    internal void Initialize() {
        partRenderer = GetComponent<MeshRenderer>();
        gc = GCTelescope.instance;
    }

    internal void OnTouch(string name) {
        if (name == gameObject.name.Substring(0, name.Length)) {
            wasFinished = true;
            DisplayPart();
            gc.PlaceTelescopePart(gameObject.name);
        }
    }

    internal void HidePart() {
        partRenderer.material = transparentMat;
    }

    internal void DisplayPart() {
        partRenderer.material = opaqueMat;
        enabled = false;
    }
}

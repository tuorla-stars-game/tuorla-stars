﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotController : MonoBehaviour {
    Vector3 targetRot = Vector3.zero;
    Quaternion startRot;

    float speed = .75f;
    float timer = 0f;

    internal Vector3 TargetRotation {
        get {
            return targetRot;
        }
        set {
            startRot = transform.localRotation;
            targetRot = value;
        }
    }

    internal Vector3 LocalRotation {
        get {
            return transform.localEulerAngles;
        }
    }

    void Start() {
        startRot = transform.localRotation;
    }

    void Update() {
        timer += Time.deltaTime;

        if (transform.localEulerAngles != targetRot && targetRot != Vector3.zero) {
            Quaternion newRot = Quaternion.Euler(targetRot);
            transform.localRotation = Quaternion.Lerp(startRot, newRot, timer / speed);
        }
        else if (transform.localEulerAngles == targetRot && targetRot != Vector3.zero) {
            this.enabled = false;
        }
	}
}

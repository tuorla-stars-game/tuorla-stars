﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonRotatorController : MonoBehaviour {
    [SerializeField]
    Material bgMat;

    float rightnessTimer = 0f;
    float playerInput = 0f;

    [SerializeField]
    float moveSpeed = 10f;
	
	void Update() {
        Vector3 rot = transform.localEulerAngles;
        rot.y += playerInput * Time.deltaTime * moveSpeed;

        float angle = rot.y >= 180f ? 90f - (360f - rot.y) : 90f + rot.y;

        //float emission = Mathf.Abs((angle / 144f) - 1.25f);
        //Color _c;
        //_c.a = _c.b = _c.g = _c.r = _c.a = emission;
        //bgMat.SetFloat("_EmissionColorUI", emission);

        transform.localRotation = Quaternion.Euler(rot);

        rot = transform.localEulerAngles;
        Debug.Log(rot.y);
        if (rot.y > (360f - 5f) || rot.y < 5f) {
            if (rightnessTimer == 0f) {
                GCTelescope.instance.SendMsgToState(999);
            }

            rightnessTimer += Time.deltaTime;
        }
        else {
            rightnessTimer = 0f;
        }

        if (rightnessTimer > 1.25f) {
            GCTelescope.instance.PlaceTelescopePart();
            this.enabled = false;
        }
        else if (rightnessTimer >= 0.5f) {
            rot = transform.localRotation.eulerAngles;
            rot.y = 0f;
            Quaternion targetRotation = Quaternion.Euler(rot);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, 10f * Time.deltaTime);
        }
    }

    

    public void PlayerInput(float value) {
        if (rightnessTimer < 0.25f) {
            playerInput = value;
        }
    }
}

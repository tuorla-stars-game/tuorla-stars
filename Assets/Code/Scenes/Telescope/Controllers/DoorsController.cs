﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsController : MonoBehaviour {
    Transform leftDoor;
    Transform rightDoor;

    float doorSpeed = .005f;
    float rightnessTimer = 0f;

    [SerializeField]
    bool doorsOpen = false;

    Vector3 closedPos;
    Vector3 openPos;

	void Start() {
        leftDoor = transform.GetChild(0);
        rightDoor = transform.GetChild(1);

        closedPos = leftDoor.localPosition;
        openPos = leftDoor.localPosition;
        openPos.x -= 0.01f;
	}
	
	void Update() {
        if (doorsOpen && leftDoor.localPosition != openPos) {
            leftDoor.localPosition = Vector3.MoveTowards(leftDoor.localPosition,
                openPos,
                Time.deltaTime * doorSpeed);

            Vector3 openRight = openPos;
            openRight.x *= -1f;

            rightDoor.localPosition = Vector3.MoveTowards(rightDoor.localPosition,
                openRight,
                Time.deltaTime * doorSpeed);
        }
        else if (!doorsOpen && leftDoor.localPosition != closedPos) {
            leftDoor.localPosition = Vector3.MoveTowards(leftDoor.localPosition,
                closedPos,
                Time.deltaTime * doorSpeed);

            Vector3 closedRight = closedPos;
            closedRight.x *= -1f;

            rightDoor.localPosition = Vector3.MoveTowards(rightDoor.localPosition,
                closedRight,
                Time.deltaTime * doorSpeed);
        }

        if (leftDoor.localPosition == openPos) {
            rightnessTimer += Time.deltaTime;

            if (rightnessTimer > 1f) {
                GCTelescope.instance.PlaceTelescopePart();
                this.enabled = false;
            }
        }
	}

    public void ToggleState() {
        if (rightnessTimer == 0f) {
            GCTelescope.instance.uiController.PlayButtonSFX();
            doorsOpen = !doorsOpen;
        }
    }
}

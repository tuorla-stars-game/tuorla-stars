﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SecMirrorRotController : MonoBehaviour {
    [SerializeField]
    float rotSpeed = 5f;

    [SerializeField]
    Text angleDisplayText;

    [SerializeField]
    Transform lightshaftTrans;

    Quaternion startRot;

    float rightnessTimer = 0f;
    float playerInput = 0f;
    float rotMin = 335f;
    float rotMax = 25f;

    void Update() {
        Vector3 rot = transform.localRotation.eulerAngles;

        float overRotation = 0f;
        float _playerInput = playerInput;

        if (rot.x < rotMin && rot.x > 250f) {
            overRotation = rotMin - rot.x;
        }
        else if (rot.x > rotMax && rot.x <= 250f) {
            overRotation = rot.x - rotMax;
        }

        if (overRotation != 0f) {
            _playerInput *= (10f - overRotation) / 10f;
        }

        rot.x += _playerInput;
        Quaternion targetRotation = Quaternion.Euler(rot);

        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, rotSpeed * Time.deltaTime);
        rot = transform.localEulerAngles;

        if (rot.x > 358f || rot.x < 2f) {
            if (rightnessTimer == 0f) {
                GCTelescope.instance.SendMsgToState(999);
            }
            rightnessTimer += Time.deltaTime;
        }
        else {
            rightnessTimer = 0f;
        }


        if (rightnessTimer > 1.25f) {
            GCTelescope.instance.PlaceTelescopePart();
            rot.x = 0f;
            transform.localRotation = Quaternion.Euler(rot);
            this.enabled = false;
        }
        else if (rightnessTimer >= 0.5f) {
            rot.x = 0f;
            targetRotation = Quaternion.Euler(rot);
            transform.localRotation = Quaternion.Lerp(startRot, targetRotation, (rightnessTimer - 0.5f) / 0.5f);
        }
        else if (rightnessTimer >= 0.25f) {
            playerInput = 0f;
            startRot = transform.localRotation;
        }

        float angle = rot.x >= 180f ? 90f - (360f - rot.x) : 90f + rot.x;
        string angleString = (Mathf.Round(angle * 100f) / 100f).ToString();

        Vector3 lightRot = lightshaftTrans.localEulerAngles;
        lightRot.x = -(90f - angle);
        lightshaftTrans.localRotation = Quaternion.Euler(lightRot);

        if (angleString.Length == 2 && angle < 100f
            || angleString.Length == 3 && angle >= 100f) {
            angleString = angleString.Insert(angleString.Length, ".");
        }

        while (angleString.Length < 5 && angle < 100f
            || angleString.Length < 6 && angle >= 100f) {
            angleString = angleString.Insert(angleString.Length, "0");
        }

        if (angleDisplayText != null) {
            angleDisplayText.text = angleString;
        }
    }

    public void PlayerInput(float value) {
        if (rightnessTimer < 0.25f) {
            playerInput = value;
        }
    }
}

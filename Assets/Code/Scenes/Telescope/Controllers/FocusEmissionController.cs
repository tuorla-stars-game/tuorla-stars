﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum TouchArea {
    None,
    Left,
    Right
}

public class FocusEmissionController : MonoBehaviour {
    Material moonMat;
    Material starsMat;
    Transform leftKnob;
    Transform rightKnob;

    const float starFocus = 1.65f;
    const float moonFocus = 1.65f;

    [SerializeField]
    float leftWheelPos = 0f;
    [SerializeField]
    float rightWheelPos = 0f;
    TouchArea touchArea = TouchArea.None;

    [SerializeField]
    float rightWheelClickThreshold = 50f;
    [SerializeField]
    float rightWheelStepAmount = 0.25f;

    [SerializeField]
    float leftWheelDamper = 3000f;
    [SerializeField]
    float rightWheelDamper = 100f;

    float rightWheelAccumulator = 0f;

    float lastLeftWheelPos = 0f;
    float lastRightWheelPos = 0f;

    float rightnessTimer = 0f;

    bool closenessInformed = false;

	void Start () {
        starsMat = transform.GetChild(0).GetComponent<MeshRenderer>().material;
        moonMat = transform.GetChild(1).GetComponent<MeshRenderer>().material;

        leftKnob = transform.GetChild(2);
        rightKnob = transform.GetChild(3);

        //gameObject.SetActive(false);
	}
	
	void Update () {
        if (touchArea != TouchArea.None) {
            if (Input.touchCount > 0) {
                Touch touch = Input.GetTouch(0);

                if (touchArea == TouchArea.Left) {
                    lastLeftWheelPos = leftWheelPos;
                    leftWheelPos = Mathf.Clamp(leftWheelPos + touch.deltaPosition.y,
                                               -0.5f * leftWheelDamper,
                                               0.5f * leftWheelDamper);

                    leftKnob.localRotation *= Quaternion.Euler((leftWheelPos - lastLeftWheelPos) / 6f, 0f, 0f);
                }
                else if (touchArea == TouchArea.Right) {
                    rightWheelAccumulator += touch.deltaPosition.y;
                    if (Mathf.Abs(rightWheelAccumulator) > rightWheelClickThreshold) {
                        lastRightWheelPos = rightWheelPos;
                        rightWheelPos = Mathf.Clamp(rightWheelPos + 
                                                    Mathf.Sign(rightWheelAccumulator) * rightWheelStepAmount * rightWheelDamper,
                                                    -0.25f * rightWheelDamper,
                                                    4f * rightWheelDamper);

                        rightKnob.localRotation *= Quaternion.Euler((rightWheelPos - lastRightWheelPos) / 6f, 0f, 0f);
                        rightWheelAccumulator = 0f;
                    }
                }
            
            }
            else {
                touchArea = TouchArea.None;
            }
        }

        float totalFocus = leftWheelPos / leftWheelDamper;
        totalFocus += rightWheelPos / rightWheelDamper;

        float dist = Mathf.Abs(moonFocus - totalFocus);

        if (dist < 0.1f) {
            rightnessTimer += Time.deltaTime;
        }
        else if (Mathf.Abs(moonFocus - totalFocus) < 0.6f && !closenessInformed) {
            GCTelescope.instance.SendMsgToState(999);
        }
        else {
            rightnessTimer = 0f;
        }
        

        if (rightnessTimer > 0.5f) {
            GCTelescope.instance.PlaceTelescopePart();
            enabled = false;
        }

        float starEmission = starFocus - totalFocus;
        float _starEmission = starEmission;
        float moonEmission = moonFocus - totalFocus;
        float _moonEmission = moonEmission;

        if (starEmission <= 0f) {
            starEmission = 1f / (-5f * starEmission + 1f);
        }
        else {
            starEmission = 1f / (5f * starEmission + 1f);
        }

        if (moonEmission <= 0f) {
            moonEmission = 1f / (-15f * moonEmission + 1f);
        }
        else {
            moonEmission = 1f / (15f * moonEmission + 1f);
        }

        Color _cStars;
        _cStars.a = _cStars.b = _cStars.g = _cStars.r = _cStars.a = starEmission;
        starsMat.SetColor("_EmissionColor", _cStars);

        Color _cMoon;
        _cMoon.a = _cMoon.b = _cMoon.g = _cMoon.r = _cMoon.a = moonEmission;
        moonMat.SetColor("_EmissionColor", _cMoon);
    }

    public void PlayerInput(bool isLeftScroll) {
        if (isLeftScroll) {
            touchArea = TouchArea.Left;
        }
        else {
            touchArea = TouchArea.Right;
        }

        if (Input.touchCount == 0) {
            touchArea = TouchArea.None;
        }

    }
}

﻿using System;
using UnityEngine;

public class GCTelescope : GameController {
    new internal static GCTelescope instance;
    new internal UIControllerTelescope uiController;
    internal TGCameraController cameraController;

    int numPartsPlaced;
    bool wasPlayerTutorialed;
    StateMachine<GCTelescope> gameState;
    internal LocalizedTelescopeStrings localizedStrings;

    [SerializeField]
    LocalizerTelescope localizer;

    [SerializeField]
    GameObject teleTutorial;
    [SerializeField]
    bool progressPhase = false;
    [SerializeField]
    internal float tutorialMsgWait = 2f;
    [SerializeField]
    internal TelescopePartController[] pipesParts;
    [SerializeField]
    internal TelescopePartController mainMirrorPart;
    [SerializeField]
    internal TelescopePartController secMirrorPart;
    [SerializeField]
    internal TelescopePartController[] focuserParts;
    [SerializeField]
    internal TelescopePartController[] ocularParts;
    [SerializeField]
    internal TelescopeRotationController teleRotController;
    [SerializeField]
    internal DoorsController doorsController;
    [SerializeField]
    internal MoonRotatorController mrController;
    [SerializeField]
    internal GameObject[] mainLightObjects;
    [SerializeField]
    internal GameObject[] secMirrorRotObjects;
    [SerializeField]
    internal SecMirrorRotController smrController;
    [SerializeField]
    internal RotController ocularHatchRot;
    [SerializeField]
    internal GameObject[] ocularLightObjs;
    [SerializeField]
    internal GameObject[] ocularFocusObjects;

    [SerializeField]
    internal Transform pipesCamPos;
    [SerializeField]
    internal Transform mainMirrorCamPos;
    [SerializeField]
    internal Transform secMirrorCamPos;
    [SerializeField]
    internal Transform focuserCamPos;
    [SerializeField]
    internal Transform ocularCamPos;
    [SerializeField]
    internal Transform baseRotCamPos;
    [SerializeField]
    internal Transform doorsCamPos;
    [SerializeField]
    internal Transform moonCamPos;
    [SerializeField]
    internal Transform[] mainLightCamPos;
    [SerializeField]
    internal Transform secMirrorRotCamPos;
    [SerializeField]
    internal Transform[] ocularLightCamPos;
    [SerializeField]
    internal Transform ocularFocusCamPos;

    [SerializeField]
    internal Transform pipesLookAt;
    [SerializeField]
    internal Transform mainMirrorLookAt;
    [SerializeField]
    internal Transform secMirrorLookAt;
    [SerializeField]
    internal Transform focuserLookAt;
    [SerializeField]
    internal Transform ocularLookAt;
    [SerializeField]
    internal Transform baseRotLookAt;
    [SerializeField]
    internal Transform doorsLookAt;
    [SerializeField]
    internal Transform moonLookAt;
    [SerializeField]
    internal Transform[] mainLightLookAt;
    [SerializeField]
    internal Transform secMirrorRotLookAt;
    [SerializeField]
    internal Transform[] ocularLightLookAt;
    [SerializeField]
    internal Transform ocularFocusLookAt;

    [SerializeField]
    internal Transform mainMirrorBez;
    [SerializeField]
    internal Transform secMirrorBez;
    [SerializeField]
    internal Transform focuserBez;
    [SerializeField]
    internal Transform ocularBez;
    [SerializeField]
    internal Transform baseRotBez;
    [SerializeField]
    internal Transform moonBez;
    [SerializeField]
    internal Transform[] mainLightBez;
    [SerializeField]
    internal Transform[] ocularLightBez;

    [SerializeField]
    internal GameObject finishScreen;

    internal int PartsPlaced {
        get {
            return numPartsPlaced;
        }
        set {
            numPartsPlaced = value;
        }
    }

    internal bool TutorialMsgSent {
        get {
            return wasPlayerTutorialed;
        }
        set {
            wasPlayerTutorialed = value;
        }
    }

    internal StateMachine<GCTelescope> StateMachine {
        get {
            return gameState;
        }
        set {
            gameState = value;
        }
    }

    protected override void SubAwake() {
        instance = this;
        dataHandler = DataHandler.instance;
        numPartsPlaced = 0;
        uiController = GameObject.FindGameObjectWithTag("Canvas")
                                 .GetComponent<UIControllerTelescope>();
        cameraController = GameObject.FindGameObjectWithTag("MainCamera")
                                     .GetComponent<TGCameraController>();

    }

    protected override void SubStart() {
        if (!dataHandler.WasTutorialPlayed(Scenes.Telescope) && teleTutorial != null) {
            teleTutorial.SetActive(true);
        }

        gameState = new StateMachine<GCTelescope>(this);
        gameState.ChangeState(PipesState.GetInstance());
    }

    protected override void SubUpdate() {
        StateMachine.Update();

        if (progressPhase) {
            numPartsPlaced++;
            gameState.MessageToState(numPartsPlaced);
            progressPhase = false;
        }
    }

    public void SendMsgToState(int msg) {
        gameState.MessageToState(msg);
    }

    public void PlaceTelescopePart(string name = "no_part") {
        numPartsPlaced++;
        gameState.MessageToState(numPartsPlaced);
        Debug.Log("finishing: " + numPartsPlaced);

        string _name = name.Substring(0, 4);
        if (_name == "Pipe") {
            name = _name;
        }

        if (name == "nextbutton") {
            GCTelescope.instance.uiController.PlayButtonSFX();
        }

        if (name != "no_part" && name != "nextbutton") {
            uiController.RemoveButton(name);
        }
    }

    public void CloseTutorial() {
        uiController.PlayButtonSFX();
        teleTutorial.SetActive(false);
        dataHandler.SetTutorialPlayed(Scenes.Telescope);
    }

    internal override UIController GetUIController() {
        return GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIControllerTelescope>();
    }

    protected override void CleanUpBeforeLoad() {
    }

    protected override void SubApplyLocalization() {
        localizedStrings = LocalizationDataLoader.LoadLocalizedTelescopeData(loc);

        localizer.enabled = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxRotator : MonoBehaviour {

    [SerializeField]
    float rotInput = 0f;
    float rotTracker = 0f;

    internal float RotatingAmount {
        get {
            return rotInput;
        }
        set {
            rotInput = value;
        }
    }

	void Update () {
        rotTracker += rotInput;

        if (transform.localEulerAngles.y != rotTracker) {
            transform.localRotation *= Quaternion.Euler(new Vector3(0f, rotInput, 0f));
        }
	}
}

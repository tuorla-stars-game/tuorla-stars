﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcularLightState : IState<GCTelescope> {
    static OcularLightState instance;
    RotController ocularHatch;
    StateNotifications notifTexts;

    float stateEntryTime;

    private OcularLightState() {

    }

    internal static OcularLightState GetInstance() {
        if (instance == null) {
            instance = new OcularLightState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        ocularHatch = context.ocularHatchRot;

        context.cameraController.Position = context.ocularLightCamPos[0];
        context.cameraController.LookAt = context.ocularLightLookAt[0];
        context.cameraController.BezierPoint = context.ocularLightBez[0];
        context.cameraController.TravelTime = 2.5f;

        context.ocularLightObjs[0].SetActive(true);
        context.ocularLightObjs[1].SetActive(true);
        
        context.uiController.nextControls.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        context.uiController.nextControls.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 15) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);

            context.cameraController.Position = context.ocularLightCamPos[1];
            context.cameraController.LookAt = context.ocularLightLookAt[1];
            context.cameraController.BezierPoint = context.ocularLightBez[1];
            context.cameraController.TravelTime = 3f;
            context.cameraController.rotSpeed *= 3f;

            Vector3 hatchRot = ocularHatch.LocalRotation;
            hatchRot.y = 180f;
            ocularHatch.TargetRotation = hatchRot;
        }
        if (num == 16) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[1]);

            context.cameraController.Position = context.ocularLightCamPos[2];
            context.cameraController.LookAt = context.ocularLightLookAt[2];
            context.cameraController.BezierPoint = context.ocularLightBez[2];
            context.cameraController.TravelTime = 3f;
            context.cameraController.rotSpeed *= 3f;

            ocularHatch.enabled = true;
            Vector3 hatchRot = ocularHatch.LocalRotation;
            hatchRot.y = 360f;
            ocularHatch.TargetRotation = hatchRot;

            GameObject[] focusObjs = context.ocularFocusObjects;

            focusObjs[0].SetActive(true);
            focusObjs[1].SetActive(true);
        }
        if (num == 17) {
            context.StateMachine.ChangeState(OcularFocusState.GetInstance());
        }
    }
}

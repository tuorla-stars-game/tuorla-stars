﻿using System;
using UnityEngine;

public class PipesState : IState<GCTelescope> {
    private static PipesState instance;
    StateNotifications notifTexts;
    TelescopePartController[] parts;

    private PipesState() {

    }

    void HandleStateChange(GCTelescope context) {


    }

    internal static PipesState GetInstance() {
        if (instance == null) {
            instance = new PipesState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        context.cameraController.Position = context.pipesCamPos;
        context.cameraController.LookAt = context.pipesLookAt;

        parts = context.pipesParts;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        foreach (TelescopePartController part in parts) {
            part.DisplayPart();
        }

        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        context.PartsPlaced = 3;
        context.StateMachine.ChangeState(MainMirrorState.GetInstance());

        //switch (num) {
        //    case 1:
        //        //msg = notifTexts.STATE_PROGRESS_FI[0];
        //        break;
        //    case 2:
        //        //msg = notifTexts.STATE_PROGRESS_FI[1];
        //        break;
        //    case 3:
        //        msg = notifTexts.STATE_EXIT_FI;
        //        HandleStateChange(context);
        //        break;
        //    default:
        //        break;
        //}
    }
}

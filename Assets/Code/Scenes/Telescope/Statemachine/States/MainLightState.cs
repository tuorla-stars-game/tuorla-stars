﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLightState : IState<GCTelescope> {
    static MainLightState instance;
    GameObject[] lightshaftObj;
    StateNotifications notifTexts;

    float stateEntryTime;

    private MainLightState() {

    }

    internal static MainLightState GetInstance() {
        if (instance == null) {
            instance = new MainLightState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        lightshaftObj = context.mainLightObjects;
        lightshaftObj[0].SetActive(true);

        context.cameraController.Position = context.mainLightCamPos[0];
        context.cameraController.BezierPoint = context.mainLightBez[0];
        context.cameraController.LookAt = context.mainLightLookAt[0];

        context.cameraController.TravelTime = 6f;
        context.cameraController.rotSpeed = 1f;

        context.uiController.nextControls.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        lightshaftObj[0].SetActive(false);
        context.uiController.nextControls.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 11) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);
            context.cameraController.Position = context.mainLightCamPos[1];
            context.cameraController.BezierPoint = context.mainLightBez[1];
            context.cameraController.LookAt = context.mainLightLookAt[1];
        }
        else if (num == 12) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[1]);
            context.cameraController.Position = context.mainLightCamPos[2];
            context.cameraController.BezierPoint = context.mainLightBez[2];
            context.cameraController.LookAt = context.mainLightLookAt[2];

            lightshaftObj[1].SetActive(true);
        }
        else if (num == 13) {
            context.StateMachine.ChangeState(SecMirrorRotState.GetInstance());
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryMirrorState : IState<GCTelescope> {
    static SecondaryMirrorState instance;
    TelescopePartController secondaryMirrorPart;
    StateNotifications notifTexts;

    float stateEntryTime;

    private SecondaryMirrorState() {

    }

    internal static SecondaryMirrorState GetInstance() {
        if (instance == null) {
            instance = new SecondaryMirrorState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        secondaryMirrorPart = context.secMirrorPart;
        secondaryMirrorPart.gameObject.SetActive(true);
        //secondaryMirrorPart.HidePart();

        context.cameraController.Position = context.secMirrorCamPos;
        context.cameraController.BezierPoint = context.secMirrorBez;
        context.cameraController.LookAt = context.secMirrorLookAt;
        context.cameraController.TravelTime = 4f;
        context.cameraController.speed = 4f;
        context.cameraController.rotSpeed = 7f;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 5) {
            context.StateMachine.ChangeState(FocuserState.GetInstance());
        }
    }
}

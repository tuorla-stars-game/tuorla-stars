﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonRotatorState : IState<GCTelescope> {

    static MoonRotatorState instance;
    StateNotifications notifTexts;

    float stateEntryTime;

    private MoonRotatorState() {

    }

    internal static MoonRotatorState GetInstance() {
        if (instance == null) {
            instance = new MoonRotatorState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;
        context.cameraController.Position = context.moonCamPos;
        context.cameraController.BezierPoint = context.moonBez;
        context.cameraController.LookAt = context.moonLookAt;
        context.cameraController.TravelTime = 3f;
        context.cameraController.speed = 3f;
        context.cameraController.rotSpeed = 5f;

        context.mrController.enabled = true;
        context.uiController.moonRotControls.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        context.uiController.moonRotControls.SetActive(false);

    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 10) {
            context.StateMachine.ChangeState(MainLightState.GetInstance());
        }
        else if (num == 999) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);
        }
    }
}

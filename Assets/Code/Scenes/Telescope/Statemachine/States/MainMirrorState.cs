﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMirrorState : IState<GCTelescope> {
    static MainMirrorState instance;
    TelescopePartController MainMirrorPart;
    StateNotifications notifTexts;

    float stateEntryTime;

    private MainMirrorState() {
    }

    internal static MainMirrorState GetInstance() {
        if (instance == null) {
            instance = new MainMirrorState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        MainMirrorPart = context.mainMirrorPart;
        MainMirrorPart.gameObject.SetActive(true);
        MainMirrorPart.HidePart();

        context.cameraController.Position = context.mainMirrorCamPos;
        context.cameraController.BezierPoint = context.mainMirrorBez;
        context.cameraController.LookAt = context.mainMirrorLookAt;
        context.cameraController.TravelTime = 2f;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 4) {
            context.StateMachine.ChangeState(SecondaryMirrorState.GetInstance());
        }
    }
}

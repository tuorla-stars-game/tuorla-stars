﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecMirrorRotState : IState<GCTelescope> {
    static SecMirrorRotState instance;
    GameObject[] secMirrorRotObjects;
    StateNotifications notifTexts;

    float stateEntryTime;

    private SecMirrorRotState() {

    }

    internal static SecMirrorRotState GetInstance() {
        if (instance == null) {
            instance = new SecMirrorRotState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;
        secMirrorRotObjects = context.secMirrorRotObjects;
        secMirrorRotObjects[1].SetActive(true);

        context.cameraController.Position = context.secMirrorRotCamPos;
        context.cameraController.LookAt = context.secMirrorRotLookAt;
        context.smrController.enabled = true;
        context.uiController.secMirrorRotControls.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        secMirrorRotObjects[0].SetActive(false);
        context.uiController.secMirrorRotControls.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 14) {
            context.StateMachine.ChangeState(OcularLightState.GetInstance());
        }
        else if (num == 999) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);
        }
    }
}

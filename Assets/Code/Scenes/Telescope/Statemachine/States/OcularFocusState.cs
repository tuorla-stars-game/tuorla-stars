﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcularFocusState : IState<GCTelescope> {
    static OcularFocusState instance;
    GameObject[] focusObjs;
    StateNotifications notifTexts;

    float stateEntryTime;
    float targetFOV;
    Vector3 targetCamRot;
    Camera sceneCam;

    private OcularFocusState() {

    }

    internal static OcularFocusState GetInstance() {
        if (instance == null) {
            instance = new OcularFocusState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        focusObjs = context.ocularFocusObjects;

        focusObjs[0].SetActive(true);
        focusObjs[1].SetActive(true);

        context.cameraController.Position = context.ocularFocusCamPos;
        context.cameraController.LookAt = context.ocularFocusLookAt;
        context.cameraController.rotSpeed = 0f;
        context.cameraController.AllowPlayerAdjust = false;

        context.uiController.gameControlsOcular.SetActive(true);

        targetFOV = focusObjs[2].GetComponent<Camera>().fieldOfView;
        targetCamRot = focusObjs[2].transform.localEulerAngles;

        sceneCam = context.cameraController.GetComponent<Camera>();

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        context.uiController.gameControlsOcular.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        float time = Time.timeSinceLevelLoad - stateEntryTime;

        if (time > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }

        if (sceneCam.transform.localEulerAngles != targetCamRot) {
            Vector3 _targetRot = sceneCam.transform.localEulerAngles;
            _targetRot.z = 60f;
            _targetRot = Vector3.Lerp(sceneCam.transform.localEulerAngles, _targetRot, time / 10f);
            sceneCam.transform.localRotation = Quaternion.Euler(_targetRot);
            _targetRot = sceneCam.transform.localEulerAngles;
            _targetRot.y = 90f;
            sceneCam.transform.localRotation = Quaternion.Euler(_targetRot);
        }

        if (sceneCam.fieldOfView != targetFOV) {
            float _targetFOV = Mathf.Lerp(sceneCam.fieldOfView, targetFOV, time / 60f);
            sceneCam.fieldOfView = _targetFOV;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 18) {
            context.dataHandler.IsSolarGameUnlocked = true;
            context.finishScreen.SetActive(true);
            context.finishScreen.GetComponent<ScreenEnabler>().ElementDisplay();
        }
        else if (num == 999) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);
        }
    }
}

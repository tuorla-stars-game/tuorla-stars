﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsState : IState<GCTelescope> {
    static DoorsState instance;
    StateNotifications notifTexts;

    float stateEntryTime;

    private DoorsState() {

    }

    internal static DoorsState GetInstance() {
        if (instance == null) {
            instance = new DoorsState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        context.cameraController.Position = context.doorsCamPos;
        context.cameraController.LookAt = context.doorsLookAt;
        context.cameraController.speed = 6f;
        context.cameraController.rotSpeed = 2f;

        context.doorsController.enabled = true;
        context.uiController.doorsOpenButton.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        context.uiController.doorsOpenButton.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 9) {
            context.StateMachine.ChangeState(MoonRotatorState.GetInstance());
        }
    }
}

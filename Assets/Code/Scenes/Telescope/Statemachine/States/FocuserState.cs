﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocuserState : IState<GCTelescope> {
    static FocuserState instance;
    TelescopePartController[] focuserParts;
    StateNotifications notifTexts;

    float stateEntryTime;

    private FocuserState() {

    }

    public static FocuserState GetInstance() {
        if (instance == null) {
            instance = new FocuserState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        focuserParts = context.focuserParts;
        

        foreach (TelescopePartController focuserPart in focuserParts) {
            focuserPart.gameObject.SetActive(true);
            focuserPart.HidePart();
        }

        context.cameraController.Position = context.focuserCamPos;
        context.cameraController.BezierPoint = context.focuserBez;
        context.cameraController.LookAt = context.focuserLookAt;
        context.cameraController.TravelTime = 3f;
        context.cameraController.speed = 2f;
        context.cameraController.rotSpeed = 3.5f;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 6) {
            foreach (TelescopePartController focuserPart in focuserParts) {
                focuserPart.DisplayPart();
            }

            context.StateMachine.ChangeState(OcularState.GetInstance());
        }
    }


}

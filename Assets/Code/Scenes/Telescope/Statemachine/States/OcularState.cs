﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcularState : IState<GCTelescope> {
    static OcularState instance;
    TelescopePartController[] ocularParts;
    StateNotifications notifTexts;

    float stateEntryTime;

    private OcularState() {

    }

    internal static OcularState GetInstance() {
        if (instance == null) {
            instance = new OcularState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        ocularParts = context.ocularParts;

        foreach (TelescopePartController ocularPart in ocularParts) {
            ocularPart.gameObject.SetActive(true);
            ocularPart.HidePart();
        }

        context.cameraController.Position = context.ocularCamPos;
        context.cameraController.BezierPoint = context.ocularBez;
        context.cameraController.LookAt = context.ocularLookAt;

        context.cameraController.TravelTime = 2f;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 7) {
            foreach (TelescopePartController ocularPart in ocularParts) {
                ocularPart.DisplayPart();
            }

            context.StateMachine.ChangeState(BaseRotState.GetInstance());
        }
    }
}

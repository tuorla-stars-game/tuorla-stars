﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRotState : IState<GCTelescope> {
    static BaseRotState instance;
    StateNotifications notifTexts;

    float stateEntryTime;

    private BaseRotState() {

    }

    internal static BaseRotState GetInstance() {
        if (instance == null) {
            instance = new BaseRotState();
        }

        return instance;
    }

    public void StateEnter(GCTelescope context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        context.cameraController.Position = context.baseRotCamPos;
        context.cameraController.BezierPoint = context.baseRotBez;
        context.cameraController.LookAt = context.baseRotLookAt;
        context.cameraController.TravelTime = 6f;
        context.cameraController.speed = 6f;
        context.cameraController.rotSpeed = 9f;

        context.teleRotController.enabled = true;
        context.uiController.baseRotSlider.SetActive(true);

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Telescope, context.loc);
    }

    public void StateExit(GCTelescope context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
        context.uiController.baseRotSlider.SetActive(false);
    }

    public void StateUpdate(GCTelescope context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }
    }

    public void MessageToState(GCTelescope context, int num) {
        if (num == 8) {
            context.StateMachine.ChangeState(DoorsState.GetInstance());
        }
        else if (num == 999) {
            context.uiController.AddNotification(notifTexts.STATE_PROGRESS[0]);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T> {
    T owner;
    IState<T> currentState;

	internal IState<T> CurrentState {
        get {
            return currentState;
        }
    }

    internal StateMachine(T owner) {
        this.owner = owner;
    }

    internal void Update() {
        if (currentState != null) {
            currentState.StateUpdate(owner);
        }
    }

    internal void ChangeState(IState<T> newState) {
        Debug.Log("state changing");
        if (currentState != null) {
            Debug.Log("calling stateexit " + currentState.ToString());
            currentState.StateExit(owner);
        }

        currentState = newState;
        if (currentState != null) {
            Debug.Log("calling stateenter" + currentState.ToString());
            currentState.StateEnter(owner);
        }
        Debug.Log("state change done");
    }

    internal void MessageToState(int num) {
        currentState.MessageToState(owner, num);
    }
}

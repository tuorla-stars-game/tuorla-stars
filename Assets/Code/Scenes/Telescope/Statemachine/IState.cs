﻿public interface IState<T> {
    void StateEnter(T context);
    void StateUpdate(T context);
    void StateExit(T context);
    void MessageToState(T context, int num);
}

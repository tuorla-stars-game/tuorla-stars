﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GCQuiz : GameController {
    internal static GCQuiz quizInstance;
    internal LocalizedQuizStrings localizedStrings;

    [SerializeField]
    Text quizFinishScreenText;
    [SerializeField]
    Text quizFinishGrats;

    protected override void CleanUpBeforeLoad() {
    }

    protected override void SubApplyLocalization() {
        localizedStrings = LocalizationDataLoader.LoadLocalizedQuizData(loc);
    }

    protected override void SubAwake() {
        if (quizInstance == null) {
            quizInstance = this;
        }
        else {
            Destroy(gameObject);
        }

        dataHandler = DataHandler.instance;
    }

    protected override void SubStart() {
    }

    protected override void SubUpdate() {
    }

    internal override UIController GetUIController() {
        return GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIControllerQuiz>();
    }

    internal void QuizFinishDisplay(int correct, int totalQuestions) {
        quizFinishScreenText.transform.parent.gameObject.SetActive(true);
        quizFinishGrats.text = localizedStrings.FINISH_SCREEN_CONGRATULATIONS;
        quizFinishScreenText.text = localizedStrings.FINISH_SCREEN_START + " " + correct + "/" + totalQuestions + " " + localizedStrings.FINISH_SCREEN_REST;
    }

    public void CloseQuizFinish() {
        uiController.PlayButtonSFX();
        quizFinishScreenText.transform.parent.gameObject.SetActive(false);
    }
}

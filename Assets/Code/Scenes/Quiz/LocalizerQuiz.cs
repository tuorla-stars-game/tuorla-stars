﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizerQuiz : MonoBehaviour {
    [SerializeField]
    Text QUESTION_ANSWER_CORRECT;
    [SerializeField]
    Text QUESTION_ANSWER_INCORRECT;

    [SerializeField]
    Text FINISH_SCREEN_CONGRATULATIONS;
    [SerializeField]
    Text FINISH_SCREEN_START;
    [SerializeField]
    Text FINISH_SCREEN_REST;

    void OnEnable() {
        LocalizedQuizStrings lqs = GCQuiz.quizInstance.localizedStrings;

        if (QUESTION_ANSWER_CORRECT != null) {
            QUESTION_ANSWER_CORRECT.text = lqs.QUESTION_ANSWER_CORRECT;
        }
        if (QUESTION_ANSWER_INCORRECT != null) {
            QUESTION_ANSWER_INCORRECT.text = lqs.QUESTION_ANSWER_INCORRECT;
        }

        if (FINISH_SCREEN_CONGRATULATIONS != null) {
            FINISH_SCREEN_CONGRATULATIONS.text = lqs.FINISH_SCREEN_CONGRATULATIONS;
        }
        if (FINISH_SCREEN_START != null) {
            FINISH_SCREEN_START.text = lqs.FINISH_SCREEN_START;
        }
        if (FINISH_SCREEN_REST != null) {
            FINISH_SCREEN_REST.text = lqs.FINISH_SCREEN_REST;
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionController : MonoBehaviour {
    QuizQuestion[] questions;
    Loc loc;
    LocalizedQuizStrings localizedStrings;

    [SerializeField]
    int numberOfQuestions = 15;
    [SerializeField]
    Text question;
    [SerializeField]
    Text[] answers;

    [SerializeField]
    Color correctColor;
    [SerializeField]
    Color incorrectColor;
    Color startColor;

    [SerializeField]
    Text questionCounter;
    [SerializeField]
    Text correctQuestions;
    [SerializeField]
    Text incorrectQuestions;

    [SerializeField]
    int loadDelay = 2;

    int currentQuestion = 0;
    int[] questionSet;

    int correct = 0;
    int incorrect = 0;
    bool isWaitingForQuestion = false;
    bool hasAnswered = false;

	void Start () {
        loc = DataHandler.instance.Localization;
        localizedStrings = GCQuiz.quizInstance.localizedStrings;

        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Quiz/quiz");
        QuizQuestionRaw[] questionsRaw = JSONHelper.getJsonArray<QuizQuestionRaw>(jsonFile.text);

        questions = new QuizQuestion[questionsRaw.Length];
        for (int i = 0; i < questions.Length; i++) {
            questions[i] = new QuizQuestion(questionsRaw[i], loc);
        }

        if (numberOfQuestions > questions.Length) {
            numberOfQuestions = questions.Length;
        }
        startColor = answers[0].GetComponentInParent<Image>().color;

        InitializeQuiz();
    }

    void RandomizeArray<T>(T[] arr) {
        for (var i = arr.Length - 1; i > 0; i--) {
            int r = UnityEngine.Random.Range(0, i);
            T tmp = arr[i];
            arr[i] = arr[r];
            arr[r] = tmp;
        }
    }

    void DisplayQuestion() {
        questionCounter.text = (currentQuestion + 1) + "/" + questionSet.Length;
        hasAnswered = false;
        isWaitingForQuestion = false;

        foreach (Text answer in answers) {
            answer.GetComponentInParent<Image>().color = startColor;
        }

        QuizQuestion qQuestion = questions[questionSet[currentQuestion]];
        question.text = qQuestion.question;

        for (int i = 0; i < answers.Length; i++) {
            answers[i].text = qQuestion.answers[i];
        }
    }

    public void AnswerQuestion(int answerNum) {
        QuizQuestion qQuestion = questions[questionSet[currentQuestion]];

        if (!isWaitingForQuestion) {
            GameController.instance.uiController.PlayButtonSFX();

            Image aImage = answers[answerNum - 1].GetComponentInParent<Image>();
            if (answerNum == qQuestion.rightAnswer) {
                aImage.color = correctColor;
                if (!hasAnswered) {
                    correct++;
                }

                isWaitingForQuestion = true;
                answers[answerNum - 1].text = answers[answerNum - 1].text + " " + localizedStrings.QUESTION_ANSWER_CORRECT;
            }
            else {
                if (!hasAnswered) {
                    incorrect++;
                }
                aImage.color = incorrectColor;
                answers[answerNum - 1].text = answers[answerNum - 1].text + " " + localizedStrings.QUESTION_ANSWER_INCORRECT;
            }

            hasAnswered = true;

            correctQuestions.text = correct + "/" + questionSet.Length;
            incorrectQuestions.text = incorrect + "/" + questionSet.Length;
        }
        else {
            if (answerNum == qQuestion.rightAnswer) {
                currentQuestion++;

                if (currentQuestion >= numberOfQuestions) {
                    GCQuiz.quizInstance.QuizFinishDisplay(correct, numberOfQuestions);
                    return;
                }

                isWaitingForQuestion = false;

                DisplayQuestion();
            }
        }
    }

    public void ResetButton() {
        correct = 0;
        incorrect = 0;
        currentQuestion = 0;

        correctQuestions.text = correct + "/" + questionSet.Length;
        incorrectQuestions.text = incorrect + "/" + questionSet.Length;

        InitializeQuiz();
    }

    internal void InitializeQuiz() {
        questionSet = new int[numberOfQuestions];

        RandomizeArray(questions);

        for (int i = 0; i < questionSet.Length; i++) {
            questionSet[i] = i;
        }

        RandomizeArray(questionSet);
        DisplayQuestion();
    }
}


[Serializable]
public struct QuizQuestionRaw {
    public int id;
    public string question_fi;
    public string question_en;
    public string question_sv;
    public string[] relatedCard;
    public string[] answer_fi;
    public string[] answer_en;
    public string[] answer_sv;
    public int rightAnswer;
}

internal struct QuizQuestion {
    internal int id;
    internal string question;
    internal CardData cheatCard;
    internal string[] answers;
    internal int rightAnswer;

    internal QuizQuestion(QuizQuestionRaw qqr, Loc loc) {
        id = qqr.id;

        switch (loc) {
            case Loc.Finnish:
                question = qqr.question_fi;
                answers = qqr.answer_fi;
                break;
            case Loc.Swedish:
                question = qqr.question_sv;
                answers = qqr.answer_sv;
                break;
            default:
                question = qqr.question_en;
                answers = qqr.answer_en;
                break;
        }

        rightAnswer = qqr.rightAnswer;
        cheatCard = new CardData() {
            category = qqr.relatedCard[0]
        };
        int.TryParse(qqr.relatedCard[1], out cheatCard.ID);
    }
}
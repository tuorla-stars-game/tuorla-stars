﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class UIControllerQuiz : UIController {
    protected override GameController GetCameController() {
        return GCQuiz.instance;
    }

    protected override void SubAwake() {

    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }

    internal override void ClearUIElements() {

    }
}

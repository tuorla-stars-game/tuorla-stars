﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CardMiscController : CardController {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;
    [SerializeField]
    Image image;

    Sprite defaultSprite;

    internal override void SubclassStart() {
        maxCards = library.GetNumCards(CardLibrary.miscString, true);

        myCard = CardHelper.IndexToCardNumber(zIndex, zIndexMax);

        defaultSprite = image.sprite;

        if (myCard < 1) {
            myCard = maxCards + myCard;
        }

        SubLoadCard(myCard);
    }

    internal override void SubLoadCard(int cardID) {
        MiscCardLocalized thisCard = library.GetMiscCardLocalizedByIndex(cardID);

        header.text = thisCard.header;
        body.text = thisCard.body;

        if (thisCard.imagePath != "") {
            image.sprite = Resources.Load<Sprite>(thisCard.imagePath);
        }
        else {
            image.sprite = defaultSprite;
        }
    }
}

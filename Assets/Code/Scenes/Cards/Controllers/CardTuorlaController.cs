﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CardTuorlaController : CardController {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;
    [SerializeField]
    Image image;
    [SerializeField]
    GameObject cardHider;

    Sprite defaultSprite;

    internal override void SubclassStart() {
        maxCards = library.GetNumCards(CardLibrary.tuorlaString, true);

        myCard = CardHelper.IndexToCardNumber(zIndex, zIndexMax);

        defaultSprite = image.sprite;

        if (myCard < 1) {
            myCard = maxCards + myCard;
        }

        SubLoadCard(myCard);
    }

    internal override void SubLoadCard(int cardID) {
        TuorlaCardLocalized thisCard = library.GetTuorlaCardLocalizedByIndex(cardID);
        if (dataHandler.HasCard(CardLibrary.tuorlaString, thisCard.ID)) {
            cardHider.SetActive(false);
            body.text = thisCard.body;
            header.text = thisCard.header;
        }
        else {
            cardHider.SetActive(true);
        }

        if (thisCard.imagePath != "") {
            image.sprite = Resources.Load<Sprite>(thisCard.imagePath);
        }
        else {
            image.sprite = defaultSprite;
        }
    }
}

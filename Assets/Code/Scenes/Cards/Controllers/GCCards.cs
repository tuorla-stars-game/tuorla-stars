﻿using System;
using UnityEngine;

internal class GCCards : GameController {
    new internal static GCCards instance;

    protected override void CleanUpBeforeLoad() {
    }

    protected override void SubApplyLocalization() {

    }

    protected override void SubAwake() {
        instance = this;
        dataHandler = DataHandler.instance;
    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }

    internal override UIController GetUIController() {
        return GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIControllerCards>();
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CardTelescopeController : CardController {
    [SerializeField]
    Text header;
    [SerializeField]
    Text body;
    [SerializeField]
    Image image;
    [SerializeField]
    Text headerTwo;
    [SerializeField]
    GameObject cardHider;

    Sprite defaultSprite;

    internal override void SubclassStart() {
        defaultSprite = image.sprite;

        maxCards = library.GetNumCards(CardLibrary.telescopeString, true);

        myCard = CardHelper.IndexToCardNumber(zIndex, zIndexMax);

        if (myCard < 1) {
            myCard = maxCards + myCard;
        }

        SubLoadCard(myCard);
    }

    internal override void SubLoadCard(int cardID) {
        TelescopeCardLocalized thisCard = library.GetTelescopeCardLocalizedByIndex(cardID);

        headerTwo.text = "";

        if (dataHandler.HasCard(CardLibrary.telescopeString, thisCard.ID)) {
            cardHider.SetActive(false);
            header.text = thisCard.header;
            if (cardID >= 2 && cardID <= 6) {
                headerTwo.text = thisCard.header;
            }
        }
        else {
            cardHider.SetActive(true);
            header.text = "???????";
        }
        body.text = thisCard.body;

        if (thisCard.imagePath != "") {
            image.sprite = Resources.Load<Sprite>(thisCard.imagePath);
        }
        else {
            image.sprite = defaultSprite;
        }
    }
}

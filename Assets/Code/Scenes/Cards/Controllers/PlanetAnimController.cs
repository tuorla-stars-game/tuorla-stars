﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetAnimController : MonoBehaviour {
    RawImage myImage;

    [SerializeField]
    float rotSpeedDamper = 20f;

	// Use this for initialization
	void Awake () {
        myImage = GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update () {
        Rect uvRect = myImage.uvRect;
        uvRect.x -= Time.deltaTime / rotSpeedDamper;
        myImage.uvRect = uvRect;
	}

    internal void SetTextureAndRotation(string path, float rot) {
        myImage.texture = Resources.Load(path) as Texture;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        transform.Rotate(new Vector3(0f, 0f, 360f - rot));
    }
}

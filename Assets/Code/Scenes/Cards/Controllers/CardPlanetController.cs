﻿using UnityEngine;
using UnityEngine.UI;

public class CardPlanetController : CardController {
    [SerializeField]
    Text header;

    [SerializeField]
    RawImage planetSlot;

    [SerializeField]
    Text rotationPeriod;

    [SerializeField]
    Text orbitalPeriod;

    [SerializeField]
    Text diameter;

    [SerializeField]
    Text moons;

    [SerializeField]
    Text mass;

    [SerializeField]
    Text axialTilt;

    [SerializeField]
    Text body;

    [SerializeField]
    PlanetCardTemperatureController pcTempController;

    [SerializeField]
    PlanetCardOrderController pcOrderController;

    [SerializeField]
    PlanetAnimController pAnimController;

    [SerializeField]
    GameObject cardHider;

    [SerializeField]
    Text astronomicalUnitHelp;

    [SerializeField]
    Text rotationPeriodHelp;

    [SerializeField]
    Text orbitalPeriodHelp;

    [SerializeField]
    Text diameterHelp;

    [SerializeField]
    Text moonsHelp;

    [SerializeField]
    Text axialTiltHelp;

    [SerializeField]
    Text massHelp;

    internal override void SubclassStart() {
        maxCards = library.GetNumCards(CardLibrary.planetsString, true);

        myCard = CardHelper.IndexToCardNumber(zIndex, zIndexMax);

        if (myCard < 1) {
            myCard = maxCards + myCard;
        }

        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Cards/json/planets_help_tab");
        PlanetCardHelpRaw pchr = JsonUtility.FromJson<PlanetCardHelpRaw>(jsonFile.text);
        PlanetCardHelp pcHelp = new PlanetCardHelp(pchr);
        int loc = (int)GCCards.instance.loc;
        astronomicalUnitHelp.text = pcHelp.ASTRONOMICAL_UNIT[loc];
        rotationPeriodHelp.text = pcHelp.ROTATION_PERIOD[loc];
        orbitalPeriodHelp.text = pcHelp.ORBITAL_PERIOD[loc];
        diameterHelp.text = pcHelp.DIAMETER[loc];
        moonsHelp.text = pcHelp.MOONS[loc];
        axialTiltHelp.text = pcHelp.AXIAL_TILT[loc];
        massHelp.text = pcHelp.MASS[loc];

        SubLoadCard(myCard);
    }

    internal override void SubLoadCard(int cardID) {
        PlanetCardLocalized thisData = library.GetPlanetCardLocalizedByIndex(myCard);

        if (dataHandler.HasCard(CardLibrary.planetsString, thisData.ID)) {
            header.text = thisData.header;

            body.text = thisData.body;

            rotationPeriod.text = thisData.rotationPeriod;
            orbitalPeriod.text = thisData.orbitalPeriod;
            diameter.text = thisData.diameter;
            moons.text = thisData.satellites.ToString();
            mass.text = thisData.mass;
            axialTilt.text = thisData.axialTilt;
            pcTempController.SetTempTexts(thisData.temperatureMin.ToString(),
                                          thisData.temperatureMax.ToString());

            pcTempController.SetTempGraphics(thisData.temperatureMin,
                                             thisData.temperatureMax);

            pcOrderController.ResetPlanetGraphic();
            pcOrderController.SetPlanetGraphic(thisData.planetOrder,
                                               thisData.distance);
            cardHider.SetActive(false);
        }
        else {
            header.text = "?";
            cardHider.SetActive(true);
        }

        pAnimController.SetTextureAndRotation(thisData.imagePath, thisData.axialTiltNum);
    }
}

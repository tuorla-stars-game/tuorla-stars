﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsController : MonoBehaviour {
    float touchTracker = 0f;
    float touchTimer = 0f;
    float carouselPos = 0f;
    float oldCarouselPos = 0f;
    Vector2 touchStartPos = Vector2.zero;
    bool isCarouselMovementAllowed = true;
    bool wasCarouselMovementAllowed = true;
    CardController[] cards;
    Touch lastTouch;

    [SerializeField]
    float moveSpeed = 100f;

    [SerializeField]
    float carouselMoveForce = 0f;

    [SerializeField]
    float cardGapMultiplier = 2f;

    [SerializeField]
    float scaleDamper = 0.75f;

    [SerializeField]
    float centerThreshold = 0.5f;

    [SerializeField]
    float centerSpeed = 5f;

    void Start () {
        cards = GetComponentsInChildren<CardController>();
        float width = GetComponent<RectTransform>().rect.width;
        float cardGap = CardHelper.CalculateCardGap(width, cards.Length - 1, cardGapMultiplier);

        foreach (CardController card in cards) {
            card.CardGap = cardGap;
            card.ScaleMultiplier = scaleDamper;
        }
    }

    void Update () {
        HandleInput();

        if (Mathf.Abs(carouselMoveForce) > 0f) {
            carouselMoveForce *= 0.95f;

            if (Mathf.Abs(carouselMoveForce) < centerThreshold) {
                carouselMoveForce = 0f;
            }
        }

        if (Mathf.Abs(carouselMoveForce) < centerThreshold && touchStartPos == Vector2.zero) {
            Vector3 cardOnTopPos = transform.GetChild(cards.Length - 1).localPosition;

            if (cardOnTopPos.x != 0f) {
                Vector3 newCardPos = Vector3.MoveTowards(cardOnTopPos, Vector3.zero,
                                                         Time.deltaTime * moveSpeed * centerSpeed);

                foreach (CardController card in cards) {
                    card.MoveBy(newCardPos.x - cardOnTopPos.x);
                }
            }
        }

        if (isCarouselMovementAllowed) {
            float lastPos = carouselPos;
            carouselPos += carouselMoveForce;

            foreach (CardController card in cards) {
                card.MoveBy(carouselPos - lastPos);
            }
        }
    }

    void HandleInput() {
        if (Input.touchCount == 1) {
            TouchSingleHandle();
        }
    }

    void TouchSingleHandle() {
        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began) {
            carouselMoveForce = 0f;
            touchStartPos = touch.position;
        }
        else if (touch.phase == TouchPhase.Moved) {

        }
        else if (touch.phase == TouchPhase.Stationary) {

        }
        else if (touch.phase == TouchPhase.Ended) {
        }

        touchTracker += touch.deltaPosition.x;
        touchTimer += touch.deltaTime;

        if (isCarouselMovementAllowed) {
            float lastPos = carouselPos;
            float _carouselPos = (touch.position.x - touchStartPos.x) / moveSpeed;
            //carouselPos += touch.deltaPosition.x * Time.deltaTime * moveSpeed;
            float moveBy = _carouselPos - oldCarouselPos;

            if (moveBy != _carouselPos) {

                foreach (CardController card in cards) {
                    card.MoveBy(moveBy);
                }

                carouselPos += moveBy;
            }

            oldCarouselPos = _carouselPos;

            if (touch.phase == TouchPhase.Ended) {
                oldCarouselPos = 0f;
                touchStartPos = Vector2.zero;
                TouchSingleEndHandle(moveBy);
            }
        }

        lastTouch = touch;
        wasCarouselMovementAllowed = isCarouselMovementAllowed;
    }

    void TouchSingleEndHandle(float lastTouchAmount) {
        if (isCarouselMovementAllowed && wasCarouselMovementAllowed) {
            carouselMoveForce = lastTouchAmount;
        }

        touchTracker = 0f;
        touchTimer = 0f;
    }

    GameObject[] GetChildren() {
        GameObject[] children = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++) {
            children[i] = transform.GetChild(i).gameObject;
        }

        return children;
    }

    public void SetCarouselMovementPermission(bool perm) {
        isCarouselMovementAllowed = perm;
    }
}

﻿using UnityEngine;

public abstract class CardController : MonoBehaviour {
    float panelWidth;
    float teleportThreshold;
    float cardGap;
    float scaleDamper;

    protected DataHandler dataHandler;

    internal int maxCards;
    internal int zIndex;
    internal int zIndexMax;
    internal int myCard = 0;
    internal CardLibrary library;

    internal float CardGap {
        set {
            cardGap = value;
            Vector3 pos = transform.localPosition;
            int cardNumber = CardHelper.IndexToCardNumber(zIndex, zIndexMax);
            pos.x = cardNumber * cardGap;
            transform.localPosition = pos;

            teleportThreshold = (CardHelper.IndexToCardNumber(0, zIndexMax) - 1) * cardGap + cardGap / 2f;
            teleportThreshold = Mathf.Abs(teleportThreshold);
        }
    }

    internal float ScaleMultiplier {
        set { scaleDamper = value; }
    }

    void Awake() {
        SubAwake();
    }

    protected virtual void SubAwake() {

    }

    void Start () {
        zIndex = transform.GetSiblingIndex();
        zIndexMax = transform.parent.childCount - 1;
        panelWidth = transform.parent.GetComponent<RectTransform>().rect.width;
        library = CardLibrary.instance;
        dataHandler = DataHandler.instance;

        SubclassStart();
    }

    internal abstract void SubclassStart();

    internal void MoveBy(float amount) {
        Vector3 pos = transform.localPosition;
        pos.x += amount;

        if (pos.x >= teleportThreshold) {
            float overflow = pos.x - teleportThreshold;
            pos.x = -teleportThreshold + overflow;
            LoadPrevCard();
        }
        else if (pos.x < -teleportThreshold) {
            float overflow = pos.x + teleportThreshold;
            pos.x = teleportThreshold + overflow;
            LoadNextCard();
        }

        for (int i = 0; i <= zIndexMax; i++) {
            float thresholdLeft = (i - 0.5f) * cardGap;
            float thresholdRight = (i + 0.5f) * cardGap;

            if (Mathf.Abs(pos.x) >= thresholdLeft && Mathf.Abs(pos.x) < thresholdRight) {
                zIndex = CardHelper.CardNumberToIndex((int)(Mathf.Sign(pos.x) * i), zIndexMax);
                break;
            }
        }

        transform.SetSiblingIndex(zIndex);
        transform.localPosition = pos;

        Vector3 scale = (1f - pos.magnitude / (scaleDamper * panelWidth)) * Vector3.one;
        transform.localScale = scale;
    }

    void LoadNextCard() {
        int myBefore = myCard;
        myCard += zIndexMax + 1;
        if (myCard > maxCards) {
            myCard = myCard - maxCards;
        }

        SubLoadCard(myCard);
    }

    void LoadPrevCard() {
        myCard -= 5;
        if (myCard < 1) {
            myCard = maxCards + myCard;
        }

        SubLoadCard(myCard);
    }

    internal abstract void SubLoadCard(int cardID);
}

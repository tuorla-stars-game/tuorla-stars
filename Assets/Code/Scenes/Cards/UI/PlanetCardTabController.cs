﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PCTab {
    Data,
    Text,
    Help
}

public class PlanetCardTabController : MonoBehaviour {
    Image myImage;
    PCTab currentTab = PCTab.Data;

    [SerializeField]
    GameObject[] dataTab;

    [SerializeField]
    GameObject[] textTab;

    [SerializeField]
    GameObject[] helpTab;

    [SerializeField]
    Sprite dataActiveImg;

    [SerializeField]
    Sprite textActiveImg;

    [SerializeField]
    Sprite helpActiveImg;

	void Start () {
        myImage = GetComponent<Image>();
	}

    void DisableCurrentTab() {
        switch (currentTab) {
            case PCTab.Data:
                foreach (GameObject gobj in dataTab) {
                    gobj.SetActive(false);
                }
                break;
            case PCTab.Text:
                foreach (GameObject gobj in textTab) {
                    gobj.SetActive(false);
                }
                break;
            case PCTab.Help:
                foreach (GameObject gobj in helpTab) {
                    gobj.SetActive(false);
                }
                break;
            default:
                break;
        }
    }

    void ActivateTab(PCTab tab) {
        switch (tab) {
            case PCTab.Data:
                foreach (GameObject gobj in dataTab) {
                    gobj.SetActive(true);
                }
                myImage.sprite = dataActiveImg;
                break;
            case PCTab.Text:
                foreach (GameObject gobj in textTab) {
                    gobj.SetActive(true);
                }
                myImage.sprite = textActiveImg;
                break;
            case PCTab.Help:
                foreach (GameObject gobj in helpTab) {
                    gobj.SetActive(true);
                }
                myImage.sprite = helpActiveImg;
                break;
            default:
                break;
        }
    }

    public void SwitchTab(int tab) {
        PCTab _tab = (PCTab)tab;

        if (_tab != currentTab) {
            DisableCurrentTab();
            ActivateTab(_tab);
            currentTab = _tab;
        }
    }
}

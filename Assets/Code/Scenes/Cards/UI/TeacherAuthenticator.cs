﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeacherAuthenticator : MonoBehaviour {
    const string hashSalt = "arcturus";
    Button myButton;
    Image myImage;
    InputField myInput;

    [SerializeField]
    Color authed;
    [SerializeField]
    Color notAuthed;
    [SerializeField]
    Color workingColor;

    void Start() {
        myButton = GetComponent<Button>();
        myImage = GetComponent<Image>();
    }

    public void SendPassword(InputField input) {
        GCMap.instance.uiController.PlayButtonSFX();
        myInput = input;
        string hash = hashSalt + input.text;
        hash = Md5Sum(hash);

        WebHelper.OnAuthReady += HandleAuthResults;
        StartCoroutine(WebHelper.AuthenticateUser(hash));

        input.GetComponent<Image>().color = workingColor;
        myImage.color = workingColor;
        myButton.GetComponentInChildren<Text>().text = "Working...";
    }

    internal void HandleAuthResults(bool auth) {
        WebHelper.OnAuthReady -= HandleAuthResults;

        if (auth) {
            myInput.interactable = false;
            myButton.interactable = false;

            myInput.GetComponent<Image>().color = authed;
            myImage.color = authed;

            myButton.GetComponentInChildren<Text>().text = "Authed! Yes!";

            GCMap.instance.MakeTeacher();
        }
        else {
            myImage.color = notAuthed;
            myButton.GetComponentInChildren<Text>().text = "Try again?";
            myInput.GetComponent<Image>().color = Color.white;
        }
    }

    public void ClosePrompt() {
        GCMap.instance.uiController.PlayButtonSFX();
        transform.parent.gameObject.SetActive(false);
    }

    string Md5Sum(string strToEncrypt) {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++) {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class PlanetCardOrderController : MonoBehaviour {
    [SerializeField]
    Color activeColor;
    Color passiveColor;

    Image[] planetImgs;

    int lastPlanet = 0;

    RectTransform lineGraphic;
    Text lineText;

    const float startingPoint = 18.8f;
    const float planetStep = 30f;

	void Awake () {
        passiveColor = transform.GetChild(2).GetComponent<Image>().color;
        lineGraphic = transform.GetChild(0).GetComponent<RectTransform>();
        lineText = lineGraphic.GetComponentInChildren<Text>();

        planetImgs = new Image[8];
        for (int i = 0; i < 8; i++) {
            planetImgs[i] = transform.GetChild(i + 2).GetComponent<Image>();
        }
	}

    internal void ResetPlanetGraphic() {
        for (int i = 0; i < planetImgs.Length; i++) {
            if (planetImgs[i].color != passiveColor) {
                planetImgs[i].color = passiveColor;
            }
        }

        lineText.text = "";
        lineGraphic.offsetMax = new Vector2(-257f, lineGraphic.offsetMax.y);
    }

    internal void SetPlanetGraphic(int planetNum, string distFromSun) {
        Transform thisPlanet = transform.GetChild(planetNum + 1);
        Image planetImg = thisPlanet.GetComponent<Image>();

        planetImg.color = activeColor;

        float right = startingPoint + (8 - planetNum) * planetStep;

        lineGraphic.offsetMax = new Vector2(-right, lineGraphic.offsetMax.y);
        lineText.text = distFromSun;
        lastPlanet = planetNum;
    }
}

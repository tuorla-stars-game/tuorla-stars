﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetCardTemperatureController : MonoBehaviour {
    [SerializeField]
    Transform leftBar;

    [SerializeField]
    Transform rightBar;

    [SerializeField]
    Transform lowBar;

    [SerializeField]
    Transform highBar;

    [SerializeField]
    RectTransform rangeGraphic;

    Text lowText;
    Text highText;

    const float tempRange = 870f;
    const float tempRangeMin = 270f;
    float rightMax;
    float barRange;

	// Use this for initialization
	void Awake () {
        lowText = lowBar.GetComponentInChildren<Text>();
        highText = highBar.GetComponentInChildren<Text>();

        barRange = Mathf.Abs(leftBar.localPosition.x);
        barRange += Mathf.Abs(rightBar.localPosition.x);

        Rect rect = transform.GetChild(0).GetComponent<RectTransform>().rect;
        rightMax = rect.xMax * 2f;
    }

    internal void SetTempTexts(string minTemp, string maxTemp) {
        lowText.text = minTemp;
        highText.text = maxTemp;
    }

    internal void SetTempGraphics(float minTemp, float maxTemp) {
        float lowBarPos = (minTemp + tempRangeMin) / tempRange;
        lowBarPos *= barRange;
        lowBarPos += leftBar.localPosition.x;

        float highBarPos = (maxTemp + tempRangeMin) / tempRange;
        highBarPos *= barRange;
        highBarPos += leftBar.localPosition.x;

        Vector3 pos = lowBar.localPosition;
        pos.x = lowBarPos;
        lowBar.localPosition = pos;

        pos = highBar.localPosition;
        pos.x = highBarPos;
        highBar.localPosition = pos;

        float graphPosTemp = (minTemp + tempRangeMin) / tempRange;
        graphPosTemp *= rightMax;
        rangeGraphic.offsetMin = new Vector2(graphPosTemp, rangeGraphic.offsetMin.y);

        graphPosTemp = 1 - (maxTemp + tempRangeMin) / tempRange;
        graphPosTemp *= rightMax;
        rangeGraphic.offsetMax = new Vector2(-graphPosTemp, rangeGraphic.offsetMax.y);
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.UI;

internal class UIControllerCards : UIController {
    new GCCards gc;

    protected override GameController GetCameController() {
        return GCCards.instance;
    }

    protected override void SubAwake() {
        gc = GCCards.instance;
    }

    protected override void SubStart() {

    }

    protected override void SubUpdate() {

    }

    internal override void ClearUIElements() {
    }
}

﻿using System;
using System.Text;

[Serializable]
public struct UnitLocalizations {
    public string[] comma;
    public string[] kilometer;
    public string[] day;
    public string[] earthMass;
    public string[] AU;
    public string[] hour;
    public string[] kg;
    public string[] year;
}

/* #################################################
 * TUORLA CARD STRUCTURES HERE
 * #################################################
 */

[Serializable]
public struct TuorlaMeta {
    public int ID;
    public string category;
    public string imagePath;
    public bool isIncluded;
}

[Serializable]
public struct TuorlaCard {
    public int ID;
    public string[] header;
    public string[] body;

    public TuorlaCard(TuorlaCardRaw tcRaw) {
        ID = tcRaw.ID;
        header = new string[Enum.GetNames(typeof(Loc)).Length];
        header[(int)Loc.Finnish] = tcRaw.header_fi;
        header[(int)Loc.English] = tcRaw.header_en;
        header[(int)Loc.Swedish] = tcRaw.header_sv;

        body = new string[Enum.GetNames(typeof(Loc)).Length];
        body[(int)Loc.Finnish] = tcRaw.body_fi;
        body[(int)Loc.English] = tcRaw.body_en;
        body[(int)Loc.Swedish] = tcRaw.body_sv;
    }
}

[Serializable]
public struct TuorlaCardRaw {
    public int ID;
    public string header_fi;
    public string header_en;
    public string header_sv;
    public string body_fi;
    public string body_en;
    public string body_sv;
}

internal struct TuorlaCardLocalized {
    internal int ID;
    internal string header;
    internal string body;
    internal string category;
    internal string imagePath;
    internal bool isIncluded;

    internal TuorlaCardLocalized(Loc loc, TuorlaMeta meta, TuorlaCard card) {
        ID = meta.ID;
        imagePath = meta.imagePath;
        category = meta.category;

        int _loc = (int)loc;
        header = card.header[_loc];
        body = card.body[_loc];

        isIncluded = meta.isIncluded;
    }
}

[Serializable]
public struct PlanetMeta {
    public int ID;
    public bool isIncluded;
    public string imagePath;
    public string diameter;
    public string distance;
    public int planetOrder;
    public string rotationPeriod;
    public string orbitalPeriod;
    public string mass;
    public int temperatureMin;
    public int temperatureAverage;
    public int temperatureMax;
    public string axialTilt;
    public int satellites;
}

/* #################################################
 * PLANET CARD STRUCTURES HERE
 * #################################################
 */

[Serializable]
public struct PlanetCard {
    public int ID;
    public string[] header;
    public string[] body;

    public PlanetCard(PlanetCardRaw pcRaw) {
        ID = pcRaw.ID;
        header = new string[Enum.GetNames(typeof(Loc)).Length];
        header[(int)Loc.Finnish] = pcRaw.header_fi;
        header[(int)Loc.English] = pcRaw.header_en;
        header[(int)Loc.Swedish] = pcRaw.header_sv;

        body = new string[Enum.GetNames(typeof(Loc)).Length];
        body[(int)Loc.Finnish] = pcRaw.body_fi;
        body[(int)Loc.English] = pcRaw.body_en;
        body[(int)Loc.Swedish] = pcRaw.body_sv;
    }
}

[Serializable]
public struct PlanetCardRaw {
    public int ID;
    public string header_fi;
    public string header_en;
    public string header_sv;
    public string body_fi;
    public string body_en;
    public string body_sv;
}

internal struct PlanetCardLocalized {
    internal int ID;
    public bool isIncluded;
    internal string header;
    internal string imagePath;
    internal string body;

    internal string diameter;
    internal string distance;
    internal int planetOrder;
    internal string rotationPeriod;
    internal string orbitalPeriod;
    internal string mass;
    internal int temperatureMin;
    internal int temperatureAverage;
    internal int temperatureMax;
    internal string axialTilt;
    internal int satellites;

    internal float axialTiltNum;

    internal PlanetCardLocalized(Loc loc, UnitLocalizations unitLocs, PlanetMeta meta, PlanetCard card) {
        ID = meta.ID;
        planetOrder = meta.planetOrder;
        imagePath = meta.imagePath;
        satellites = meta.satellites;

        int _loc = (int)loc;

        header = card.header[_loc];
        body = card.body[_loc];

        StringBuilder builder = new StringBuilder(meta.axialTilt);

        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        float.TryParse(builder.ToString().Replace(',', '.'),
                       System.Globalization.NumberStyles.Any,
                       System.Globalization.CultureInfo.InvariantCulture,
                       out axialTiltNum);

        builder = builder.Append('°');
        axialTilt = builder.ToString();

        builder = new StringBuilder(meta.diameter);

        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        builder = builder.Replace("%km", unitLocs.kilometer[_loc]);
        diameter = builder.ToString();

        builder = new StringBuilder(meta.distance);
        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        builder = builder.Replace("%AU", unitLocs.AU[_loc]);
        distance = builder.ToString();

        builder = new StringBuilder(meta.mass);
        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        builder = builder.Replace("%earthMass", unitLocs.earthMass[_loc]);
        builder = builder.Replace("%kg", unitLocs.kg[_loc]);
        mass = builder.ToString();

        builder = new StringBuilder(meta.orbitalPeriod);
        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        builder = builder.Replace("%d", unitLocs.day[_loc]);
        builder = builder.Replace("%a", unitLocs.year[_loc]);
        orbitalPeriod = builder.ToString();

        builder = new StringBuilder(meta.rotationPeriod);
        builder = builder.Replace("%,", unitLocs.comma[_loc]);
        builder = builder.Replace("%h", unitLocs.hour[_loc]);
        builder = builder.Replace("%d", unitLocs.day[_loc]);
        rotationPeriod = builder.ToString();

        temperatureAverage = meta.temperatureAverage;
        temperatureMax = meta.temperatureMax;
        temperatureMin = meta.temperatureMin;

        isIncluded = meta.isIncluded;
    }
}

/* #################################################
* MISC CARD STRUCTURES HERE
* #################################################
*/

[Serializable]
public struct MiscMeta {
    public int ID;
    public bool isIncluded;
    public string category;
    public string imagePath;
}

[Serializable]
public struct MiscCard {
    public int ID;
    public string[] header;
    public string[] body;

    public MiscCard(MiscCardRaw mcRaw) {
        ID = mcRaw.ID;
        header = new string[Enum.GetNames(typeof(Loc)).Length];
        header[(int)Loc.Finnish] = mcRaw.header_fi;
        header[(int)Loc.English] = mcRaw.header_en;
        header[(int)Loc.Swedish] = mcRaw.header_sv;

        body = new string[Enum.GetNames(typeof(Loc)).Length];
        body[(int)Loc.Finnish] = mcRaw.body_fi;
        body[(int)Loc.English] = mcRaw.body_en;
        body[(int)Loc.Swedish] = mcRaw.body_sv;
    }
}

[Serializable]
public struct MiscCardRaw {
    public int ID;
    public string header_fi;
    public string header_en;
    public string header_sv;
    public string body_fi;
    public string body_en;
    public string body_sv;
}

internal struct MiscCardLocalized {
    internal int ID;
    public bool isIncluded;
    internal string header;
    internal string body;
    internal string category;
    internal string imagePath;

    internal MiscCardLocalized(Loc loc, MiscMeta meta, MiscCard card) {
        ID = meta.ID;
        imagePath = meta.imagePath;
        category = meta.category;

        int _loc = (int)loc;
        header = card.header[_loc];
        body = card.body[_loc];
        isIncluded = meta.isIncluded;
    }
}

/* #################################################
* TELESCOPE CARD STRUCTURES HERE
* #################################################
*/

[Serializable]
public struct TelescopeMeta {
    public int ID;
    public bool isIncluded;
    public string category;
    public string imagePath;
    public string subType;
}

[Serializable]
public struct TelescopeCard {
    public int ID;
    public string[] header;
    public string[] body;

    public TelescopeCard(TelescopeCardRaw tcRaw) {
        ID = tcRaw.ID;
        header = new string[Enum.GetNames(typeof(Loc)).Length];
        header[(int)Loc.Finnish] = tcRaw.header_fi;
        header[(int)Loc.English] = tcRaw.header_en;
        header[(int)Loc.Swedish] = tcRaw.header_sv;

        body = new string[Enum.GetNames(typeof(Loc)).Length];
        body[(int)Loc.Finnish] = tcRaw.body_fi;
        body[(int)Loc.English] = tcRaw.body_en;
        body[(int)Loc.Swedish] = tcRaw.body_sv;
    }
}

[Serializable]
public struct TelescopeCardRaw {
    public int ID;
    public string header_fi;
    public string header_en;
    public string header_sv;
    public string body_fi;
    public string body_en;
    public string body_sv;
}

internal struct TelescopeCardLocalized {
    internal int ID;
    public bool isIncluded;
    internal string header;
    internal string body;
    internal string category;
    internal string imagePath;
    internal string subType;

    internal TelescopeCardLocalized(Loc loc, TelescopeMeta meta, TelescopeCard card) {
        ID = meta.ID;
        imagePath = meta.imagePath;
        category = meta.category;
        subType = meta.subType;

        int _loc = (int)loc;
        header = card.header[_loc];
        body = card.body[_loc];

        isIncluded = meta.isIncluded;
    }
}

/*
 * Planet card help section data structure here
 * */

[Serializable]
public struct PlanetCardHelpRaw {
    public string ASTRONOMICAL_UNIT_FI;
    public string ASTRONOMICAL_UNIT_EN;
    public string ASTRONOMICAL_UNIT_SV;
    public string ROTATION_PERIOD_FI;
    public string ROTATION_PERIOD_EN;
    public string ROTATION_PERIOD_SV;
    public string ORBITAL_PERIOD_FI;
    public string ORBITAL_PERIOD_EN;
    public string ORBITAL_PERIOD_SV;
    public string DIAMETER_FI;
    public string DIAMETER_EN;
    public string DIAMETER_SV;
    public string MOONS_FI;
    public string MOONS_EN;
    public string MOONS_SV;
    public string AXIAL_TILT_FI;
    public string AXIAL_TILT_EN;
    public string AXIAL_TILT_SV;
    public string MASS_FI;
    public string MASS_EN;
    public string MASS_SV;
}

internal struct PlanetCardHelp {
    internal string[] ASTRONOMICAL_UNIT;
    internal string[] ROTATION_PERIOD;
    internal string[] ORBITAL_PERIOD;
    internal string[] DIAMETER;
    internal string[] MOONS;
    internal string[] AXIAL_TILT;
    internal string[] MASS;

    internal PlanetCardHelp(PlanetCardHelpRaw pchr) {
        int length = Enum.GetNames(typeof(Loc)).Length;
        ASTRONOMICAL_UNIT = new string[length];
        ROTATION_PERIOD = new string[length];
        ORBITAL_PERIOD = new string[length];
        DIAMETER = new string[length];
        MOONS = new string[length];
        AXIAL_TILT = new string[length];
        MASS = new string[length];

        ASTRONOMICAL_UNIT[(int)Loc.Finnish] = pchr.ASTRONOMICAL_UNIT_FI;
        ASTRONOMICAL_UNIT[(int)Loc.English] = pchr.ASTRONOMICAL_UNIT_EN;
        ASTRONOMICAL_UNIT[(int)Loc.Swedish] = pchr.ASTRONOMICAL_UNIT_SV;
        ROTATION_PERIOD[(int)Loc.Finnish] = pchr.ROTATION_PERIOD_FI;
        ROTATION_PERIOD[(int)Loc.English] = pchr.ROTATION_PERIOD_EN;
        ROTATION_PERIOD[(int)Loc.Swedish] = pchr.ROTATION_PERIOD_SV;
        ORBITAL_PERIOD[(int)Loc.Finnish] = pchr.ORBITAL_PERIOD_FI;
        ORBITAL_PERIOD[(int)Loc.English] = pchr.ORBITAL_PERIOD_EN;
        ORBITAL_PERIOD[(int)Loc.Swedish] = pchr.ORBITAL_PERIOD_SV;
        DIAMETER[(int)Loc.Finnish] = pchr.DIAMETER_FI;
        DIAMETER[(int)Loc.English] = pchr.DIAMETER_EN;
        DIAMETER[(int)Loc.Swedish] = pchr.DIAMETER_SV;
        MOONS[(int)Loc.Finnish] = pchr.MOONS_FI;
        MOONS[(int)Loc.English] = pchr.MOONS_EN;
        MOONS[(int)Loc.Swedish] = pchr.MOONS_SV;
        AXIAL_TILT[(int)Loc.Finnish] = pchr.AXIAL_TILT_FI;
        AXIAL_TILT[(int)Loc.English] = pchr.AXIAL_TILT_EN;
        AXIAL_TILT[(int)Loc.Swedish] = pchr.AXIAL_TILT_SV;
        MASS[(int)Loc.Finnish] = pchr.MASS_FI;
        MASS[(int)Loc.English] = pchr.MASS_EN;
        MASS[(int)Loc.Swedish] = pchr.MASS_SV;
    }
}
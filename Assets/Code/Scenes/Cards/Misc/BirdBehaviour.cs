﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBehaviour : MonoBehaviour {
    Animator animator;

    [SerializeField]
    float minHeight = -6f;

    [SerializeField]
    float maxHeight = 10f;

    [SerializeField]
    float flapBoost = 1f;

    [SerializeField]
    float glideChance = 20f;

    [SerializeField]
    float flapChance = 10f;

    [SerializeField]
    float moveSpeed = 5f;

    [SerializeField]
    float glideFallSpeed = 0.1f;

    int lastGlideUpdatesAgo = 0;
    int lastFlapUpdatesAgo = 0;
    int nextUpdate = 1;
    bool isGliding = false;
    Vector3 targetPos;

    void Awake() {
        animator = GetComponent<Animator>();
        targetPos = transform.localPosition;
    }

    void Start() {
        StartGliding();
    }

    void Update () {
        if (Time.time > nextUpdate) {
            float glideMoti = lastGlideUpdatesAgo * (Random.value * glideChance / 100f);
            float flapMoti = lastFlapUpdatesAgo * (Random.value * flapChance / 100f);

            if (flapMoti > glideMoti) {
                StartFlapping();
                lastFlapUpdatesAgo = 1;
                lastGlideUpdatesAgo++;
            }
            else {
                StartGliding();
                lastGlideUpdatesAgo = 1;
                lastFlapUpdatesAgo++;
            }

            if (transform.localPosition.y < minHeight) {
                StartFlapping();
            }
            else if (transform.localPosition.y > maxHeight) {
                StartGliding();
            }

            if (isGliding) {
                targetPos -= transform.up * glideFallSpeed;
            }

            nextUpdate = Mathf.FloorToInt(Time.time + 1);
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition,
                                               targetPos,
                                               Time.deltaTime * moveSpeed);
	}

    void StartFlapping() {
        animator.SetBool("flap", true);
        animator.SetBool("glide", false);
        isGliding = false;
    }

    void StartGliding() {
        animator.SetBool("glide", true);
        animator.SetBool("flap", false);
        isGliding = true;
    }

    public void WingFlapEvent() {
        targetPos += transform.up * flapBoost;
    }
}

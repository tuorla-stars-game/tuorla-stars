﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActiveBrowser {
    Tuorla,
    Planet,
    Misc,
    Telescope
}

public class CardBrowserChanger : MonoBehaviour {
    [SerializeField]
    GameObject[] cardBrowsers;

    [SerializeField]
    float moveSpeed = 50f;

    CardsController[] cardsControllers;
    bool moving = false;

    [SerializeField]
    ActiveBrowser currentlyActive;

	// Use this for initialization
	void Awake () {
        cardsControllers = new CardsController[cardBrowsers.Length];

        for (int i = 0; i < cardsControllers.Length; i++) {
            cardsControllers[i] = cardBrowsers[i].GetComponent<CardsController>();
        }
	}

    void Start() {
        SetActiveBrowser((int)currentlyActive);
    }

    // Update is called once per frame
    void Update () {
        Vector3 pos = transform.localPosition;
        float activePos = NumberToCoord((int)currentlyActive);

        if (Vector3.Distance(pos, new Vector3(activePos, 0f, 0f)) > 1f) {
            Vector3 newPos = Vector3.zero;
            newPos.x = activePos;
            transform.localPosition = Vector3.Lerp(pos, newPos, Time.deltaTime * moveSpeed);
            newPos = transform.localPosition;
        }
        else {
            if (moving) {
                for (int i = 0; i < cardBrowsers.Length; i++) {
                    if (i != (int)currentlyActive) {
                        cardBrowsers[i].SetActive(false);
                    }
                }

                pos = transform.localPosition;
                pos.x = activePos;
                transform.localPosition = pos;
                moving = false;
            }
        }
	}

    float NumberToCoord(int x) {
        return (-x + 1) * 1400f;
    }

    public void SetActiveBrowser(int x) {
        currentlyActive = (ActiveBrowser)x;

        foreach (CardsController cc in cardsControllers) {
            cc.enabled = false;
            cc.gameObject.SetActive(true);
        }

        cardsControllers[x].enabled = true;
        moving = true;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputHandlerSolar : MonoBehaviour {
    NotificationsController notifs;
    GCSolar gc;
    UIControllerSolar uiController;
    SolarCameraController cameraController;

    float touchTimer = 0f;
    float touchTracker = 0f;
    float maxTapTime = 0.15f;
    bool touchStartedOverUIObject;
    bool isTwoTouch = false;
    Vector2 touchStartPos;
    POPlanet carryingPlanet;

    [SerializeField]
    bool runThing = false;

    void Start() {
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();

        gc = GCSolar.instance;
        cameraController = gc.cameraController;
        uiController = GetComponent<UIControllerSolar>();
    }

    void Update() {
        if (Input.touchCount == 1) {
            TouchSingleHandle();
        }
        else if (Input.touchCount > 1) {
            TouchMultiHandle(Input.touchCount);
        }

        if (carryingPlanet != null && Input.touchCount == 0) {
            carryingPlanet = null;
        }
    }

    void TouchSingleHandle() {
        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began) {
            touchTimer = 0f;
            touchTracker = 0f;
            touchStartedOverUIObject = InputHelper.IsPointerOverUIObject();
            isTwoTouch = false;
            touchStartPos = touch.position;
        }
        else if (touch.phase == TouchPhase.Moved) {

        }
        else if (touch.phase == TouchPhase.Stationary) {

        }
        else if (touch.phase == TouchPhase.Ended) {
            if (!isTwoTouch) {
                TouchSingleEndHandle(touch.position);
            }

            isTwoTouch = false;
            touchStartedOverUIObject = false;
            return;
        }

        if (carryingPlanet != null) {
            carryingPlanet.transform.position = touch.position;
        }

        if (touchStartedOverUIObject) {
        }
        else {
            cameraController.TouchOrbitPos += touch.deltaPosition.x;
            cameraController.TouchHeightPos -= touch.deltaPosition.y;
        }

        touchTimer += Time.deltaTime;
        touchTracker += touch.deltaPosition.magnitude;
    }

    void TouchSingleEndHandle(Vector2 pos) {
        if (carryingPlanet != null) {
            GraphicRaycaster gr = GetComponent<GraphicRaycaster>();
            PointerEventData ped = new PointerEventData(null);
            ped.position = pos;

            List<RaycastResult> results = new List<RaycastResult>();
            gr.Raycast(ped, results);

            if (results.Count > 0) {
                foreach (RaycastResult res in results) {
                    POPlanet poPlanet = res.gameObject.GetComponent<POPlanet>();

                    if (poPlanet != null) {
                        poPlanet.PlacePlanet(carryingPlanet);
                        carryingPlanet = null;
                        return;
                    }
                }
            }

            bool playResetAnim = true;
            if (touchTimer < 0.3f) {
                uiController.csController.AddCardToStack(CardLibrary.planetsString,
                                                         carryingPlanet.planetOrderNumber + 1,
                                                         false);
                playResetAnim = false;
            }

            carryingPlanet.ResetPos(playResetAnim);
            carryingPlanet = null;
        }

        cameraController.isTouching = false;
    }

    void TouchMultiHandle(int amount) {
        if (amount == 2) {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            float deltaDeltaTouch = InputHelper.TouchTwoDeltaDeltaCalc(touch1, touch2);

            if (touch2.phase == TouchPhase.Began) {
                isTwoTouch = true;
            }
            else if (touch2.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved) {
                cameraController.AdjustCameraZoon(deltaDeltaTouch);
            }
            else if (touch1.phase == TouchPhase.Ended || touch2.phase == TouchPhase.Ended) {

            }
        }
    }

    internal void StartCarryingPlanet(POPlanet planet) {
        if (Input.touchCount == 1) {
            carryingPlanet = planet;
        }
    }
}

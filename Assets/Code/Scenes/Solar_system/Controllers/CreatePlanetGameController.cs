﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatePlanetGameController : MonoBehaviour {
    [SerializeField]
    GameObject sampleSolarParticle;

    [SerializeField]
    int numberOfParticles = 20;

    [SerializeField]
    float pickupDistance = 300f;

    [SerializeField]
    SpinnersController spinController;

    [SerializeField]
    bool spoof = false;

    [SerializeField]
    int numParticlesInSpinnerToEnd = 100;

    [SerializeField]
    float fadeSpeed = 1f;

    [SerializeField]
    float badAreaRadius = 300f;

    [SerializeField]
    float spinnerCaptureRadius = 100f;

    [SerializeField]
    Color tintRangeStart;

    [SerializeField]
    Color tintRangeEnd;

    [SerializeField]
    float scaleVariation = 0.2f;

    [SerializeField]
    CanvasGroup cgParticles;

    [SerializeField]
    CanvasGroup cgSpinners;

    [SerializeField]
    Transform planetTransform;

    [SerializeField]
    GameObject sun2DGraphic;

    [SerializeField]
    float gravityStrength = 1f;

    [SerializeField]
    Sprite[] particleSprites;



    CanvasGroup cgSun;
    CanvasGroup cgMe;
    SolarParticle[] solarParticles;
    int leftAlive = -1;
    bool gameDone = false;
    bool fadeSpinners = false;
    bool fadeTheRest = false;
    float spinnersValues = 1f;

    internal bool GameFinished {
        get {
            return gameDone;
        }
        set {
            gameDone = value;
        }
    }

	void Start () {
        //cgParticles = GetComponentInChildren<CanvasGroup>();
        cgSun = GCSolar.instance.container.sunAnimScaler.GetComponent<CanvasGroup>();
        cgMe = GetComponent<CanvasGroup>();
        Transform newParent = cgParticles.transform;
        int centerX = Screen.width / 2;
        int centerY = Screen.height / 2;
        Vector3 center = new Vector3(Screen.width / 2, Screen.height / 2, 0f);

        solarParticles = new SolarParticle[numberOfParticles];
        for (int i = 0; i < numberOfParticles; i++) {
            solarParticles[i] = Instantiate(sampleSolarParticle, newParent).GetComponent<SolarParticle>();
            solarParticles[i].gameObject.SetActive(true);
            solarParticles[i].enabled = true;

            Vector3 pos = RNGPos();
            solarParticles[i].transform.position = pos;

            int randomSprite = Random.Range(0, particleSprites.Length);
            Image solarImage = solarParticles[i].GetComponent<Image>();
            solarImage.sprite = particleSprites[randomSprite];

            float rng = Random.value;
            solarImage.color = Color.Lerp(tintRangeStart, tintRangeEnd, rng);

            rng = 1f + (Random.value * scaleVariation - scaleVariation / 2f);
            solarImage.transform.localScale *= rng;
        }

        spinController.numParticlesToEnd = numParticlesInSpinnerToEnd;
        leftAlive = numberOfParticles;
    }

    void Update() {
        float delta = Time.deltaTime;

        if (leftAlive != -1) {
            Vector3 touchPos = Vector3.zero;
            if (Input.touchCount > 0) {
                touchPos = Input.GetTouch(0).position;
            }

            foreach (SolarParticle particle in solarParticles) {
                if (particle != null && !particle.IsCaptured) {
                    Vector3 vector = Vector3.zero;

                    if (touchPos != Vector3.zero) {
                        vector = touchPos - particle.transform.position;
                    }

                    Vector3 distFromSpinner = spinController.transform.position - particle.transform.position;

                    if (touchPos != Vector3.zero) {
                        /*if (vector.magnitude < 1f && leftAlive > (numberOfParticles - (spinController.MaxParticlesSpinner + 20))) {
                            Destroy(particle.gameObject);
                            leftAlive--;
                        }*/
                        if (vector.magnitude < pickupDistance) {
                            particle.MoveVector = vector;
                            particle.DistanceFactor = 1f;
                        }
                        else if (vector.magnitude < pickupDistance * 2f) {
                            particle.MoveVector = vector;
                            particle.DistanceFactor = 1f - (vector.magnitude - pickupDistance) / pickupDistance;
                        }
                        else {
                            particle.MoveVector = Vector3.zero;
                        }
                    }

                    particle.transform.position = Vector3.MoveTowards(particle.transform.position,
                                                        spinController.transform.position,
                                                        delta * gravityStrength);


                    if (distFromSpinner.magnitude < spinnerCaptureRadius) {
                        particle.IsCaptured = true;
                        spinController.AddToSpinner(particle);
                    }
                }
            }

            if (gameDone) {
                if (cgParticles != null) {
                    cgParticles.alpha -= Time.deltaTime * fadeSpeed;

                    if (cgParticles.alpha < 0.05f) {
                        Destroy(cgParticles.gameObject);
                        fadeSpinners = true;
                    }
                }

                if (fadeSpinners) {
                    spinnersValues -= (delta * fadeSpeed);
                    cgSpinners.alpha = spinnersValues;

                    cgSpinners.transform.localScale *= (1f - (spinnersValues / 100f));

                    if (planetTransform.localScale.x < 2f) {
                        planetTransform.localScale += Vector3.one * ((1f - spinnersValues) / 10f);
                    }

                    if (cgSpinners.alpha < 0.05f) {
                        Destroy(cgSpinners.gameObject);
                        fadeTheRest = true;
                        fadeSpinners = false;
                    }
                }
                else if (fadeTheRest) {
                    cgMe.alpha -= delta * fadeSpeed;
                    cgSun.alpha -= delta * fadeSpeed;

                    if (cgMe.alpha < 0.05f) {
                        cgMe.alpha = 0f;
                        cgSun.alpha = 0f;
                        fadeTheRest = false;
                        this.enabled = false;
                    }
                }

            }
            else {
                if (cgParticles.alpha < 1f) {
                    cgParticles.alpha += delta * fadeSpeed;
                }
            }
        }
    }

    internal void DestroyMe() {
        Destroy(sun2DGraphic);
        Destroy(gameObject);
    }

    Vector3 RNGPos() {
        Vector3 rPos = new Vector3(0.5f, 0.5f, 0f);
        Vector3 center = new Vector3(0.5f, 0.5f, 0f);

        while (Vector3.Distance(rPos, center) < badAreaRadius) {
            rPos.x = Random.value;
            rPos.y = Random.value;
        }

        rPos.x *= Screen.width;
        rPos.y *= Screen.height;
        return rPos;
    }
}

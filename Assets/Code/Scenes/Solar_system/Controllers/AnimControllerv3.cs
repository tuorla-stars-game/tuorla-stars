﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControllerv3 : MonoBehaviour {
    [SerializeField]
    Texture[] frames;
    [SerializeField]
    float fps;
    [SerializeField]
    float fluctuation = 5f;
    [SerializeField]
    float fluctuationSpeed = 3f;

    float frameTime = 0f;
    Material myMat;
    int currentFrame = 0;
    float timer = 0f;

	void Start () {
        myMat = GetComponent<MeshRenderer>().material;
        frameTime = 1f / fps;
	}
	
	void Update () {
        timer += Time.deltaTime;
        if (timer >= frameTime) {
            currentFrame++;

            if (currentFrame >= frames.Length) {
                currentFrame = 0;
            }

            timer = 0f;
            myMat.mainTexture = frames[currentFrame];
            myMat.SetTexture("_EmissionMap", frames[currentFrame]);
            currentFrame++;
        }
        Color myEmission;
        myEmission.a = myEmission.b = myEmission.g = myEmission.r = myEmission.a = (Mathf.Sin(fluctuationSpeed * Time.realtimeSinceStartup) + fluctuation) / fluctuation + 1f / fluctuation;
        myMat.SetColor("_EmissionColor", myEmission);
    }
}

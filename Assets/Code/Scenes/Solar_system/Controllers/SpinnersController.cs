﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinnersController : MonoBehaviour {
    [SerializeField]
    internal SunSpinner centerSpinner;
    [SerializeField]
    internal SunSpinner innerSpinner;
    [SerializeField]
    internal SunSpinner outerSpinner;

    [SerializeField]
    int maxParticlesCenter = 10;
    [SerializeField]
    int maxParticlesInner = 30;
    [SerializeField]
    int maxParticlesOuter = 100;

    int particlesInCenter = 0;
    int particlesInInner = 0;
    int particlesInOuter = 0;
    bool stateNotified = false;

    internal int numParticlesToEnd = 100;

    internal int MaxParticlesSpinner {
        get {
            return maxParticlesCenter + maxParticlesInner + maxParticlesOuter;
        }
    }

    internal void AddToSpinner(SolarParticle particle) {
        if (particlesInCenter < maxParticlesCenter) {
            centerSpinner.AddToSpinner(particle);
            particlesInCenter++;
        }
        else if (particlesInInner < maxParticlesInner) {
            if (particlesInInner == 0) {
                GCSolar.instance.AdvanceGameProgress();
            }
            innerSpinner.AddToSpinner(particle);
            particlesInInner++;
        }
        else if (particlesInOuter < maxParticlesOuter) {
            if (particlesInOuter == 0) {
                GCSolar.instance.AdvanceGameProgress();
            }
            outerSpinner.AddToSpinner(particle);
            particlesInOuter++;
        }
        else {
            Destroy(particle.gameObject);
        }

        if (particlesInCenter + particlesInInner + particlesInOuter > numParticlesToEnd && !stateNotified) {
            GCSolar.instance.AdvanceGameProgress();
            stateNotified = true;
        }
    }
}

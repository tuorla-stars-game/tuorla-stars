﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GCSolar : GameController {
    new internal static GCSolar instance;
    new internal UIControllerSolar uiController;
    NotificationsController notifs;
    internal SolarCameraController cameraController;
    internal StateMachine<GCSolar> gameState;
    internal LocalizedSolarStrings localizedStrings;

    bool wasPlayerTutorialed = false;
    int playerGameProgress = 0;

    [SerializeField]
    bool progressPhase = false;
    [SerializeField]
    internal float tutorialMsgWait = 2f;
    [SerializeField]
    internal GameObject solarTutorial;
    [SerializeField]
    internal int startStateNum = 0;
    [SerializeField]
    GameObject ignitionGame;
    [SerializeField]
    GameObject planetCreateGame;
    [SerializeField]
    GameObject planetOrderGame;
    [SerializeField]
    LocalizerSolar localizer;

    internal SolarStateContainer container;

    internal bool TutorialMsgSent {
        get {
            return wasPlayerTutorialed;
        }
        set {
            wasPlayerTutorialed = value;
        }
    }

    protected override void CleanUpBeforeLoad() {

    }

    protected override void SubAwake() {
        instance = this;
        dataHandler = DataHandler.instance;

        uiController = GameObject.FindGameObjectWithTag("Canvas")
                         .GetComponent<UIControllerSolar>();

        container = GetComponentInChildren<SolarStateContainer>();


        cameraController = GameObject.FindGameObjectWithTag("MainCamera")
                                     .GetComponent<SolarCameraController>();
    }

    protected override void SubStart() {
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();

        gameState = new StateMachine<GCSolar>(this);

        if (startStateNum == 0) {
            startStateNum = dataHandler.SolarGameProgress;
        }

        if (startStateNum >= 19) {
            Destroy(ignitionGame);
            Destroy(planetCreateGame);
            Destroy(planetOrderGame);
            Destroy(container.sunAnimator.gameObject);
            playerGameProgress = startStateNum;
            gameState.ChangeState(SolarSystemBrowserState.GetInstance(playerGameProgress));
            return;
        }
        else if (startStateNum >= 10) {
            Destroy(ignitionGame);
            Destroy(planetCreateGame);
            Destroy(container.sunAnimator.gameObject);
            playerGameProgress = startStateNum;
            gameState.ChangeState(PlanetOrderingState.GetInstance(playerGameProgress));
            return;
        }

        gameState.ChangeState(SolarIgnitionState.GetInstance(playerGameProgress));

        notifs.AcceptNewNotifications = false;
        while (playerGameProgress < startStateNum) {
            AdvanceGameProgress();
        }
        notifs.AcceptNewNotifications = true;
    }

    protected override void SubUpdate() {
        if (progressPhase) {
            AdvanceGameProgress();
            progressPhase = false;
        }

        gameState.Update();
    }

    internal override UIController GetUIController() {
        return GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIControllerSolar>();
    }

    internal void MessageToState(int num) {
        gameState.MessageToState(num);
    }

    internal void AdvanceGameProgress() {
        playerGameProgress++;
        dataHandler.SolarGameProgress = playerGameProgress;
        MessageToState(playerGameProgress);
    }

    internal void DestroyGO(GameObject go) {
        Destroy(go);
    }

    protected override void SubApplyLocalization() {
        localizedStrings = LocalizationDataLoader.LoadLocalizedSolarData(loc);
        localizer.enabled = true;
    }

    public void CloseTutorial() {
        GCSolar.instance.uiController.PlayButtonSFX();

        solarTutorial.SetActive(false);
        dataHandler.SetTutorialPlayed(Scenes.Solar);
    }
}

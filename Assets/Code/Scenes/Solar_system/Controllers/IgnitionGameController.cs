using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IgnitionGameController : MonoBehaviour {
    [SerializeField]
    GameObject sampleSolarParticle;

    [SerializeField]
    int numberOfParticles = 20;

    [SerializeField]
    float pickupDistance = 300f;

    [SerializeField]
    SpinnersController spinController;

    [SerializeField]
    bool spoof = false;

    [SerializeField]
    int numParticlesInSpinnerToEnd = 100;

    [SerializeField]
    float fadeSpeed = 1f;

    [SerializeField]
    float spinnerCaptureRadius = 100f;

    [SerializeField]
    Color tintRangeStart;

    [SerializeField]
    Color tintRangeEnd;

    [SerializeField]
    float scaleVariation = 0.2f;

    [SerializeField]
    CanvasGroup cgParticles;

    [SerializeField]
    CanvasGroup cgSpinners;

    [SerializeField]
    float gravityStrength = 1f;

    [SerializeField]
    Sprite[] particleSprites;


    SolarParticle[] solarParticles;
    int leftAlive = -1;
    bool gameDone = false;
    bool fadeSpinners = false;

    internal bool GameFinished {
        get {
            return gameDone;
        }
        set {
            gameDone = value;
        }
    }

	void Start () {
        //cgParticles = GetComponentInChildren<CanvasGroup>();
        Transform newParent = cgParticles.transform;

        solarParticles = new SolarParticle[numberOfParticles];
        for (int i = 0; i < numberOfParticles; i++) {
            solarParticles[i] = Instantiate(sampleSolarParticle, newParent).GetComponent<SolarParticle>();
            solarParticles[i].gameObject.SetActive(true);
            solarParticles[i].enabled = true;
            solarParticles[i].transform.position = RNGPos();
            int randomSprite = Random.Range(0, particleSprites.Length);
            Image solarImage = solarParticles[i].GetComponent<Image>();
            solarImage.sprite = particleSprites[randomSprite];

            float rng = Random.value;
            solarImage.color = Color.Lerp(tintRangeStart, tintRangeEnd, rng);

            rng = 1f + (Random.value * scaleVariation - scaleVariation / 2f);
            solarImage.transform.localScale *= rng;
        }

        spinController.numParticlesToEnd = numParticlesInSpinnerToEnd;
        leftAlive = numberOfParticles;
    }

    void Update () {
        if (leftAlive != -1) {

            Vector3 touchPos = Vector3.zero;
            if (Input.touchCount > 0) {
                touchPos = Input.GetTouch(0).position;
            }

            foreach (SolarParticle particle in solarParticles) {
                if (particle != null && !particle.IsCaptured) {
                    Vector3 vector = Vector3.zero;

                    if (touchPos != Vector3.zero) {
                        vector = touchPos - particle.transform.position;
                    }

                    Vector3 distFromSpinner = spinController.transform.position - particle.transform.position;

                    if (touchPos != Vector3.zero) {
                        /*if (vector.magnitude < 1f && leftAlive > (numberOfParticles - (spinController.MaxParticlesSpinner + 20))) {
                            Destroy(particle.gameObject);
                            leftAlive--;
                        }*/
                        if (vector.magnitude < pickupDistance) {
                            particle.MoveVector = vector;
                            particle.DistanceFactor = 1f;
                        }
                        else if (vector.magnitude < pickupDistance * 2f) {
                            particle.MoveVector = vector;
                            particle.DistanceFactor = 1f - (vector.magnitude - pickupDistance) / pickupDistance;
                        }
                        else {
                            particle.MoveVector = Vector3.zero;
                        }
                    }

                    particle.transform.position = Vector3.MoveTowards(particle.transform.position,
                                                      spinController.transform.position,
                                                      Time.deltaTime * gravityStrength);


                    if (distFromSpinner.magnitude < spinnerCaptureRadius) {
                        particle.IsCaptured = true;
                        spinController.AddToSpinner(particle);
                    }

                    //Vector3 gravity = spinController.transform.position - particle.transform.position;
                }
            }

            if (gameDone) {
                if (cgParticles != null) {
                    cgParticles.alpha -= Time.deltaTime * fadeSpeed;

                    if (cgParticles.alpha < 0.05f) {
                        Destroy(cgParticles.gameObject);
                        fadeSpinners = true;
                    }
                }

                if (fadeSpinners) {
                    cgSpinners.alpha -= (Time.deltaTime * fadeSpeed * 2f);

                    if (cgSpinners.alpha < 0.05f) {
                        cgSpinners.alpha = 0f;
                        this.enabled = false;
                    }
                }
            }
        }
    }

    internal void DestroyMe() {
        Destroy(gameObject);
    }

    Vector3 RNGPos() {
        Vector3 myPos = Vector3.zero;
        myPos.x = Random.value * Screen.width;
        myPos.y = Random.value * Screen.height;

        return myPos;
    }
}

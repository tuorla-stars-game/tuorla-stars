﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarCameraController : MonoBehaviour {
    Transform lookAt;

    float minZoom = 100f;
    float maxZoom = 15000f;
    float minHeight = -300f;
    float maxHeight = 300f;
    Camera thisCamera;

    internal bool isTouching = false;

    [SerializeField]
    float camOrbitPos = 0f;

    [SerializeField]
    float camHeightPos = 0f;

    [SerializeField]
    float playerZoomAmount = 10f;

    [SerializeField]
    float zoomDamper = 4f;

    [SerializeField]
    float camSpeedDamper = 4f;

    [SerializeField]
    float zoomSpeed = 5f;

    [SerializeField]
    float moveSpeed = 20f;

    [SerializeField]
    float moveSpeedZoomAdjustStep = 200f;

    [SerializeField]
    Transform theSun;

    float standardCamDist = 250f;

    internal float TouchOrbitPos {
        get {
            return camOrbitPos;
        }
        set {
            camOrbitPos = value;
        }
    }

    internal float TouchHeightPos {
        get {
            return camHeightPos;
        }
        set {
            camHeightPos = value;
            camHeightPos = Mathf.Clamp(camHeightPos, minHeight, maxHeight);
        }
    }

    internal float TouchZoomAmount {
        get {
            return playerZoomAmount;
        }
        set {
            float pZoom = Mathf.Clamp(value / zoomDamper, minZoom, maxZoom);
            playerZoomAmount = pZoom;
        }
    }

    internal Transform LookAtTarget {
        get {
            return lookAt;
        }
        set {
            lookAt = value;
        }
    }

    void Start() {
        lookAt = theSun;
        thisCamera = GetComponent<Camera>();
    }

    void LateUpdate() {
        FollowCameraUpdate();
    }

    void FollowCameraUpdate() {
        FollowCameraPosAndRotUpdate();
    }

    void FollowCameraPosAndRotUpdate() {
        Vector3 pos = lookAt.position;

        Quaternion rot = Quaternion.Euler(camHeightPos / camSpeedDamper, camOrbitPos / camSpeedDamper, 0f);
        float distance = playerZoomAmount;

        Vector3 vDist = new Vector3(0f, 0f, -distance);
        pos += rot * vDist;

        float speedAdjust = Mathf.Clamp(playerZoomAmount / moveSpeedZoomAdjustStep, 1f, float.MaxValue);
        float _moveSpeed = moveSpeed * speedAdjust;
        //transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * moveSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * _moveSpeed);
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * _moveSpeed);
    }

    internal void SetCamFocus(Transform t, float focusZoomMin, float focusZoomStart) {
        lookAt = t;
        standardCamDist = focusZoomStart;
        minZoom = focusZoomMin;
        playerZoomAmount = focusZoomStart;
    }

    internal void AdjustCameraZoon(float delta) {
        float actualDelta = delta / zoomDamper;
        float distAdjust = Vector3.Distance(transform.position, lookAt.position) / (2f * standardCamDist);
        distAdjust = Mathf.Clamp(distAdjust, 0.5f, zoomDamper * 2f);
        actualDelta *= distAdjust;
        playerZoomAmount += actualDelta;
        playerZoomAmount = Mathf.Clamp(playerZoomAmount, minZoom, maxZoom);
    }

    public void ResetCameraFocus() {
        GameController.instance.uiController.PlayButtonSFX();
        lookAt = theSun;
        playerZoomAmount = 300f;
        minZoom = 100f;
    }
}

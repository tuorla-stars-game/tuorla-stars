﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSTimeController : MonoBehaviour {
    OrbitController[] ocs;

    [SerializeField]
    float earthYearInSeconds = 60f;

    float[] speeds = new float[] { 1f, 10f, 30f, 60f, 600f, 6000f };
    int currentSpeed = 5;

    float timeOffset = 0f;

    float accumulatedTime = 0f;

	void Start () {
        ocs = GetComponentsInChildren<OrbitController>();
	}
	
	void Update () {
        float time = (Time.deltaTime % earthYearInSeconds) / earthYearInSeconds;
        accumulatedTime += time;

        foreach (OrbitController oc in ocs) {
            oc.UpdatePlanets(accumulatedTime);
        }
	}

    internal float MakeFaster() {
        if (currentSpeed > 0) {
            currentSpeed--;
        }

        float newYearInSecs = speeds[currentSpeed];
        //timeOffset += ((Time.time % earthYearInSeconds) / earthYearInSeconds) - 
                     //((Time.time % newYearInSecs) / newYearInSecs);

        earthYearInSeconds = newYearInSecs;
        return newYearInSecs;
    }

    internal float MakeSlower() {
        if (currentSpeed < speeds.Length - 1) {
            currentSpeed++;
        }

        float newYearInSecs = speeds[currentSpeed];
        //timeOffset += ((Time.time % earthYearInSeconds) / earthYearInSeconds) -
                     //((Time.time % newYearInSecs) / newYearInSecs);

        earthYearInSeconds = newYearInSecs;
        return newYearInSecs;
    }
}

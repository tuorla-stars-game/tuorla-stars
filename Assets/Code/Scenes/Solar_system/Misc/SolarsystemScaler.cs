﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarsystemScaler : MonoBehaviour {
    [SerializeField]
    float scaleOfTheSun = 1f;
    float lastScaleOfTheSun = 1f;

    [SerializeField]
    float scaleOfTheSunAdjust = 1f;

    [SerializeField]
    Transform theSun;

    [SerializeField]
    float scaleOfDistances = 1f;
    float lastScaleOfDistances = 1f;

    [SerializeField]
    float scaleOfThePlanets = 1f;
    float lastScaleOfThePlanets = 1f;

    [SerializeField]
    float logScale = 1f;
    float lastLogScale = 1f;

    [SerializeField]
    float logScaleStart = 227f;
    float lastLogScaleStart = 227f;

    [SerializeField]
    float logScaleEnds = 5906.38f;
    float lastLogScaleEnds = 5906.38f;

    Vector3 sunStartScale;
    Vector3[] planetStartScales;

    void Awake() {
        OrbitController[] orbitControllers = GetComponentsInChildren<OrbitController>();

        foreach (OrbitController oc in orbitControllers) {
            oc.STARTDistanceScale = scaleOfDistances;
            oc.LogScaleBegin = logScaleStart;
            oc.LogScaleEnd = logScaleEnds;
            oc.LogScaleBase = logScale;
        }

        sunStartScale = theSun.localScale;

        SSPlanet[] planets = GetComponentsInChildren<SSPlanet>();
        planetStartScales = new Vector3[planets.Length];

        for (int i = 0; i < planets.Length; i++) {
            planetStartScales[i] = planets[i].transform.localScale;
        }

        SetSunScale();
        SetPlanetScales();

        lastLogScale = logScale;
        lastLogScaleEnds = logScaleEnds;
        lastLogScaleStart = logScaleStart;
        lastScaleOfDistances = scaleOfDistances;
        lastScaleOfThePlanets = scaleOfThePlanets;
        lastScaleOfTheSun = scaleOfTheSun;
    }

	void Update () {
        if (lastLogScale != logScale) {
            SetAllScaleStuff();
        }
        else if (lastLogScaleEnds != logScaleEnds) {
            SetAllScaleStuff();
        }
        else if (lastLogScaleStart != logScaleStart) {
            SetAllScaleStuff();
        }
        else if (lastScaleOfDistances != scaleOfDistances) {
            SetAllScaleStuff();
        }
        else if (lastScaleOfThePlanets != scaleOfThePlanets) {
            SetAllScaleStuff();
        }
        else if (lastScaleOfTheSun != scaleOfTheSun) {
            SetAllScaleStuff();
        }

        lastLogScale = logScale;
        lastLogScaleEnds = logScaleEnds;
        lastLogScaleStart = logScaleStart;
        lastScaleOfDistances = scaleOfDistances;
        lastScaleOfThePlanets = scaleOfThePlanets;
        lastScaleOfTheSun = scaleOfTheSun;
    }

    void SetAllScaleStuff() {
        SetSunScale();
        SetScaleOrbitControllers();
        SetPlanetScales();
    }

    void SetScaleOrbitControllers() {
        OrbitController[] orbitControllers = GetComponentsInChildren<OrbitController>();

        foreach (OrbitController oc in orbitControllers) {
            oc.DistanceScale = scaleOfDistances;
            oc.LogScaleBegin = logScaleStart;
            oc.LogScaleEnd = logScaleEnds;
            oc.LogScaleBase = logScale;
        }
    }

    void SetSunScale() {
        float scale = scaleOfTheSun * scaleOfTheSunAdjust;
        Vector3 newScale = sunStartScale / scale;
        theSun.localScale = newScale;
    }

    void SetPlanetScales() {
        SSPlanet[] scaleables = GetComponentsInChildren<SSPlanet>();

        for (int i = 0; i < scaleables.Length; i++) {
            Vector3 newScale = planetStartScales[i] * scaleOfThePlanets;
            scaleables[i].transform.localScale = newScale;
        }
    }
}

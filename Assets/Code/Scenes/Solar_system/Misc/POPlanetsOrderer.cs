﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POPlanetsOrderer : MonoBehaviour {
    int numPlanets = 0;

    [SerializeField]
    float padding = 0.1f;

    [SerializeField]
    Transform planetlines;

	void Start () {
        numPlanets = transform.childCount;
        float planetLineWidth = Screen.width;
        List<int> randomPoses = new List<int>();
        for (int i = 0; i < numPlanets; i++) {
            randomPoses.Add(i);
        }

        for (int i = 0; i < numPlanets; i++) {
            int randomPos = i;

            while (randomPos == i) {
                int randomInt = UnityEngine.Random.Range(0, randomPoses.Count);
                randomPos = randomPoses[randomInt];
            }

            randomPoses.Remove(randomPos);

            float numPos = (float)randomPos / (float)(numPlanets - 1);
            float newPos = padding * planetLineWidth + (numPos * (planetLineWidth * (1f - 2f * padding)));
            Transform child = transform.GetChild(i);
            POPlanet planet = child.GetComponent<POPlanet>();
            planet.myLine = planetlines.GetChild(i).GetComponent<Planetline>();
            planet.CurPlanetOrderNum = randomPos;
            Vector3 vPos = child.position;
            vPos.x = newPos;
            child.position = vPos;

            planetlines.GetChild(randomPos).transform.position = vPos;
        }

        randomPoses.Clear();
	}
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunSpinner : MonoBehaviour {
    [SerializeField]
    float wheelRadius = 10f;
    [SerializeField]
    float rotSpeed = 5f;
    [SerializeField]
    float rotLerpSpeed = 5f;
    [SerializeField]
    float posOffsetAmount = 1f;

    float myRotSpeed = 5f;

    List<SolarParticle> myParticles = new List<SolarParticle>();

    internal float RotationSpeed {
        get {
            return rotSpeed;
        }
        set {
            rotSpeed = value;
        }
    }

    void Start() {
        myRotSpeed = rotSpeed;
    }

    void Update () {
        if (myRotSpeed != rotSpeed) {
            myRotSpeed = Mathf.Lerp(myRotSpeed, rotSpeed, Time.deltaTime * rotLerpSpeed);

            if (Mathf.Abs(rotSpeed - myRotSpeed) < 0.05f) {
                myRotSpeed = rotSpeed;
            }
        }
        transform.localRotation *= Quaternion.Euler(0f, 0f, Time.deltaTime * myRotSpeed);
	}

    internal void AddToSpinner(SolarParticle particle) {
        particle.transform.SetParent(transform, true);
        myParticles.Add(particle);

        foreach (SolarParticle part in myParticles) {
            part.MoveVector = CalculateParticlePos(part) * wheelRadius;
        }
    }

    Vector3 CalculateParticlePos(SolarParticle particle) {
        int pos = particle.transform.GetSiblingIndex() + 1;
        float relativePos = (float)pos / (float)(transform.childCount);
        relativePos *= Mathf.PI * 2f;

        Vector3 posOnWheel = transform.up * Mathf.Sin(relativePos);
        posOnWheel += transform.right * Mathf.Sin(relativePos + Mathf.PI / 2f);
        Vector3 posNoise = new Vector3(Random.value - 0.5f, Random.value - 0.5f, 0f);
        posOnWheel += posNoise * posOffsetAmount;

        return posOnWheel;
    }
}

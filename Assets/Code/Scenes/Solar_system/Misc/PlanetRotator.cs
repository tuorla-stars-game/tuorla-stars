﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotator : MonoBehaviour {
    [SerializeField]
    float rotSpeed = 5f;

	// Update is called once per frame
	void Update () {
        Debug.Log("planet rotator start frame#:" + Time.frameCount);

        float rotAmount = Time.deltaTime * rotSpeed;
        transform.localRotation *= Quaternion.Euler(0f, 0f, rotAmount);

        Debug.Log("planet rotator end frame#:" + Time.frameCount);
    }
}

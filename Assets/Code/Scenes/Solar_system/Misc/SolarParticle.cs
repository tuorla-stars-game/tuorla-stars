﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarParticle : MonoBehaviour {
    Vector3 moveVector = Vector3.zero;
    float distFactor = 1f;
    float timer = 0f;
    bool isCaptured = false;

    [SerializeField]
    float moveSpeed = 1f;
    [SerializeField]
    float slowdownPeriod = 1f;

    internal float DistanceFactor {
        get {
            return distFactor;
        }
        set {
            distFactor = value;
        }
    }

    internal Vector3 MoveVector {
        get {
            return moveVector;
        }
        set {
            moveVector = value;
            timer = Mathf.Epsilon;
        }
    }

    internal bool IsCaptured {
        get {
            return isCaptured;
        }
        set {
            moveVector = Vector3.zero;
            isCaptured = value;
        }
    }

    void Update () {
        //Debug.Log("particle update start frame#:" + Time.frameCount);
        if (!isCaptured) {
            if (timer > 0f) {
                timer += Time.deltaTime;
            }

            if (moveVector != Vector3.zero) {
                Vector3 newPos = transform.position + moveVector;
                float _moveSpeed = Time.deltaTime * moveSpeed * (slowdownPeriod - timer) * DistanceFactor;
                transform.position = Vector3.MoveTowards(transform.position,
                                                         newPos,
                                                         _moveSpeed);

                if (timer > 1f) {
                    moveVector = Vector3.zero;
                    timer = 0f;
                }
            }
        }
        else {
            if (moveVector != Vector3.zero && transform.localPosition != MoveVector) {
                Vector3 newPos = Vector3.MoveTowards(transform.localPosition,
                                                     moveVector,
                                                     Time.deltaTime * moveSpeed);

                if (!float.IsNaN(newPos.x)) {
                    transform.localPosition = newPos;
                }
            }
        }

        //Debug.Log("particle update end frame#:" + Time.frameCount);
    }
}

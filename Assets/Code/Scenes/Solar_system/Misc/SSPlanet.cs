﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSPlanet : MonoBehaviour, ICelestialBody {
    [SerializeField]
    internal float planeInclination;
    [SerializeField]
    internal float aphelion;
    [SerializeField]
    internal float eccentricity;
    [SerializeField]
    internal float semimajorAxis;
    [SerializeField]
    internal float polarRotation;
    [SerializeField]
    internal float orbitalPeriod = 1f;
    [SerializeField]
    internal float axialTilt = 0f;
    [SerializeField]
    float siderealRotationPeriod = 1f;
    [SerializeField]
    Transform actualPlanet;
    [SerializeField]
    bool debugMe = false;
    [SerializeField]
    float timeOffset = 0f;
    [SerializeField]
    internal float camZoom = 1f;
    [SerializeField]
    internal float minCamZoom = 1f;
    [SerializeField]
    internal int vertexCount = 64;


    internal WirePrimitives.WireCircle myOrbitCircle;
    SSMoon[] myMoons;

    internal float scaledAphelion = 0f;
    internal float scaledSemiMajor = 0f;
    internal float igSemiMajor = 0f;
    internal float igSemiMinor = 0f;

    float accumulatedOrbitTime = 0f;
    float lastOrbitTime = 0f;

    float accumulatedSiderealTime = 0f;
    float lastSiderealTime = 0f;
    float lastSiderealCT = 0f;

    internal SSMoon[] MoonsOfPlanet {
        get {
            return myMoons;
        }
    }

    void Start() {
        myMoons = GetComponentsInChildren<SSMoon>();
        OrbitController myOrbitController = GetComponentInParent<OrbitController>();

        actualPlanet.localRotation = Quaternion.Euler(new Vector3(axialTilt, 0f, 0f));

        foreach (SSMoon moon in myMoons) {
            moon.parentPlanetScale = transform.localScale.x;

            myOrbitController.DoBodySetup(moon, transform.localScale.x);
        }
    }

    public void UpdateBodyOrientation(float time) {
        float currentTime;

        if (siderealRotationPeriod < 1f) {
            if (time < lastSiderealTime) {
                accumulatedSiderealTime = ((lastSiderealTime % siderealRotationPeriod) / siderealRotationPeriod);
            }

            currentTime = accumulatedSiderealTime + ((time % siderealRotationPeriod) / siderealRotationPeriod);
        }
        else {
            if (lastSiderealTime > time) {
                accumulatedSiderealTime += lastSiderealTime;
                lastSiderealTime = 0f;
            }

            currentTime = ((accumulatedSiderealTime + time) % siderealRotationPeriod) / siderealRotationPeriod;

            if (currentTime >= siderealRotationPeriod) {
                accumulatedSiderealTime = (time % siderealRotationPeriod) / siderealRotationPeriod;
            }

            currentTime = ((accumulatedSiderealTime + time) % siderealRotationPeriod) / siderealRotationPeriod;
        }
        currentTime *= -1f;


        /*Vector3 newRot = actualPlanet.localEulerAngles;
        newRot.y = currentTime * 360f;

        actualPlanet.localRotation = Quaternion.Euler(newRot);*/
        actualPlanet.Rotate(transform.up, (currentTime - lastSiderealCT) * 360f);

        foreach (SSMoon moon in myMoons) {
            moon.UpdateBodyOrientation(time);
        }

        lastSiderealTime = time;
        lastSiderealCT = currentTime;
    }

    public void UpdateOrbitPosition(float time) {
        time += timeOffset;
        float currentTime;

        if (orbitalPeriod < 1f) {
            if (time < lastSiderealTime) {
                accumulatedOrbitTime = ((lastSiderealTime % orbitalPeriod) / orbitalPeriod);
            }

            currentTime = accumulatedOrbitTime + ((time % orbitalPeriod) / orbitalPeriod);
        }
        else {
            if (lastOrbitTime > time) {
                accumulatedOrbitTime += lastOrbitTime;
                lastOrbitTime = 0f;
            }

            currentTime = ((accumulatedOrbitTime + time) % orbitalPeriod) / orbitalPeriod;

            if (currentTime >= orbitalPeriod) {
                accumulatedOrbitTime = (time % orbitalPeriod) / orbitalPeriod;
            }

            currentTime = ((accumulatedOrbitTime + time) % orbitalPeriod) / orbitalPeriod;
        }
        currentTime *= -1f;

        Vector3 newPos = new Vector3(Mathf.Sin(currentTime * 2f * Mathf.PI) * igSemiMinor,
                                        0f,
                                        Mathf.Sin(currentTime * 2f * Mathf.PI + Mathf.PI / 2f) * igSemiMajor);
        transform.localPosition = newPos;

        foreach (SSMoon moon in myMoons) {
            moon.UpdateOrbitPosition(time);
        }

        lastOrbitTime = time;
    }
}

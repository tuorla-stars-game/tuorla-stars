﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitController : MonoBehaviour {
    const float RADIUS_OF_THE_SUN = .6957f;
    WirePrimitives.WireCircle wCircle;

    [SerializeField]
    Transform theSun;
    [SerializeField]
    GameObject wCircleSample;

    SSPlanet myPlanet;
    float distanceScale = 1f;
    float logScaleStart = 0f;
    float logScaleEnd = 0f;
    float logScaleBase = 1f;

    internal float LogScaleBegin {
        get {
            return logScaleStart;
        }
        set {
            logScaleStart = value;
        }
    }

    internal float LogScaleEnd {
        get {
            return logScaleEnd;
        }
        set {
            logScaleEnd = value;
        }
    }

    internal float LogScaleBase {
        get {
            return logScaleBase;
        }
        set {
            logScaleBase = value;
        }
    }

    internal float DistanceScale {
        set {
            distanceScale = value;
            DoBodySetup(myPlanet);
        }
    }

    internal float STARTDistanceScale {
        set {
            distanceScale = value;
        }
    }

	void Start () {
        myPlanet = GetComponentInChildren<SSPlanet>();

        DoBodySetup(myPlanet);
    }

    void OrbitInclAndOffsetSetup(SSPlanet planet) {
        float polarRot = UnityEngine.Random.value * 360f;

        transform.localRotation = Quaternion.Euler(planet.planeInclination,
                                                   polarRot,
                                                   0f);
        transform.localPosition = transform.forward * CalcPlanetOffset(planet.scaledAphelion,
                                                                       planet.scaledSemiMajor);
    }

    void OrbitInclAndOffsetSetup(SSMoon moon) {
        float polarRot = UnityEngine.Random.value * 360f;

        Transform t = moon.transform.parent.transform;
        t.localRotation = Quaternion.Euler(moon.planeInclination,
                                                polarRot,
                                                0f);
        t.localPosition = t.forward * CalcPlanetOffset(moon.scaledAphelion / moon.scaleOrbitSizeBy,
                                                       moon.scaledSemiMajor / moon.scaleOrbitSizeBy);
    }

    WirePrimitives.WireCircle OrbitWireSetup(float gameSemimajor, float gameSemiminorRel, string bodyName, Transform parent, int vertex) {
        wCircle = Instantiate(wCircleSample, parent).GetComponent<WirePrimitives.WireCircle>();
        wCircle.VertexCount = vertex;
        wCircle.Radius = gameSemimajor;

        Vector3 newScale = wCircle.transform.localScale;
        newScale.x = gameSemiminorRel;
        wCircle.transform.localScale = newScale;

        wCircle.name = bodyName + " orbit";
        return wCircle;
    }

    float CalcPlanetOffset(float aphelion, float semimajorAxis) {
        float offset = aphelion - semimajorAxis;
        return offset * CalcConversionFactor();
    }

    float CalcConversionFactor() {
        return theSun.transform.localScale.x / (distanceScale * RADIUS_OF_THE_SUN);
    }

    float CalcGameSemiMajor(float semimajorAxis) {
        float semiMajor = semimajorAxis * CalcConversionFactor();
        return semiMajor;
    }

    float CalcGameApogee(float apogee) {
        float gameApogee = apogee * CalcConversionFactor();
        return gameApogee;
    }

    float CalcGameSemiMinorRelative(float eccentricity) {
        return Mathf.Sqrt(1f - Mathf.Pow(eccentricity, 2f));
    }

    float CalcScalingFactor(float semiMajor) {
        float factor = semiMajor / logScaleStart;
        factor = Mathf.Clamp(factor, 1f, 5f);

        return factor;
    }

    internal void DoBodySetup(SSPlanet planet) {
        float igSemiMinorRelative = CalcGameSemiMinorRelative(planet.eccentricity);
        planet.igSemiMajor = CalcGameSemiMajor(planet.semimajorAxis);
        float scaleFactor = Mathf.Log(CalcScalingFactor(planet.semimajorAxis), logScaleBase) + 1f;
        //Debug.Log("scaleFactor: " + scaleFactor);
        planet.igSemiMajor /= scaleFactor;
        planet.scaledAphelion = planet.aphelion / scaleFactor;
        planet.scaledSemiMajor = planet.semimajorAxis / scaleFactor;
        planet.igSemiMinor = igSemiMinorRelative * planet.igSemiMajor;

        OrbitInclAndOffsetSetup(planet);

        if (planet.myOrbitCircle != null) {
            Destroy(planet.myOrbitCircle.gameObject);
        }

        planet.myOrbitCircle = OrbitWireSetup(planet.igSemiMajor, igSemiMinorRelative, planet.name, transform, planet.vertexCount);
    }

    internal void DoBodySetup(SSMoon moon, float scale) {
        float igSemiMinorRelative = CalcGameSemiMinorRelative(moon.eccentricity);
        float scaleFactor = Mathf.Log(CalcScalingFactor(moon.semimajorAxis), logScaleBase) + 1f;
        moon.igSemiMajor = CalcGameSemiMajor(moon.semimajorAxis) * scale;
        moon.igSemiMajor /= scaleFactor;
        moon.scaledAphelion = moon.aphelion / scaleFactor;
        moon.scaledSemiMajor = moon.semimajorAxis / scaleFactor;
        moon.igSemiMinor = igSemiMinorRelative * moon.igSemiMajor;

        OrbitInclAndOffsetSetup(moon);

        if (moon.myOrbitCircle != null) {
            Destroy(moon.myOrbitCircle);
        }

        Transform t = moon.transform.parent.transform;
        moon.myOrbitCircle = OrbitWireSetup(moon.igSemiMajor, igSemiMinorRelative, moon.name, t, moon.vertexCount);
    }

    internal void UpdatePlanets(float time) {
        //float time = (Time.time % yearInSeconds) / yearInSeconds;

        myPlanet.UpdateOrbitPosition(time);
        myPlanet.UpdateBodyOrientation(time);
    }
}

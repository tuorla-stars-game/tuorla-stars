﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSMoon : MonoBehaviour, ICelestialBody {
    [SerializeField]
    internal float planeInclination;
    [SerializeField]
    internal float apoapsisInKm;
    [SerializeField]
    internal float eccentricity;
    [SerializeField]
    internal float semimajorAxisInkm;
    [SerializeField]
    internal float polarRotation;
    [SerializeField]
    internal float scaleOrbitSizeBy = 1f;
    [SerializeField]
    internal float orbitalPeriod = 1f;
    [SerializeField]
    internal float axialTilt = 0f;
    [SerializeField]
    float siderealRotationPeriod = 1f;
    [SerializeField]
    internal float minCamZoom = 20f;
    [SerializeField]
    internal float camZoom = 30f;
    [SerializeField]
    internal int vertexCount = 64;


    internal WirePrimitives.WireCircle myOrbitCircle;

    internal float aphelion;
    internal float semimajorAxis;

    internal float parentPlanetScale = 1f;
    internal float scaledAphelion = 0f;
    internal float scaledSemiMajor = 0f;
    internal float igSemiMajor = 0f;
    internal float igSemiMinor = 0f;

    float accumulatedOrbitTime = 0f;
    float lastOrbitTime = 0f;

    float accumulatedSiderealTime = 0f;
    float lastSiderealTime = 0f;
    float lastSiderealCT = 0f;

    void Awake() {
        aphelion = scaleOrbitSizeBy * apoapsisInKm / 1000000f;
        semimajorAxis = scaleOrbitSizeBy * semimajorAxisInkm / 1000000f;

        transform.localRotation = Quaternion.Euler(new Vector3(axialTilt, 0f, 0f));
    }

    public void UpdateBodyOrientation(float time) {
        float currentTime;

        if (siderealRotationPeriod < 1f) {
            if (time < lastSiderealTime) {
                accumulatedSiderealTime = ((lastSiderealTime % siderealRotationPeriod) / siderealRotationPeriod);
            }

            currentTime = accumulatedSiderealTime + ((time % siderealRotationPeriod) / siderealRotationPeriod);
        }
        else {
            if (lastSiderealTime > time) {
                accumulatedSiderealTime += lastSiderealTime;
                lastSiderealTime = 0f;
            }

            currentTime = ((accumulatedSiderealTime + time) % siderealRotationPeriod) / siderealRotationPeriod;

            if (currentTime >= siderealRotationPeriod) {
                accumulatedSiderealTime = (time % siderealRotationPeriod) / siderealRotationPeriod;
            }

            currentTime = ((accumulatedSiderealTime + time) % siderealRotationPeriod) / siderealRotationPeriod;
        }
        currentTime *= -1f;
        /*Vector3 newRot = transform.localEulerAngles;
        //transform.rotation
        newRot.y = currentTime * 360f;

        transform.localRotation = Quaternion.Euler(newRot);*/

        transform.Rotate(transform.up, (currentTime - lastSiderealCT) * 360f);

        lastSiderealTime = time;
        lastSiderealCT = currentTime;
    }

    public void UpdateOrbitPosition(float time) {
        float currentTime;

        if (orbitalPeriod < 1f) {
            if (time < lastSiderealTime) {
                accumulatedOrbitTime = ((lastSiderealTime % orbitalPeriod) / orbitalPeriod);
            }

            currentTime = accumulatedOrbitTime + ((time % orbitalPeriod) / orbitalPeriod);
        }
        else {
            if (lastOrbitTime > time) {
                accumulatedOrbitTime += lastOrbitTime;
                lastOrbitTime = 0f;
            }

            currentTime = ((accumulatedOrbitTime + time) % orbitalPeriod) / orbitalPeriod;

            if (currentTime >= orbitalPeriod) {
                accumulatedOrbitTime = (time % orbitalPeriod) / orbitalPeriod;
            }

            currentTime = ((accumulatedOrbitTime + time) % orbitalPeriod) / orbitalPeriod;
        }

        currentTime *= -1f;

        Vector3 newPos = new Vector3(Mathf.Sin(currentTime * 2f * Mathf.PI) * igSemiMinor,
                                        0f,
                                        Mathf.Sin(currentTime * 2f * Mathf.PI + Mathf.PI / 2f) * igSemiMajor);
        transform.localPosition = newPos;

        lastOrbitTime = time;
    }
}

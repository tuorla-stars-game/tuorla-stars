﻿public interface ICelestialBody {
    void UpdateOrbitPosition(float time);
    void UpdateBodyOrientation(float time);
}

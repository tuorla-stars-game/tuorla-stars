﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarStateContainer : MonoBehaviour {
    [SerializeField]
    internal IgnitionGameController ignitionGameController;
    [SerializeField]
    internal GameObject theSun;
    [SerializeField]
    internal SpinnersController ignitionSpinners;
    [SerializeField]
    internal Scaler sunAnimScaler;
    [SerializeField]
    internal Animator sunAnimator;

    [SerializeField]
    internal CreatePlanetGameController createPlanetGC;
    [SerializeField]
    internal GameObject planet;
    [SerializeField]
    internal SpinnersController planetSpinners;

    [SerializeField]
    internal GameObject planetOrderGame;

    [SerializeField]
    internal GameObject solarBrowserPlanets;
    [SerializeField]
    internal GameObject solarBrowserControls;
}

﻿using System;
using UnityEngine;

public class PlanetCreationState : IState<GCSolar> {
    private static PlanetCreationState instance;
    StateNotifications notifTexts;
    SpinnersController spinners;
    float stateEntryTime;
    int stateNum = 0;
    float timer = 0f;
    float timerLength = 2f;
    bool timerActive = false;

    private PlanetCreationState(int num) {
        stateNum = num;
    }

    internal static PlanetCreationState GetInstance(int num) {
        if (instance == null) {
            instance = new PlanetCreationState(num);
        }

        return instance;
    }

    public void StateEnter(GCSolar context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Solar, context.loc);

        context.container.createPlanetGC.gameObject.SetActive(true);
        context.container.createPlanetGC.enabled = true;

        spinners = context.container.planetSpinners;
    }

    public void StateExit(GCSolar context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;

        context.container.createPlanetGC.DestroyMe();
    }

    public void StateUpdate(GCSolar context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }

        if (timerActive && timer > timerLength) {
            timer = 0f;
            timerActive = false;
            context.AdvanceGameProgress();
        }
        else if (timerActive && timer > 0f) {
            timer += Time.deltaTime;
        }
    }

    public void MessageToState(GCSolar context, int num) {
        int stateProgress = num - stateNum;

        if (stateProgress == 1) {
            spinners.centerSpinner.RotationSpeed *= 4f;
        }
        else if (stateProgress == 2) {
            spinners.centerSpinner.RotationSpeed *= 4f;
            spinners.innerSpinner.RotationSpeed *= 8f;
        }
        else if (stateProgress == 3) {
            timer = 0.01f;
            timerActive = true;
            context.container.createPlanetGC.GameFinished = true;
        }
        else if (stateProgress == 4) {
            timer = 0.01f;
            timerActive = true;
            timerLength = 4f;
        }
        else if (stateProgress == 5) {
            context.gameState.ChangeState(PlanetOrderingState.GetInstance(num));
        }

    }
}

﻿using System;
using UnityEngine;

public class PlanetOrderingState : IState<GCSolar> {
    private static PlanetOrderingState instance;
    StateNotifications notifTexts;
    float stateEntryTime;
    int stateNum = 0;
    CanvasGroup cgOrderGame;
    float fadeSpeed = 0.5f;
    float timer = 0f;
    float timerLength = 2f;
    bool gameDone = false;

    private PlanetOrderingState(int num) {
        stateNum = num;
    }

    internal static PlanetOrderingState GetInstance(int num) {
        if (instance == null) {
            instance = new PlanetOrderingState(num);
        }

        return instance;
    }

    public void StateEnter(GCSolar context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Solar, context.loc);

        context.container.planetOrderGame.SetActive(true);
        cgOrderGame = context.container.planetOrderGame.GetComponent<CanvasGroup>();
    }

    public void StateExit(GCSolar context) {
        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;

        context.DestroyGO(context.container.planetOrderGame);
    }

    public void StateUpdate(GCSolar context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(msg);
            }

            context.TutorialMsgSent = true;
        }

        if (!gameDone && cgOrderGame.alpha < 1f) {
            cgOrderGame.alpha += Time.deltaTime * fadeSpeed;

            if (cgOrderGame.alpha > 0.98f) {
                cgOrderGame.alpha = 1f;
            }
        }

        if (gameDone && timer > 0f) {
            timer += Time.deltaTime;
            cgOrderGame.alpha = 1f - (timer / timerLength);

            if (cgOrderGame.alpha < 0.02f) {
                cgOrderGame.alpha = 0f;
                timer = 0f;
                context.AdvanceGameProgress();
            }
        }
    }

    public void MessageToState(GCSolar context, int num) {
        int stateProgress = num - stateNum;

        if (stateProgress == 8) {
            timer = Mathf.Epsilon;
            gameDone = true;

            string msg = notifTexts.STATE_EXIT;

            if (msg != null) {
                context.uiController.AddNotification(msg);
            }
        }
        else if (stateProgress == 9) {
            context.gameState.ChangeState(SolarSystemBrowserState.GetInstance(num));
        }
    }
}

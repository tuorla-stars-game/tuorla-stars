﻿using System;
using UnityEngine;

public class SolarIgnitionState : IState<GCSolar> {
    private static SolarIgnitionState instance;
    StateNotifications notifTexts;
    SpinnersController spinners;
    int stateNum = 0;
    float timer = 0f;
    bool spinItUp = false;
    bool timerActive = false;

    private SolarIgnitionState(int num) {
        stateNum = num;
    }

    internal static SolarIgnitionState GetInstance(int num) {
        if (instance == null) {
            instance = new SolarIgnitionState(num);
        }

        return instance;
    }

    public void StateEnter(GCSolar context) {
        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Solar, context.loc);
        spinners = context.container.ignitionSpinners;
    }

    public void StateExit(GCSolar context) {
        context.container.ignitionGameController.DestroyMe();

        string msg = notifTexts.STATE_EXIT;

        if (msg != "null") {
            context.uiController.AddNotification(msg);
        }

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCSolar context) {
        if (Time.timeSinceLevelLoad > context.tutorialMsgWait && !context.TutorialMsgSent) {
            string msg = notifTexts.STATE_ENTRY;

            if (msg != "null") {
                context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            }

            context.TutorialMsgSent = true;
        }

        if (timerActive && timer > 2f) {
            timer = 0f;
            timerActive = false;
            context.AdvanceGameProgress();
        }
        else if (timerActive && timer > 0f) {
            timer += Time.deltaTime;
        }

        if (spinItUp) {
            spinners.centerSpinner.RotationSpeed *= 1.2f;
            spinners.innerSpinner.RotationSpeed *= 1.4f;
            spinners.outerSpinner.RotationSpeed *= 4f;
        }
    }

    public void MessageToState(GCSolar context, int num) {
        int stateProgress = num - stateNum;

        if (stateProgress == 1) {
            spinners.centerSpinner.RotationSpeed *= 4f;
            context.container.sunAnimScaler.TargetScale = new Vector3(2f, 2f, 2f);
        }
        else if (stateProgress == 2) {
            spinners.centerSpinner.RotationSpeed *= 4f;
            spinners.innerSpinner.RotationSpeed *= 8f;
            context.container.sunAnimScaler.TargetScale = new Vector3(3f, 3f, 3f);
        }
        else if (stateProgress == 3) {
            spinners.centerSpinner.RotationSpeed *= 4f;
            spinners.innerSpinner.RotationSpeed *= 8f;
            spinners.outerSpinner.RotationSpeed *= 16f;

            timer = 0.01f;
            timerActive = true;
            context.container.ignitionGameController.GameFinished = true;
            context.container.sunAnimator.SetBool("mass", true);
            context.container.sunAnimScaler.TargetScale *= 1.2f;
            context.container.sunAnimScaler.scalingSpeed = 0.25f;
        }
        else if (stateProgress == 4) {
            timer = 0.01f;
            timerActive = true;
        }
        else if (stateProgress == 5) {
            context.gameState.ChangeState(PlanetCreationState.GetInstance(num));
        }

    }
}

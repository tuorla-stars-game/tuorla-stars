﻿using System;
using UnityEngine;

public class SolarSystemBrowserState : IState<GCSolar> {
    private static SolarSystemBrowserState instance;
    StateNotifications notifTexts;
    float stateEntryTime;
    int stateNum = 0;

    private SolarSystemBrowserState(int num) {
        stateNum = num;
    }

    internal static SolarSystemBrowserState GetInstance(int num) {
        if (instance == null) {
            instance = new SolarSystemBrowserState(num);
        }

        return instance;
    }

    public void StateEnter(GCSolar context) {
        stateEntryTime = Time.timeSinceLevelLoad;

        string stateName = this.GetType().ToString();
        notifTexts = StateNotifsHelper.LoadNotifData(stateName, Scenes.Solar, context.loc);

        context.container.solarBrowserPlanets.SetActive(true);
        context.container.solarBrowserControls.SetActive(true);

        if (!context.dataHandler.WasTutorialPlayed(Scenes.Solar) && context.solarTutorial != null) {
            context.solarTutorial.SetActive(true);
        }
    }

    public void StateExit(GCSolar context) {
        string msg = notifTexts.STATE_EXIT;
        context.uiController.AddNotification(msg);

        context.TutorialMsgSent = false;
    }

    public void StateUpdate(GCSolar context) {
        if (Time.timeSinceLevelLoad - stateEntryTime > context.tutorialMsgWait && !context.TutorialMsgSent) {
            context.uiController.AddNotification(notifTexts.STATE_ENTRY);
            context.TutorialMsgSent = true;
            context.uiController.notifs.gameObject.SetActive(false);
        }
    }

    public void MessageToState(GCSolar context, int num) {
        int stateProgress = num - stateNum;

    }
}

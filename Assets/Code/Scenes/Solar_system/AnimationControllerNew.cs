using UnityEngine;

public class AnimationControllerNew : MonoBehaviour {
    Material material;

    float currentFrame = 0f;
    float spritesheetSize;
    float timer = 0f;
    float rows;
    float columns;

    [SerializeField]
    float playbackSpeed = 1f;
    [SerializeField]
    float maxFrames = -1;
    [SerializeField]
    float textureSize = 1024;

    void Start () {
        material = GetComponent<MeshRenderer>().material;

        if (maxFrames == -1) {
            maxFrames = material.mainTexture.height / material.mainTexture.width;
        }

        spritesheetSize = material.mainTexture.height / material.mainTexture.width;

        rows = material.mainTexture.width / textureSize;
        columns = material.mainTexture.height / textureSize;

        material.mainTextureScale = new Vector2(1f / rows, 1f / columns);
        material.mainTextureOffset = new Vector2(0f, 0f);
    }

    void Update () {
        timer += Time.deltaTime;
        if (timer >= ((1 / maxFrames) / playbackSpeed)) {
            timer = 0f;

            currentFrame--;

            if (currentFrame < 0) {
                currentFrame = maxFrames - 1;
            }

            float offsetX = (float)(currentFrame % columns) / columns;
            float offsetY = (float)(currentFrame / (int)rows) / rows;

            material.mainTextureOffset = new Vector2(
                offsetY,
                offsetX);

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizerSolar : MonoBehaviour {
    [SerializeField]
    Text TUTORIAL_MOON_BROWSER;

    [SerializeField]
    Text TUTORIAL_PLANET_BROWSER;

    [SerializeField]
    Text TUTORIAL_TIME_CONTROLS;

    [SerializeField]
    Text TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS;

    void OnEnable() {
        LocalizedSolarStrings lss = GCSolar.instance.localizedStrings;

        if (TUTORIAL_MOON_BROWSER != null) {
            TUTORIAL_MOON_BROWSER.text = lss.TUTORIAL_MOON_BROWSER;
        }

        if (TUTORIAL_PLANET_BROWSER != null) {
            TUTORIAL_PLANET_BROWSER.text = lss.TUTORIAL_PLANET_BROWSER;
        }

        if (TUTORIAL_TIME_CONTROLS != null) {
            TUTORIAL_TIME_CONTROLS.text = lss.TUTORIAL_TIME_CONTROLS;
        }

        if (TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS != null) {
            TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS.text = lss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS;
        }

        enabled = false;
    }
	
}

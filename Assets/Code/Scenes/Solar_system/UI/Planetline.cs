﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Planetline : MonoBehaviour {
    [SerializeField]
    Color correctColor;

    [SerializeField]
    float myBaseAlpha = 0.65f;

    [SerializeField]
    float alphaAddition = 0.3f;

    [SerializeField]
    float colorSpeed = 1f;

    [SerializeField]
    float fadeSpeed = 1f;

    [SerializeField]
    float pulseRangeDamper = 10f;

    [SerializeField]
    float pulseFrequency = 2f;

    [SerializeField]
    bool isCorrect = false;
    Image myImage;
    Image mySubImage;
    //Image mySubImage2;
    CanvasGroup myCG;

    internal bool IsCorrect {
        get {
            return isCorrect;
        }
        set {
            isCorrect = value;
        }
    }

	void Start () {
        myImage = GetComponent<Image>();
        mySubImage = transform.GetChild(0).GetComponent<Image>();
        //mySubImage2 = transform.GetChild(1).GetComponent<Image>();
        myCG = GetComponent<CanvasGroup>();
	}
	
	void Update () {
        if (isCorrect && (!myImage.color.Equals(correctColor))) {
            myImage.color = Color.Lerp(myImage.color, correctColor, Time.deltaTime * colorSpeed);
            mySubImage.color = Color.Lerp(mySubImage.color, correctColor, Time.deltaTime * colorSpeed);
            //mySubImage2.color = Color.Lerp(mySubImage2.color, correctColor, Time.deltaTime * colorSpeed);

            if (myImage.color.Equals(correctColor)) {
                isCorrect = false;
            }
        }

        myCG.alpha = (1 / pulseRangeDamper) * Mathf.Sin(pulseFrequency * Time.timeSinceLevelLoad) + myBaseAlpha;
	}

    internal void SetCorrect() {
        isCorrect = true;
        myBaseAlpha += alphaAddition;
    }
}

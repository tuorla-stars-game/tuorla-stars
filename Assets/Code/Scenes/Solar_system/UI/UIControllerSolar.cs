﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class UIControllerSolar : UIController {
    protected override GameController GetCameController() {
        return GCSolar.instance;
    }

    protected override void SubAwake() {

    }

    protected override void SubStart() {
        gc = GCSolar.instance;
        notifs = GetComponentInChildren<NotificationsController>();
    }

    protected override void SubUpdate() {

    }

    internal override void ClearUIElements() {

    }

    internal void AddNotification(string msg) {
        notifs.AddNotification(msg);
    }
}

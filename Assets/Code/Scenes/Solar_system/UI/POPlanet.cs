﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class POPlanet : MonoBehaviour {
    [SerializeField]
    InputHandlerSolar inputHandler;
    [SerializeField]
    float moveSpeed = 50f;
    [SerializeField]
    float recenterDuration = 2f;

    internal Planetline myLine;
    internal int planetOrderNumber = 0;
    internal bool locked = false;

    int curPlanetOrderNum = 0;
    float padding = 0.1f;
    Image myImage;

    float recenterTimer = 0f;

    bool beingCarried = false;
    Vector3 assignedPos = Vector3.zero;
    Vector3 rcStartPos = Vector3.zero;

    internal int CurPlanetOrderNum {
        get {
            return curPlanetOrderNum;
        }

        set {
            curPlanetOrderNum = value;
            if (curPlanetOrderNum == planetOrderNumber) {
                locked = true;
                GCSolar.instance.AdvanceGameProgress();
                myLine.SetCorrect();
            }
        }
    }

    void Awake() {
        planetOrderNumber = transform.GetSiblingIndex();
        myImage = GetComponent<Image>();
    }

    void Start () {
        assignedPos = transform.position;
	}
	
	void Update () {
        if (!beingCarried) {
            if (recenterTimer > 0f) {
                recenterTimer += Time.deltaTime;
                if (recenterTimer > recenterDuration) {
                    recenterTimer = recenterDuration;
                }

                if (assignedPos != transform.localPosition) {
                    transform.position = Vector3.Lerp(rcStartPos,
                                                      assignedPos,
                                                      Mathf.Pow(recenterTimer / recenterDuration, 1/2f));
                }

                if (recenterTimer == recenterDuration) {
                    recenterTimer = 0f;
                    myImage.raycastTarget = true;
                    rcStartPos = Vector3.zero;
                }
            }
        }
	}

    public void PickUpPlanet(RectTransform part) {
        if (!locked) {
            beingCarried = true;
            myImage.raycastTarget = false;
            inputHandler.StartCarryingPlanet(this);
        }
    }

    public void PlacePlanet(POPlanet part) {
        if (part.planetOrderNumber == CurPlanetOrderNum) {
            CurPlanetOrderNum = part.CurPlanetOrderNum;
            part.CurPlanetOrderNum = part.planetOrderNumber;
            ResetPos();
        }

        part.ResetPos();
    }

    internal void ResetPos(bool playAnimation = true) {
        beingCarried = false;

        int numPlanets = transform.parent.childCount;
        float planetLineWidth = Screen.width;
        float numPos = (float)CurPlanetOrderNum / (float)(numPlanets - 1);
        float newPos = padding * planetLineWidth + (numPos * (planetLineWidth * (1f - 2f * padding)));
        assignedPos.x = newPos;

        if (!playAnimation) {
            transform.position = assignedPos;
            myImage.raycastTarget = true;
            return;
        }

        myImage.raycastTarget = false;
        rcStartPos = transform.position;
        recenterTimer = Mathf.Epsilon;
    }

    float CalcMoveSpeed() {
        if (recenterTimer > 0f) {
            recenterTimer += Time.deltaTime;
        }

        float speed = 1f - (recenterTimer / recenterDuration);
        speed = Mathf.Clamp(speed * moveSpeed, speed / 10f, speed);
        return speed;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SSBrowserPlanetButton : MonoBehaviour {
    [SerializeField]
    SSPlanet myPlanet;

    Color inactiveColor;
    [SerializeField]
    Color activeColor;

    SolarCameraController cc;
    BrowserPlanetlineOrderer bpOrd;
    Image myImage;

	void Start () {
        cc = GCSolar.instance.cameraController;
        myImage = GetComponent<Image>();
        inactiveColor = myImage.color;
        bpOrd = GetComponentInParent<BrowserPlanetlineOrderer>();
	}

    public void FocusCameraOnMe() {
        GameController.instance.uiController.PlayButtonSFX();
        bpOrd.InactivateAll();
        myImage.color = activeColor;
        cc.SetCamFocus(myPlanet.transform, myPlanet.minCamZoom, myPlanet.camZoom);

        if (myPlanet.MoonsOfPlanet.Length > 0) {
            bpOrd.moonLine.OpenMoonSelector(myPlanet);
        }
        else {
            bpOrd.moonLine.CloseMoonSelector();
        }
    }

    internal void Inactivate() {
        myImage.color = inactiveColor;
    }
}

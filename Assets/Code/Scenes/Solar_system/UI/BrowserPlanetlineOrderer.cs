﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrowserPlanetlineOrderer : MonoBehaviour {
    int numPlanets = 0;

    [SerializeField]
    float padding = 0.1f;
    [SerializeField]
    internal BrowserMoonLine moonLine;

    SSBrowserPlanetButton[] buttons;

    void Start() {
        numPlanets = transform.childCount;
        RectTransform rTrans = GetComponent<RectTransform>();
        float planetLineWidth = rTrans.rect.width;
        float lineStart = rTrans.offsetMin.x;

        buttons = new SSBrowserPlanetButton[numPlanets];
        for (int i = 0; i < numPlanets; i++) {
            float numPos = i / (float)(numPlanets - 1);
            float newPos = padding * planetLineWidth + (numPos * (planetLineWidth * (1f - 2f * padding)));
            Transform child = transform.GetChild(i);

            Vector3 vPos = child.localPosition;
            vPos.x = - (planetLineWidth / 2f) + newPos;
            child.localPosition = vPos;
            buttons[i] = child.GetComponent<SSBrowserPlanetButton>();
        }
    }

    public void InactivateAll() {
        foreach (SSBrowserPlanetButton button in buttons) {
            button.Inactivate();
        }
    }
}

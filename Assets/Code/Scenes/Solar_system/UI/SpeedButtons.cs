﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedButtons : MonoBehaviour {
    Text myText;

    [SerializeField]
    SSTimeController timeController;

	void Start () {
        myText = GetComponent<Text>();		
	}

    public void PlusButton() {
        myText.text = timeController.MakeSlower().ToString();
    }

    public void MinusButton() {
        myText.text = timeController.MakeFaster().ToString();
    }
}

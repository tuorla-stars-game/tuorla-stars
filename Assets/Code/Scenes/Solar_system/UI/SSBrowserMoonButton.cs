﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SSBrowserMoonButton : MonoBehaviour {
    internal SSMoon myMoon;

    Color inactiveColor;
    [SerializeField]
    Color activeColor;

    SolarCameraController cc;
    BrowserMoonLine mline;
    Image myImage;

    void Start () {
        myImage = GetComponent<Image>();
        inactiveColor = myImage.color;
        mline = transform.parent.GetComponentInParent<BrowserMoonLine>();
        cc = GCSolar.instance.cameraController;
	}

    public void FocusCameraOnMe() {
        GameController.instance.uiController.PlayButtonSFX();
        mline.InactivateAll();
        myImage.color = activeColor;
        cc.SetCamFocus(myMoon.transform, myMoon.minCamZoom, myMoon.camZoom);
    }

    internal void Inactivate() {
        myImage.color = inactiveColor;
    }
}

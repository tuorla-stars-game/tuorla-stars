﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrowserMoonLine : MonoBehaviour {
    [SerializeField]
    float positionGap = 20f;
    [SerializeField]
    float positionClosed = 30f;
    [SerializeField]
    float fadeSpeed = 1f;
    [SerializeField]
    float openSpeed = 1f;
    [SerializeField]
    GameObject sampleMoon;
    [SerializeField]
    Transform parent;
    [SerializeField]
    float padding = 10f;
    [SerializeField]
    float moonsOffset = 20f;

    float position;
    float alpha = 0f;

    RectTransform myRect;
    CanvasGroup myCG;
    List<SSBrowserMoonButton> buttons;
    SSPlanet myPlanet;

    void Awake() {
        myRect = GetComponent<RectTransform>();
        myCG = GetComponent<CanvasGroup>();
        buttons = new List<SSBrowserMoonButton>();
        position = positionClosed;
    }
	
	void Update () {
        Vector3 offsetMax = myRect.offsetMax;

        if (offsetMax.y != position) {
            offsetMax.y = Mathf.Lerp(offsetMax.y, position, Time.deltaTime * openSpeed);

            if (Mathf.Abs(offsetMax.y - position) < 1f) {
                offsetMax.y = position;
            }

            myRect.offsetMax = offsetMax;
        }

        if (myCG.alpha != alpha) {
            myCG.alpha = Mathf.MoveTowards(myCG.alpha, alpha, Time.deltaTime * fadeSpeed);
        }

	}

    internal void OpenMoonSelector(SSPlanet planet) {
        CloseMoonSelector();
        myPlanet = planet;
        SSMoon[] moons = planet.MoonsOfPlanet;

        alpha = 1f;
        position = positionClosed + moons.Length * positionGap;
        float _position = position - moonsOffset;
        float _padding = _position * padding;
        if (moons.Length == 2) {
            _padding *= 1.5f;
        }

        for (int i = 0; i < moons.Length; i++) {
            SSBrowserMoonButton newButton = Instantiate(sampleMoon, parent).GetComponent<SSBrowserMoonButton>();
            newButton.gameObject.SetActive(true);
            newButton.myMoon = moons[i];

            float numPos = .5f;
            if (moons.Length > 1) {
                numPos = i / (float)(moons.Length - 1);
            }

            float newPos = moonsOffset + _padding + numPos * (_position - 2f * _padding);

            Vector3 vPos = newButton.transform.localPosition;
            vPos.y = -(_position / 2f) + newPos;
            Debug.Log(vPos);
            newButton.transform.localPosition = vPos;

            buttons.Add(newButton);
        }
    }

    public void CloseMoonSelector() {
        foreach (SSBrowserMoonButton button in buttons) {
            Destroy(button.gameObject);
        }
        buttons.Clear();

        alpha = 0f;
        position = positionClosed;
    }

    public void FocusOnPlanet() {
        InactivateAll();
        GameController.instance.uiController.PlayButtonSFX();
        GCSolar.instance.cameraController.SetCamFocus(myPlanet.transform, myPlanet.minCamZoom, myPlanet.camZoom);
    }

    internal void InactivateAll() {
        foreach (SSBrowserMoonButton button in buttons) {
            button.Inactivate();
        }
    }
}

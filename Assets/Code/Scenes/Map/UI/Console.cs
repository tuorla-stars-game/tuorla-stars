﻿using UnityEngine;
using UnityEngine.UI;

public class Console : MonoBehaviour {
    Text[] textRows;

    void Start() {
        int countChildren = transform.childCount;
        textRows = new Text[countChildren];

        for (int i = 0; i < countChildren; i++) {
            textRows[i] = transform.GetChild(i).GetComponent<Text>();
        }
    }

    void Update() {

    }

    internal void WriteLine(string line) {
        for (int i = textRows.Length - 2; i >= 0 ; i--) {
            textRows[i + 1].text = textRows[i].text;
        }

        textRows[0].text = line;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizerGameScene : MonoBehaviour {
    [SerializeField]
    Text TUTORIAL_SAMPLE_NOTIFICATION;
    [SerializeField]
    Text TUTORIAL_BUILDING_HEXAGON;
    [SerializeField]
    Text TUTORIAL_TELESCOPE_PART;
    [SerializeField]
    Text TUTORIAL_HEX_MENU;

    [SerializeField]
    Text INITIALS_ENTER;
    [SerializeField]
    Text INITIALS_TYPE_HERE;

    [SerializeField]
    Text UNLOCK_TELESCOPE;
    [SerializeField]
    Text UNLOCK_TELESCOPE_DIRECTIONS;

    [SerializeField]
    Text CAVE_MAP_MATERIALSTORAGE;
    [SerializeField]
    Text CAVE_MAP_MIRROR_COATING;
    [SerializeField]
    Text CAVE_MAP_MIRROR_POLISHING;
    [SerializeField]
    Text CAVE_MAP_TEST_TUNNEL;
    [SerializeField]
    Text CAVE_MAP_MEASURING_TUNNEL;

    void OnEnable() {
        LocalizedGameSceneStrings lgs = GCMap.instance.localizedStrings;

        if (TUTORIAL_SAMPLE_NOTIFICATION != null) {
            TUTORIAL_SAMPLE_NOTIFICATION.text = lgs.TUTORIAL_SAMPLE_NOTIFICATION;
        }
        if (TUTORIAL_BUILDING_HEXAGON != null) {
            TUTORIAL_BUILDING_HEXAGON.text = lgs.TUTORIAL_BUILDING_HEXAGON;
        }
        if (TUTORIAL_TELESCOPE_PART != null) {
            TUTORIAL_TELESCOPE_PART.text = lgs.TUTORIAL_TELESCOPE_PART;
        }
        if (TUTORIAL_HEX_MENU != null) {
            TUTORIAL_HEX_MENU.text = lgs.TUTORIAL_HEX_MENU;
        }

        if (INITIALS_ENTER != null) {
            INITIALS_ENTER.text = lgs.INITIALS_ENTER;
        }
        if (INITIALS_TYPE_HERE != null) {
            INITIALS_TYPE_HERE.text = lgs.INITIALS_TYPE_HERE;
        }

        if (UNLOCK_TELESCOPE != null) {
            UNLOCK_TELESCOPE.text = lgs.UNLOCK_TELESCOPE;
        }
        if (UNLOCK_TELESCOPE_DIRECTIONS != null) {
            UNLOCK_TELESCOPE_DIRECTIONS.text = lgs.UNLOCK_TELESCOPE_DIRECTIONS;
        }

        if (CAVE_MAP_MATERIALSTORAGE != null) {
            CAVE_MAP_MATERIALSTORAGE.text = lgs.CAVE_MAP_MATERIALSTORAGE;
        }
        if (CAVE_MAP_MIRROR_COATING != null) {
            CAVE_MAP_MIRROR_COATING.text = lgs.CAVE_MAP_MIRROR_COATING;
        }
        if (CAVE_MAP_MIRROR_POLISHING != null) {
            CAVE_MAP_MIRROR_POLISHING.text = lgs.CAVE_MAP_MIRROR_POLISHING;
        }
        if (CAVE_MAP_TEST_TUNNEL != null) {
            CAVE_MAP_TEST_TUNNEL.text = lgs.CAVE_MAP_TEST_TUNNEL;
        }
        if (CAVE_MAP_MEASURING_TUNNEL != null) {
            CAVE_MAP_MEASURING_TUNNEL.text = lgs.CAVE_MAP_MEASURING_TUNNEL;
        }

        enabled = false;
    }
	
}

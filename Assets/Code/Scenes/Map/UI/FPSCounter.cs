﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour {
    Text fpsText;
    Text ftText;
    Queue<float> frameTimes = new Queue<float>();

    void Start () {
        fpsText = transform.GetChild(0).GetChild(0).GetComponent<Text>();
        ftText = transform.GetChild(1).GetChild(0).GetComponent<Text>();
    }

    void Update () {
        frameTimes.Enqueue(Time.unscaledDeltaTime);

        float totalTime = 0f;
        foreach (float frameTime in frameTimes) {
            totalTime += frameTime;
        }

        while (totalTime > 1f) {
            float removed = frameTimes.Dequeue();
            totalTime -= removed;
        }

        float fps = frameTimes.Count / totalTime;
        fps = Mathf.Round(fps * 100f) / 100f;
        fpsText.color = fpsToColor(fps);
        fpsText.text = fps.ToString();
        float frameTimeText = Time.unscaledDeltaTime * 100000f;
        frameTimeText = Mathf.Round(frameTimeText) / 100f;
        ftText.text = frameTimeText.ToString();
    }

    Color fpsToColor(float fps) {
        float red = Mathf.Clamp(1 - (fps / 30f), 0f, 1f);
        float green = Mathf.Clamp(fps / 30f, 0f, 1f);

        Color fpsColor = new Color(red, green, 0f);
        return fpsColor;
    }
}

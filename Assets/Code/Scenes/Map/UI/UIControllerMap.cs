﻿using System;
using UnityEngine;

internal class UIControllerMap : UIController {
    //POIUIBuilder poiBuilder;
    [SerializeField]
    GameObject caveMap;
    [SerializeField]
    GameObject teacherPrompt;

    protected override GameController GetCameController() {
        return GCMap.instance;
    }

    protected override void SubAwake() {
        //poiBuilder = GameObject.FindGameObjectWithTag("POIUIBuilder").GetComponent<POIUIBuilder>();
    }

    protected override void SubStart() {
        gc = GCMap.instance;
        notifs = GetComponentInChildren<NotificationsController>();
    }

    protected override void SubUpdate() {

    }

    internal override void ClearUIElements() {
        CloseVisibleWindow();
    }

    internal void CloseVisibleWindow() {
        if (openHexMenuId == 0 && openWindowId == 0) {
            ToggleDim();
            //poiBuilder.ToggleState();
            gc.IsUIElementActive = false;
        }
    }

    internal void DisplayPOIWindow(POIData data) {
        if (!gc.IsUIElementActive) {
            ToggleDim();
            //poiBuilder.BuildWindowForPOI(data);
            gc.IsUIElementActive = true;
        }
    }

    public void DisplayMap() {
        if (!gc.IsUIElementActive) {
            gc.IsUIElementActive = true;
            caveMap.SetActive(true);
            buttonSfx.PlayOneShot(buttonSfx.clip);
        }
    }

    public void CloseMap() {
        gc.IsUIElementActive = false;
        caveMap.SetActive(false);
    }

    public void OpenTeacherAuthWindow() {
        teacherPrompt.SetActive(true);
    }
}

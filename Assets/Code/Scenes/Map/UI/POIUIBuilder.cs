﻿using UnityEngine;
using UnityEngine.UI;

public class POIUIBuilder : MonoBehaviour {

    [SerializeField]
    Text header;
    [SerializeField]
    Image image;
    [SerializeField]
    Text body;

    void Start() {
        ToggleState();
    }

    void ToggleChildrenEnabledState() {
        int max = transform.childCount;

        for (int i = 0; i < max; i++) {
            GameObject child = transform.GetChild(i).gameObject;
            child.SetActive(!child.activeSelf);
        }
    }

    internal void ToggleState() {
        Image myImage = gameObject.GetComponent<Image>();
        myImage.enabled = !myImage.enabled;
        ToggleChildrenEnabledState();
    }

    internal void BuildWindowForPOI(POIData data) {
        ToggleState();
        header.text = data.header;
        image.sprite = POIHelper.LoadSprite(data.imagePath);
        image.type = Image.Type.Tiled;
        body.text = data.body;
    }
}

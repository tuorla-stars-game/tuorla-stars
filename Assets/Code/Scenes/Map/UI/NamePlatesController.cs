﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NamePlatesController : MonoBehaviour {
    [SerializeField]
    OtherIndicatorsController oic;
    [SerializeField]
    GameObject samplePlate;

    float timer = 0f;
    List<NamePlate> namePlates;

    void Awake() {
        namePlates = new List<NamePlate>();        
    }

    void Update () {
        timer += Time.deltaTime;

        while (namePlates.Count > oic.OtherIndicators.Count) {
            NamePlate toDelete = namePlates.Find(x => oic.OtherIndicators.Exists(y => y.ID == x.myIndicator.ID) == false);
            namePlates.Remove(toDelete);
            Destroy(toDelete.gameObject);
        }

        if (timer > 2f) {
            if (namePlates.Count < oic.OtherIndicators.Count) {
                foreach (OtherIndicatorController controller in oic.OtherIndicators) {
                    if (namePlates.Exists(x => x.myIndicator.ID == controller.ID)) {
                        continue;
                    }

                    GameObject newPlate = Instantiate(samplePlate, transform);
                    newPlate.SetActive(true);
                    NamePlate plate = newPlate.GetComponent<NamePlate>();
                    plate.myIndicator = controller;
                    namePlates.Add(plate);
                    plate.enabled = true;
                }
            }

            timer = 0f;
        }
	}
}

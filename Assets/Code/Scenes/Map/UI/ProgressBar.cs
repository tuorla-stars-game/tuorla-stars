﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour {
    RectTransform rectT;
    float rightMax = 0f;

    [SerializeField]
    bool isGrowing = false;

    float progress = 0f;
    float _progress = 0f;

    [SerializeField]
    float progressSpeed = 3;
    [SerializeField]
    float finishThreshold = 0.001f;

    void Start() {
        rectT = GetComponentInChildren<RectTransform>();
        rightMax = rectT.rect.xMax * 2f;
        rectT.offsetMax = new Vector2(-rightMax, 0f);
    }

    void Update() {
        if (isGrowing) {
            _progress = Mathf.Lerp(_progress, progress, Time.deltaTime * progressSpeed);

            if (progress - _progress < finishThreshold) {
                _progress = progress;
                isGrowing = false;
            }

            rectT.offsetMax = new Vector2((1 - _progress) * -rightMax, 0f);
        }
    }

    internal void SetProgress(float percentage) {
        isGrowing = true;
        progress = percentage;
    }
}

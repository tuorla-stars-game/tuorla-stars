﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NamePlate : MonoBehaviour {
    internal OtherIndicatorController myIndicator;
    GameObject plateGraphic;
    bool isAlerting = false;
    string lastInitials = "";

    [SerializeField]
    Camera mainCam;

    [SerializeField]
    Color alertColor;

    [SerializeField]
    float nameplateHeight = 0.5f;

    Color passiveColor;

    void Awake() {
        plateGraphic = transform.GetChild(0).gameObject;
        passiveColor = plateGraphic.GetComponent<Image>().color;
    }

    void OnEnable() {
        GetComponentInChildren<Text>().text = myIndicator.Initials;
        lastInitials = myIndicator.Initials;
    }

    void Update () {
        if (IsInViewport(myIndicator.transform.position)) {
            if (!plateGraphic.activeSelf) {
                plateGraphic.SetActive(true);
            }

            Vector3 nameplateWorldPos = myIndicator.transform.position;
            nameplateWorldPos += myIndicator.transform.up * nameplateHeight;
            transform.position = mainCam.WorldToScreenPoint(nameplateWorldPos);

            if (lastInitials != myIndicator.Initials) {
                GetComponentInChildren<Text>().text = myIndicator.Initials;
            }

            lastInitials = myIndicator.Initials;
        }
        else {
            if (plateGraphic.activeSelf) {
                transform.GetChild(0).gameObject.SetActive(false);
            }
        }

        if (plateGraphic.activeSelf) {
            if (isAlerting) {
                if (myIndicator.Message == "") {
                    plateGraphic.GetComponent<Image>().color = passiveColor;
                    isAlerting = false;
                }
            }
            else {
                if (myIndicator.Message != "") {
                    plateGraphic.GetComponent<Image>().color = alertColor;
                    isAlerting = true;
                }
            }

        }
	}

    bool IsInViewport(Vector3 pos) {
        Vector3 vpPoint = mainCam.WorldToViewportPoint(pos);

        if (vpPoint.x < 0f || vpPoint.x > 1f || vpPoint.y < 0f || vpPoint.y > 1f) {
            return false;
        }

        return true;
    }
}

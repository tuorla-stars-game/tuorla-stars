﻿using UnityEngine;

public class HexagonSpinner : MonoBehaviour {
    [SerializeField]
    float spinSpeed = 50f;

    bool playerClose;

    [SerializeField]
    float gravityStrength = 0.02f;

    [SerializeField]
    float bounceForce = 1.5f;

    [SerializeField]
    float reBounceThreshold = 0.05f;

    [SerializeField]
    float moveSpeed = 1.5f;

    [SerializeField]
    float bounceCoefficient = 0.65f;

    internal bool IsPlayerClose {
        get {
            return playerClose;
        }
        set {
            playerClose = value;
            currentBounceForce = 0f;
            moveForce = Vector3.zero;
        }
    }


    float currentBounceForce = 0f;
    Vector3 startPos;
    Vector3 moveForce;

    private void Start() {
        startPos = transform.position;
    }

    void Update() {
        transform.localRotation *= Quaternion.Euler(0f, 0f, spinSpeed * Time.deltaTime);

        if (playerClose || transform.position.y > startPos.y) {
            moveForce += Vector3.down * gravityStrength;

            if (transform.position.y < startPos.y) {
                transform.position = startPos;
                moveForce = Vector3.zero;
                moveForce += Vector3.up * currentBounceForce;
                currentBounceForce *= bounceCoefficient;

                if (currentBounceForce < reBounceThreshold) {
                    currentBounceForce = bounceForce;
                    moveForce = Vector3.zero;
                }
            }

            Vector3 newPos = transform.position + moveForce * Time.deltaTime * moveSpeed;
            transform.position = newPos;

            if (!playerClose && transform.position.y < startPos.y) {
                transform.position = startPos;
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartAdder : MonoBehaviour {
    [SerializeField]
    int partNum;

    internal CardData AddPart() {
        GCMap.instance.AddPart(partNum);
        gameObject.SetActive(false);
        CardData cdata = new CardData();
        cdata.category = CardLibrary.telescopeString;
        cdata.ID = partNum;
        return cdata;
    }

    internal CardData GetPart() {
        CardData cdata = new CardData();
        cdata.category = CardLibrary.telescopeString;
        cdata.ID = partNum;
        return cdata;
    }

    internal void DestroyHex() {
        Destroy(gameObject);
    }
}

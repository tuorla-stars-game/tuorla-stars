﻿using UnityEngine;

public class CameraFocuser : MonoBehaviour {
    CameraController mainCamController;
    bool centerOnTouch = true;

    internal bool CenterOnTouch {
        get {
            return centerOnTouch;
        }
        set {
            centerOnTouch = value;
        }
    }

    void Awake() {
        mainCamController = GameObject.FindGameObjectWithTag("MainCamera")
                                      .GetComponent<CameraController>();
    }

    public void CenterCameraOnMe(bool isFromTouch) {
        if (isFromTouch == centerOnTouch) {
            mainCamController.LookAtTarget = transform;
        }
    }


}

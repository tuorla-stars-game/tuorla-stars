﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdFlierRotator : MonoBehaviour {
    [SerializeField]
    float rotSpeed = 5f;

	// Update is called once per frame
	void Update () {
        float rotAmount = Time.deltaTime * rotSpeed;
        transform.localRotation *= Quaternion.Euler(0f, rotAmount, 0f);
	}
}

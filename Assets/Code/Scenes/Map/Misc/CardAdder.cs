﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAdder : MonoBehaviour {
    [SerializeField]
    CardData[] cards;

    internal void DestroyHex() {
        Destroy(gameObject);
    }

    internal CardData[] GetCards() {
        return cards;
    }
}

[Serializable]
public struct CardData {
    public string category;
    public int ID;
}
﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    Transform lookAt;
    Transform player;
    float minZoom = 5f;
    float maxZoom = 90f;
    float minHeight = 12.5f;
    float maxHeight = 50f;
    Camera thisCamera;

    float birdMinZoom = 1f;
    float defaultMinZoom = 0f;

    [SerializeField]
    float camOrbitPos = 0f;

    [SerializeField]
    float camHeightPos = 0f;

    [SerializeField]
    float playerZoomAmount = 10f;

    [SerializeField]
    float zoomDamper = 4f;

    [SerializeField]
    float camSpeedDamper = 4f;

    [SerializeField]
    bool useOrthographicCamera = true;

    [SerializeField]
    GameObject testCube;

    [SerializeField]
    float zoomSpeed = 5f;

    internal float TouchOrbitPos {
        get { return camOrbitPos; }
        set { camOrbitPos = value; }
    }

    internal float TouchHeightPos {
        get { return camHeightPos; }
        set {
            camHeightPos = value;
            camHeightPos = Mathf.Clamp(camHeightPos, minHeight, maxHeight);
        }
    }

    internal float TouchZoomAmount {
        get { return playerZoomAmount; }
        set {
            playerZoomAmount = Mathf.Clamp(value, minZoom * zoomDamper, maxZoom * zoomDamper);
        }
    }

    internal Transform LookAtTarget {
        get {
            return lookAt;
        }
        set {
            lookAt = value;

            if (lookAt.name == "bird") {
                minZoom = birdMinZoom;
            }
            else {
                minZoom = defaultMinZoom;
            }
        }
    }

    void Start() {
        lookAt = GameObject.FindGameObjectWithTag("Player").transform;
        player = lookAt;
        maxHeight *= camSpeedDamper;
        minHeight *= camSpeedDamper;
        defaultMinZoom = minZoom;
        //playerZoomAmount *= zoomDamper;

        thisCamera = GetComponent<Camera>();
    }

    void LateUpdate() {
        FollowCameraUpdate();
    }

    void FollowCameraUpdate() {
        FollowCameraPosAndRotUpdate();
    }

    void FollowCameraPosAndRotUpdate() {
        Vector3 pos = lookAt.position;

        Quaternion rot = Quaternion.Euler(camHeightPos / camSpeedDamper, camOrbitPos / camSpeedDamper, 0f);
        float distance = playerZoomAmount / zoomDamper;

        if (useOrthographicCamera) {
            if (!thisCamera.orthographic) {
                thisCamera.orthographic = true;
                playerZoomAmount = 300f;
                camHeightPos = 130f * 3f;
                camOrbitPos = -200f;
            }

            thisCamera.orthographicSize = Mathf.Lerp(thisCamera.orthographicSize,
                                                     playerZoomAmount / zoomDamper,
                                                     Time.deltaTime * zoomSpeed);
            distance = 100f;
        }
        else {
            if (thisCamera.orthographic) {
                thisCamera.orthographic = false;
                playerZoomAmount = 50f;
            }
        }

        RaycastHit rayHit;

        Vector3 vDist = new Vector3(0f, 0f, -distance);
        pos += rot * vDist;
        Vector3 endPoint = pos;

        if (!useOrthographicCamera) {
            if (Physics.Linecast(lookAt.position, pos, out rayHit)) {
                distance = rayHit.distance * .9f;
                vDist.z = -distance;
                pos = lookAt.position;
                pos += rot * vDist;
                endPoint = rayHit.point;
            }
        }

        transform.position = Vector3.Slerp(transform.position, pos, Time.deltaTime * 20f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 20f);
        //transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * 30f);
    }

    public void ResetCameraFocus() {
        GCMap.instance.uiController.PlayButtonSFX();
        lookAt = player;
        minZoom = defaultMinZoom;
        playerZoomAmount = 300f;
    }
}

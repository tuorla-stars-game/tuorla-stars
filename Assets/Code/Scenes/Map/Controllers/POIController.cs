﻿using UnityEngine;

public class POIController : MonoBehaviour {
    Transform player;
    UIControllerMap uiController;
    CardAdder cardAdder;
    PartAdder partAdder;
    HexagonSpinner hexSpinner;
    //POIData myData;
    CameraFocuser cameraFocuser;
    bool isPlayerWithinRange;
    CardData[] myCards;
    bool wasJustDiscovered = false;

    [SerializeField]
    float activateDistance = 3f;

    [SerializeField]
    bool tripMe = false;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        uiController = GameObject.FindGameObjectWithTag("Canvas")
                              .GetComponent<UIControllerMap>();

        //myData = POIHelper.LoadPOIData(gameObject.name);

        Transform parent = transform.parent;
        cardAdder = parent.GetComponentInChildren<CardAdder>();
        partAdder = parent.GetComponentInChildren<PartAdder>();
        hexSpinner = parent.GetComponentInChildren<HexagonSpinner>();
        cameraFocuser = parent.GetComponentInChildren<CameraFocuser>();

        myCards = cardAdder.GetCards();

        bool playerHasCards = true;

        foreach (CardData card in myCards) {
            //Debug.Log("got card (" + card.category + ", " + card.ID + ")");
            if (!GCMap.instance.dataHandler.HasCard(card.category, card.ID)) {
                playerHasCards = false;
            }
        }

        if (playerHasCards) {
            cardAdder.DestroyHex();
        }

        if (partAdder != null) {
            if (GCMap.instance.dataHandler.HasTelescopePart(partAdder.GetPart().ID)) {
                partAdder.DestroyHex();
            }
        }

        if (cameraFocuser != null) {
            cameraFocuser.CenterOnTouch = false;
        }
    }

    // Update is called once per frame
    void Update() {
        float dist = Vector3.Distance(transform.position, player.position);
        isPlayerWithinRange = dist <= activateDistance;

        if (hexSpinner.IsPlayerClose != isPlayerWithinRange) {
            hexSpinner.IsPlayerClose = isPlayerWithinRange;
        }

        if (tripMe) {
            OnTouch();
            tripMe = false;
        }
    }

    internal void OnTouch() {
        if (cameraFocuser != null) {
            cameraFocuser.CenterCameraOnMe(false);
        }

        CardData[] _myCards;

        if (isPlayerWithinRange || tripMe) {
            if (cardAdder != null) {
                cardAdder.DestroyHex();
                wasJustDiscovered = true;
            }

            if (partAdder != null) {
                CardData partCard = partAdder.AddPart();
                partAdder.DestroyHex();
                wasJustDiscovered = true;

                _myCards = new CardData[myCards.Length + 1];

                for (int i = 0; i < myCards.Length; i++) {
                    _myCards[i] = myCards[i];
                }

                _myCards[_myCards.Length - 1] = partCard;
            }
            else {
                _myCards = myCards;
            }
        }
        else {
            _myCards = myCards;
        }


        if (_myCards != null) {
            foreach (CardData card in _myCards) {
                Debug.Log("Adding card: (" + card.category + ", " + card.ID + ")");
                uiController.csController.AddCardToStack(card.category, card.ID, wasJustDiscovered);
            }

            wasJustDiscovered = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
    Material material;

    float currentFrame = 0f;
    float maxFrames;
    float timer = 0f;

    [SerializeField]
    float playbackSpeed = 1f;

    void Start () {
        material = GetComponent<MeshRenderer>().material;
        maxFrames = material.mainTexture.height / material.mainTexture.width;
        
        material.mainTextureScale = new Vector2(1f, 1f / maxFrames);
        material.mainTextureOffset = new Vector2(0f, 0f);
    }

    void Update () {
        timer += Time.deltaTime;
        if (timer >= ((1 / maxFrames) / playbackSpeed)) {
            timer = 0f;

            currentFrame--;

            if (currentFrame < 0) {
                currentFrame = maxFrames - 1;
            }

            material.mainTextureOffset = new Vector2(0f, currentFrame / maxFrames);
        }
    }
}

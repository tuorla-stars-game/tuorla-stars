﻿using System;
using UnityEngine;
using UnityEngine.UI;

internal class GCMap : GameController {
    new internal static GCMap instance;
    internal NotificationsController notifs;
    Bounds mapBounds;
    GameObject locIndicator;
    internal LocalizedGameSceneStrings localizedStrings;

    [SerializeField]
    GameObject namePrompt;

    [SerializeField]
    internal IndicatorController indController;

    [SerializeField]
    OtherIndicatorsController oic;

    [SerializeField]
    GameObject alertButton;

    [SerializeField]
    GameObject mapTutorial;

    [SerializeField]
    LocalizerGameScene localizer;

    [SerializeField]
    AccessChecker teleAccessChecker;

    [SerializeField]
    ScreenEnabler teleCongratulations;

    internal Bounds Bounds {
        get { return mapBounds; }
    }

    protected override void SubAwake() {
        instance = this;
        dataHandler = DataHandler.instance;
    }

    protected override void SubStart() {
        locIndicator = GameObject.FindGameObjectWithTag("Player");
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();
        GameObject map = GameObject.FindGameObjectWithTag("Map");
        mapBounds = map.GetComponent<Renderer>().bounds;

        if (!dataHandler.WasTutorialPlayed(Scenes.Map) && mapTutorial != null) {
            mapTutorial.SetActive(true);
        }
        else if (dataHandler.Initials == "") {
            namePrompt.SetActive(true);
            IsUIElementActive = true;
            //uiController.ToggleWindow(3);
        }
    }

    internal override void GrantTelescopeAccess() {
        if (teleAccessChecker != null) {
            if (!teleAccessChecker.hasAccess) {
                teleAccessChecker.EnableAccess(Scenes.Telescope);
                if (teleCongratulations != null) {
                    teleCongratulations.gameObject.SetActive(true);
                    teleCongratulations.ElementDisplay();
                }

                int numTeleCards = cardLibrary.GetNumCards(CardLibrary.telescopeString);

                for (int i = 0; i < numTeleCards; i++) {
                    dataHandler.AddCard(CardLibrary.telescopeString, i + 1);
                }
            }
        }
    }

    internal void TeleportIndicator(Vector3 coords) {
        locIndicator.transform.position = (coords + Vector3.up * 0.025f);
    }

    protected override void SubUpdate() {
    }

    internal override UIController GetUIController() {
        return GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIControllerMap>();
    }

    internal void MakeTeacher() {
        indController.IsTeacher = true;
        dataHandler.IsTeacher = true;

        alertButton.SetActive(indController.IsTeacher);
    }

    public void PromptFinish(bool isTeacher) {
        uiController.PlayButtonSFX();
        InputField field = namePrompt.GetComponentInChildren<InputField>();
        string initials = field.text;

        if (initials.Length > 2) {
            initials = initials.Substring(0, 2);
        }
        else {
            while (initials.Length < 2) {
                string randomChar = ((char)(65 + (int)(UnityEngine.Random.value * 25f))).ToString();
                initials = initials.Insert(initials.Length, randomChar);
            }
        }

        initials = initials.ToUpper();
        indController.Initials = initials;
        indController.IsTeacher = isTeacher;
        dataHandler.Initials = initials;
        dataHandler.IsTeacher = isTeacher;
        namePrompt.gameObject.SetActive(false);
        IsUIElementActive = false;
        //uiController.ToggleWindow(3);
        alertButton.SetActive(isTeacher);
    }

    public void CloseTutorial() {
        uiController.PlayButtonSFX();
        mapTutorial.SetActive(false);
        dataHandler.SetTutorialPlayed(Scenes.Map);

        if (dataHandler.Initials == "") {
            namePrompt.SetActive(true);
            IsUIElementActive = true;
            //uiController.ToggleWindow(3);
        }
    }

    protected override void CleanUpBeforeLoad() {
        oic.EventCleanup();
    }

    protected override void SubApplyLocalization() {
        localizedStrings = LocalizationDataLoader.LoadLocalizedGameSceneData(loc);
        localizer.enabled = true;
    }
}

﻿using UnityEngine;

internal class IndicatorController : MonoBehaviour {
    bool locationEnabled = false;
    float timeCounter = 0f;
    string databaseID = "";
    string myInitials = "";
    bool isTeacher = false;
    NotificationsController notifs;
    Bounds mapBounds;
    Animator animator;
    Vector2 spoofRelative = Vector2.zero;

    GameObject playerModel;
    GameObject teacherModel;

    [SerializeField]
    bool spoofGoodShit = false;
    [SerializeField]
    float locUpdateFrequency = 2f;
    [SerializeField]
    bool disableGPS = false;

    internal string Initials {
        get {
            return myInitials;
        }
        set {
            myInitials = value;
        }
    }

    internal bool IsTeacher {
        get {
            return isTeacher;
        }
        set {
            if (isTeacher != value) {
                isTeacher = value;
                playerModel.SetActive(!isTeacher);
                teacherModel.SetActive(isTeacher);

                if (isTeacher) {
                    animator = teacherModel.GetComponent<Animator>();
                }
                else {
                    animator = playerModel.GetComponent<Animator>();
                }
            }
        }
    }

    void Awake() {
        playerModel = transform.GetChild(0).gameObject;
        teacherModel = transform.GetChild(1).gameObject;

        myInitials = GCMap.instance.dataHandler.Initials;
        isTeacher = GCMap.instance.dataHandler.IsTeacher;
    }

    void Start() {
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();
        //notifs.AddNotification(transform.position);

        WebHelper.OnPostReady += PostDoneHandler;
        LocationHelper.OnLocReady += LocDoneHandler;

        mapBounds = GCMap.instance.Bounds;

        animator = transform.GetChild(0).GetComponent<Animator>();

        if (GCMap.instance.dataHandler.Initials != "") {
            Debug.Log("returning, " + GCMap.instance.dataHandler.Initials + " / " + GCMap.instance.dataHandler.IsTeacher.ToString());
            myInitials = GCMap.instance.dataHandler.Initials;
            IsTeacher = GCMap.instance.dataHandler.IsTeacher;

            playerModel.SetActive(!isTeacher);
            teacherModel.SetActive(isTeacher);

            if (isTeacher) {
                animator = teacherModel.GetComponent<Animator>();
            }
        }

        if (!disableGPS) {
            StartCoroutine(LocationHelper.StartLocServices());
        }
    }

    void Update() {
        timeCounter += Time.deltaTime;
        Vector2 relativeLoc = LocationHelper.GetRelativeLocationData(NumberHelper.TuorlaMapBoundaries);

        if (spoofGoodShit) {
            relativeLoc = new Vector2(0.5f, 0.5f);
        }

        if (timeCounter > locUpdateFrequency) {
            if ((locationEnabled || spoofGoodShit) && IsPlayerInBounds(relativeLoc)) {
                LocationInfo loc = LocationHelper.GetLocationData();
                float longitude = loc.longitude;
                float latitude = loc.latitude;

                if (spoofGoodShit) {
                    float padding = 0.2f;
                    float height = NumberHelper.TuorlaMapBoundaries.topBoundary;
                    height -= NumberHelper.TuorlaMapBoundaries.bottomBoundary;

                    latitude = Random.Range(NumberHelper.TuorlaMapBoundaries.bottomBoundary + height * padding, 
                                             NumberHelper.TuorlaMapBoundaries.topBoundary - height * padding);

                    float width = NumberHelper.TuorlaMapBoundaries.rightBoundary;
                    width -= NumberHelper.TuorlaMapBoundaries.leftBoundary;

                    longitude = Random.Range(NumberHelper.TuorlaMapBoundaries.leftBoundary + width * padding,
                                            NumberHelper.TuorlaMapBoundaries.rightBoundary - width * padding);

                    relativeLoc = LocationHelper.RealCoordsToRelativeCoords(new Vector2(longitude, latitude),
                                                                            NumberHelper.TuorlaMapBoundaries);
                    spoofRelative = relativeLoc;
                }

                //Debug.Log("dbid: " + databaseID);

                if (databaseID == "" && myInitials != "") {
                    string name = SystemInfo.deviceUniqueIdentifier;
                    //Debug.Log("posting: " + myInitials + ", " + isTeacher);
                    StartCoroutine(WebHelper.PostLocData(name, longitude, latitude, myInitials, isTeacher));
                    databaseID = "working";
                }
                else if (databaseID == "working") {

                }
                else if (databaseID != "working" && databaseID != "") {
                    //Debug.Log("Putting");
                    StartCoroutine(WebHelper.PutLocData(databaseID, longitude, latitude, isTeacher, myInitials));
                }
            }

            timeCounter = 0f;
        }

        if ((locationEnabled || spoofGoodShit) && IsPlayerInBounds(relativeLoc)) {
            Vector3 targetPos = transform.position;

            if (spoofGoodShit) {
                relativeLoc = spoofRelative;
            }

            Vector3 topLeft = mapBounds.min;
            topLeft.x *= -1;
            Vector3 botRight = mapBounds.max;
            botRight.x *= -1;
            targetPos.x = topLeft.x + relativeLoc.x * (botRight.x - topLeft.x);
            targetPos.z = topLeft.z + relativeLoc.y * (botRight.z - topLeft.z);
            targetPos.y = 30f;
            targetPos = IndicatorHelper.CalculatePosOnGround(targetPos);

            if (transform.position != targetPos) {
                RaycastHit rayHit;
                Physics.Raycast(transform.position, Vector3.down, out rayHit, 50f, 1 << LayerMask.NameToLayer("ground"));

                Quaternion targetRot = Quaternion.LookRotation(targetPos - transform.position, rayHit.normal);
                transform.position = Vector3.MoveTowards(transform.position, targetPos, 0.5f);

                Vector3 pos = transform.position;
                pos.y = 30f;
                Physics.Raycast(pos, Vector3.down, out rayHit, 50f, 1 << LayerMask.NameToLayer("ground"));
                pos = rayHit.point + Vector3.up * 0.025f;
                transform.position = pos;

                transform.rotation = targetRot;
                animator.SetBool("walk", true);
            }
            else {
                animator.SetBool("walk", false);
            }

            Debug.DrawLine(topLeft, mapBounds.center, Color.red);
            Debug.DrawLine(botRight, mapBounds.center, Color.green);
        }

    }

    bool IsPlayerInBounds(Vector2 loc) {
        if (loc.x < 0f || loc.x > 1f) {
            return false;
        }

        if (loc.y < 0f || loc.y > 1f) {
            return false;
        }

        return true;
    }

    internal void LocDoneHandler(bool status) {
        locationEnabled = status;
        //notifs.AddNotification("Loc " + (status ? "success!" : "failure!"));
        LocationHelper.OnLocReady -= LocDoneHandler;
    }

    internal void PostDoneHandler(string id) {
        databaseID = id;
        WebHelper.OnPostReady -= PostDoneHandler;
    }

    internal void PutDoneHandler() {

    }

    public void StartAlert() {
        GCMap.instance.uiController.PlayButtonSFX();
        StartCoroutine(WebHelper.PutMsgData("alert", databaseID));
    }
}

﻿using System;
using UnityEngine;

internal class OtherIndicatorController : MonoBehaviour {
    Vector2 coords;
    string deviceName = "null";
    string id;
    string initials;
    string msg = "";
    bool isTeacher;
    Bounds mapBounds;
    Animator animator;
    double messageStart;

    GameObject playerModel;
    GameObject teacherModel;

    internal Vector2 Coordinates {
        get { return coords; }
        set { coords = value; }
    }

    internal string Name {
        get { return deviceName; }
        set { deviceName = value; }
    }

    internal string ID {
        get { return id; }
        set { id = value; }
    }

    internal string Initials {
        get {
            return initials;
        }
        set {
            initials = value;
        }
    }

    internal bool IsTeacher {
        get {
            return isTeacher;
        }
        set {
            if (isTeacher != value) {
                isTeacher = value;
                playerModel.SetActive(!isTeacher);
                teacherModel.SetActive(isTeacher);

                if (isTeacher) {
                    animator = teacherModel.GetComponent<Animator>();
                }
                else {
                    animator = playerModel.GetComponent<Animator>();
                }
            }
        }
    }

    internal string Message {
        get {
            return msg;
        }
        set {
            msg = value;
            messageStart = DateTime.Now.TimeOfDay.TotalSeconds;
        }
    }

    void Awake() {
        playerModel = transform.GetChild(0).gameObject;
        teacherModel = transform.GetChild(1).gameObject;
    }

    // Use this for initialization
    void Start() {
        mapBounds = GCMap.instance.Bounds;
        animator = isTeacher ? transform.GetChild(1).GetComponent<Animator>() : transform.GetChild(0).GetComponent<Animator>();
    }

    void Update() {
        if (DateTime.Now.TimeOfDay.TotalSeconds - messageStart >= 60f) {
            msg = "";
            messageStart = 0f;
        }

        if (deviceName != SystemInfo.deviceUniqueIdentifier || deviceName != "null") {
            Vector3 targetPos = transform.position;
            Vector3 topLeft = mapBounds.min;
            topLeft.x *= -1;
            Vector3 botRight = mapBounds.max;
            botRight.x *= -1;
            Vector2 relativeLoc = LocationHelper.RealCoordsToRelativeCoords(coords, NumberHelper.TuorlaMapBoundaries);
            targetPos.x = topLeft.x + relativeLoc.x * (botRight.x - topLeft.x);
            targetPos.z = topLeft.z + relativeLoc.y * (botRight.z - topLeft.z);
            targetPos.y = 30f;
            targetPos = IndicatorHelper.CalculatePosOnGround(targetPos);

            if (transform.position != targetPos) {
                RaycastHit rayHit;
                Physics.Raycast(transform.position, Vector3.down, out rayHit, 50f, 1 << LayerMask.NameToLayer("ground"));

                Quaternion targetRot = Quaternion.LookRotation(targetPos - transform.position, rayHit.normal);
                transform.position = Vector3.MoveTowards(transform.position, targetPos, 0.5f);

                Vector3 pos = transform.position;
                pos.y = 30f;
                Physics.Raycast(pos, Vector3.down, out rayHit, 50f, 1 << LayerMask.NameToLayer("ground"));
                pos = rayHit.point + Vector3.up * 0.025f;
                transform.position = pos;

                transform.rotation = targetRot;
                animator.SetBool("walk", true);
            }
            else {
                animator.SetBool("walk", false);
            }
        }
    }
}

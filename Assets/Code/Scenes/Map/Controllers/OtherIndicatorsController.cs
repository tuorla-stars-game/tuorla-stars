﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class OtherIndicatorsController : MonoBehaviour {
    [SerializeField]
    GameObject sample;
    List<OtherIndicatorController> indicators;
    float timeCounter = 0f;

    internal List<OtherIndicatorController> OtherIndicators {
        get {
            return indicators;
        }
    }

    void Awake() {
        indicators = new List<OtherIndicatorController>();
    }

    void Start() {
        WebHelper.OnGetReady += OnGetReadyHandle;
        WebHelper.OnMsgGetReady += OnMsgGetReadyHandle;
    }

    void Update() {
        timeCounter += Time.deltaTime;

        if (timeCounter > 2f) {
            timeCounter = 0f;

            StartCoroutine(WebHelper.GetPlayerDataFromServer());
            StartCoroutine(WebHelper.GetMsgData());
        }
    }

    void OnMsgGetReadyHandle(MsgData[] messages) {
        if (messages.Length > 0) {
            foreach (MsgData message in messages) {
                OtherIndicatorController indicator = indicators.Find(x => x.ID == message.senderID);

                if (indicator != null && indicator.Message == "") {
                    indicator.Message = message.msgString;
                }
            }
        }
    }

    void OnGetReadyHandle(PlayerData[] players) {

        for (int i = indicators.Count - 1; i >= 0; i--) {
            for (int j = players.Length - 1; j >= 0; j--) {
                if (indicators[i].ID == players[j]._id || indicators[i].Name == players[j].name) {
                    indicators[i].Coordinates = new Vector2(players[j].loc[0], players[j].loc[1]);
                    indicators[i].IsTeacher = players[j].isTeacher;
                    indicators[i].Initials = players[j].initials;
                    break;
                }

                if (j == 0) {
                    Destroy(indicators[i].gameObject);
                    indicators.RemoveAt(i);
                }
            }
        }

        int counter = 0;
        int length = players.Length;
        if (Array.Exists<PlayerData>(players, x => x.name == SystemInfo.deviceUniqueIdentifier)) {
            length--;
        }
        while (length > indicators.Count) {
            if (indicators.Exists(x => x.Name == players[counter].name) || players[counter].name == SystemInfo.deviceUniqueIdentifier) {
                counter++;
                continue;
            }

            GameObject newIndicator = Instantiate(sample, transform);
            newIndicator.SetActive(true);
            OtherIndicatorController controller = newIndicator.GetComponent<OtherIndicatorController>();
            controller.enabled = true;

            controller.Name = players[counter].name;
            controller.Coordinates = new Vector2(players[counter].loc[0], players[counter].loc[1]);
            controller.ID = players[counter]._id;
            controller.Initials = players[counter].initials;
            controller.IsTeacher = players[counter].isTeacher;

            indicators.Add(controller);
            counter++;
        }

        if (players.Length < 1) {
            foreach (OtherIndicatorController indicator in indicators) {
                Destroy(indicator.gameObject, 0.25f);
            }

            indicators.Clear();
        }
    }

    internal void EventCleanup() {
        WebHelper.OnGetReady -= OnGetReadyHandle;
        WebHelper.OnMsgGetReady -= OnMsgGetReadyHandle;
    }
}

﻿using UnityEngine;

public class InputHandlerMap : MonoBehaviour {
    protected NotificationsController notifs;
    GCMap gc;
    UIControllerMap uiController;
    CameraController cameraController;

    protected float touchTimer = 0f;
    protected float touchTracker = 0f;
    protected float maxTapTime = 0.15f;
    protected bool touchStartedOverUIObject;
    protected bool isTwoTouch = false;
    Vector2 cameraTouchTracker = new Vector2();

    void Start() {
        notifs = GameObject.FindGameObjectWithTag("Notifications")
                           .GetComponent<NotificationsController>();

        gc = GCMap.instance;
        uiController = GetComponent<UIControllerMap>();
        cameraController = GameObject.FindGameObjectWithTag("MainCamera")
                                     .GetComponent<CameraController>();

    }

    void Update() {
        if (Input.touchCount == 1) {
            TouchSingleHandle();
        }
        else if (Input.touchCount > 1) {
            TouchMultiHandle(Input.touchCount);
        }
    }

    void TouchSingleHandle() {
        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began) {
            touchTimer = 0f;
            touchTracker = 0f;
            touchStartedOverUIObject = InputHelper.IsPointerOverUIObject();
            isTwoTouch = false;
        }
        else if (touch.phase == TouchPhase.Moved) {

        }
        else if (touch.phase == TouchPhase.Stationary) {

        }
        else if (touch.phase == TouchPhase.Ended) {
            if (!isTwoTouch) {
                TouchSingleEndHandle(touch.position);
            }

            isTwoTouch = false;
            touchStartedOverUIObject = false;
            return;
        }

        if (touchStartedOverUIObject) {

        }
        else {
            touchTimer += Time.deltaTime;
            touchTracker += touch.deltaPosition.magnitude;
            cameraTouchTracker.x = touch.deltaPosition.x;
            cameraTouchTracker.y = touch.deltaPosition.y;
            cameraController.TouchOrbitPos += cameraTouchTracker.x;
            cameraController.TouchHeightPos -= cameraTouchTracker.y;
        }
    }

    void TouchSingleEndHandle(Vector2 pos) {
        if (touchStartedOverUIObject) {

        }
        else {
            Ray ray = Camera.current.ScreenPointToRay(pos);
            bool doTeleport = !gc.IsUIElementActive;

            RaycastHit rayHit;
            if (Physics.Raycast(ray, out rayHit) &&
                touchTimer < maxTapTime &&
                touchTracker < 10f) {

                //notifs.AddNotification(rayHit.collider.name +
                //                  ", touchTimer: " + touchTimer +
                //                  ", touchTracker: " + touchTracker);

                POIController toucheePOI = rayHit.collider.GetComponent<POIController>();
                CameraFocuser toucheeCF = rayHit.collider.GetComponent<CameraFocuser>();

                bool doCenterCam = toucheeCF == null ? false : true;

                if (toucheePOI != null && !gc.IsUIElementActive) {
                    toucheePOI.OnTouch();
                }
                else {
                    if (gc.IsUIElementActive) {
                        Debug.Log("being called from inputhandler route 1");
                        uiController.csController.CloseCardOnTop();
                    }

                    if (doTeleport && !doCenterCam) {
                        gc.TeleportIndicator(rayHit.point);
                    }
                }

                if (doCenterCam) {
                    toucheeCF.CenterCameraOnMe(true);
                }
            }
            else {
                if (gc.IsUIElementActive) {
                    Debug.Log("being called from inputhandler route 2");
                    uiController.csController.CloseCardOnTop();
                }
            }
        }
    }

    void TouchMultiHandle(int amount) {
        if (amount == 2) {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            float deltaDeltaTouch = InputHelper.TouchTwoDeltaDeltaCalc(touch1, touch2);

            if (touch2.phase == TouchPhase.Began) {
                isTwoTouch = true;
            }
            else if (touch2.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved) {
                cameraController.TouchZoomAmount += deltaDeltaTouch;
            }
            else if (touch1.phase == TouchPhase.Ended || touch2.phase == TouchPhase.Ended) {

            }
        }
    }

}

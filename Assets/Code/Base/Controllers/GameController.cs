﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Loc {
    Finnish,
    English,
    Swedish
}

public abstract class GameController : MonoBehaviour {
    internal static GameController instance;
    bool uiElementActive = false;
    internal Loc loc = Loc.Finnish;
    float loadTimer = 0f;
    AsyncOperation loadingOperation;
    internal DataHandler dataHandler;
    internal UIController uiController;
    internal CardLibrary cardLibrary;

    [SerializeField]
    CardAddGraphicsController cardsAnimController;
    [SerializeField]
    protected LocalizerShared localizerShared;

    internal AudioSource musicPlayer;
    internal AudioSource sfxPlayer;

    internal bool IsUIElementActive {
        get {
            return uiElementActive;
        }

        set {
            uiElementActive = value;
        }
    }

    void Awake() {
        if (instance == null) {
            instance = this;
            Debug.Log("load ends: " + DateTime.Now.TimeOfDay.TotalMilliseconds / 1000f);
        }
        else if (instance != this) {
            Destroy(gameObject);
        }

        SubAwake();

        loc = dataHandler.Localization;

        musicPlayer = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
        sfxPlayer = musicPlayer.transform.GetChild(0).GetComponent<AudioSource>();

        musicPlayer.volume = dataHandler.MusicVolume;
        sfxPlayer.volume = dataHandler.SFXVolume;
    }

    protected abstract void SubAwake();

    void Start() {
        dataHandler = DataHandler.instance;
        cardLibrary = CardLibrary.instance;
        dataHandler.OnCardAddReady += OnAddCardReady;
        uiController = GetUIController();

        SubStart();
        ApplyLocalization((int)loc);
    }

    protected abstract void SubStart();

    internal abstract UIController GetUIController();

    void Update () {
        if (loadTimer > 0f) {
            HandleLoading();
        }

        SubUpdate();
    }

    protected abstract void SubUpdate();

    internal void AddPart(int part) {
        dataHandler.AddTelescopePart(part);

        if (dataHandler.HasAllTelescopeParts()) {
            GrantTelescopeAccess();
        }
    }

    internal virtual void GrantTelescopeAccess() {
        Debug.Log("access to tele granted");
    }

    internal void AddCard(string cat, int ID) {
        dataHandler.AddCard(cat, ID);
        cardsAnimController.AddCardGraphic(1);
        dataHandler.SaveData();
    }

    internal void BulkAddCard(CardData[] cards) {
        foreach (CardData card in cards) {
            dataHandler.AddCard(card.category, card.ID);
        }

        cardsAnimController.AddCardGraphic(cards.Length);
        dataHandler.SaveData();
    }

    internal void HandleLoading() {
        loadTimer += Time.deltaTime;

        if (loadTimer > 1f) {
            loadTimer = 0f;
            loadingOperation.allowSceneActivation = true;
        }
    }

    public void LoadScene(string sceneName) {
        if (loadTimer == 0f) {
            if (sceneName == "backScene") {
                sceneName = dataHandler.LastSceneName;
            }

            CleanUpBeforeLoad();
            dataHandler.OnCardAddReady -= OnAddCardReady;
            loadingOperation = SceneManager.LoadSceneAsync(sceneName);
            loadingOperation.allowSceneActivation = false;
            Debug.Log("load starts: " + DateTime.Now.TimeOfDay.TotalMilliseconds / 1000f);
            dataHandler.LastSceneName = SceneManager.GetActiveScene().name;

            loadTimer += Time.deltaTime;
        }
    }

    protected abstract void CleanUpBeforeLoad();

    public void ResetData() {
        uiController.PlayButtonSFX();
        dataHandler.DeleteData();
        uiController.UpdateProgress();

        string msg = "";

        switch (loc) {
            case Loc.Finnish:
                msg = "Pelin data nollattu!";
                break;
            case Loc.Swedish:
                msg = "(text behövs)";
                break;
            default:
                msg = "Game data reset!";
                break;
        }

        uiController.notifs.AddNotification(msg);
    }

    public void CheatAddCards(string cat) {
        dataHandler.CheatAddCards(cat);
    }

    internal void OnAddCardReady() {
        string msg = "";

        switch (loc) {
            case Loc.Finnish:
                msg = "Sait uuden kortin kirjastoosi!";
                break;
            case Loc.Swedish:
                msg = "(text behövs)";
                break;
            default:
                Debug.Log("Card added to library!");
                break;
        }

        uiController.notifs.AddNotification(msg);
    }

    public void ApplyLocalization(int newLoc) {
        if (newLoc >= 10) {
            newLoc -= 10;
            uiController.PlayButtonSFX();
        }

        Loc _newLoc = (Loc)newLoc;
        loc = _newLoc;
        dataHandler.Localization = loc;
        cardLibrary.Localization = loc;

        Debug.Log("Applying localization: " + loc);

        if (localizerShared != null) {
            localizerShared.enabled = true;
        }

        SubApplyLocalization();
    }

    public void QuitButton() {
        dataHandler.SaveData();
        Application.Quit();
    }

    protected abstract void SubApplyLocalization();

    public void SetMusicVol(Slider slider) {
        dataHandler.MusicVolume = slider.value;
        musicPlayer.volume = slider.value;
    }

    public void SetSFXVol(Slider slider) {
        dataHandler.SFXVolume = slider.value;
        sfxPlayer.volume = slider.value;
    }
}

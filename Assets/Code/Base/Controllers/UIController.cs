﻿using UnityEngine;
using UnityEngine.UI;

internal abstract class UIController : MonoBehaviour {
    LoadIndicatorController loadingIndicator;
    HexMenuController hexMenu;
    UIWindowController uiWindow;
    internal ProgressBar progressBar;
    Image dimScreen;
    protected GameController gc;
    internal NotificationsController notifs;

    bool dim = false;
    float maxDim = 0.7f;
    float dimSpeed = 4f;
    protected int openHexMenuId = 0;
    protected int openWindowId = 0;

    float cardsAddedTimer = 0f;
    const float WAITAFTERCARDADD = 1f;

    [SerializeField]
    internal CardStackController csController;

    [SerializeField]
    bool closeWindow = false;

    [SerializeField]
    internal AudioSource buttonSfx;

    internal bool IsDimActive {
        get {
            return dim;
        }
    }

    internal bool IsUIElementActive {
        get {
            return gc.IsUIElementActive;
        }
        set {
            gc.IsUIElementActive = value;
        }
    }

    void Awake() {
        loadingIndicator = GetComponentInChildren<LoadIndicatorController>();
        hexMenu = GetComponentInChildren<HexMenuController>();
        uiWindow = GetComponentInChildren<UIWindowController>();
        dimScreen = GameObject.FindGameObjectWithTag("Dim").GetComponent<Image>();

        notifs = GetComponentInChildren<NotificationsController>();

        SubAwake();
    }

    protected abstract void SubAwake();

    void Start () {
        gc = GetCameController();
        Debug.Log("gc assigned: " + gc);
        gc.dataHandler.OnCardAddReady += OnAddCardReady;
        progressBar = GetComponentInChildren<ProgressBar>();
        UpdateProgress();

        SubStart();
	}

    protected abstract GameController GetCameController();

    protected abstract void SubStart();

	void Update () {
        if (dim && dimScreen.color.a < maxDim) {
            Color newColor = dimScreen.color;
            newColor.a += dimSpeed * Time.deltaTime;
            dimScreen.color = newColor;
        }
        else if (dim && dimScreen.color.a > maxDim) {
            Color newColor = dimScreen.color;
            newColor.a = maxDim;
            dimScreen.color = newColor;
        }
        else if (!dim && dimScreen.color.a > 0f) {
            Color newColor = dimScreen.color;
            newColor.a -= dimSpeed * Time.deltaTime;
            dimScreen.color = newColor;
        }

        if (cardsAddedTimer > 0f) {
            cardsAddedTimer += Time.deltaTime;

            if (cardsAddedTimer > WAITAFTERCARDADD) {
                cardsAddedTimer = 0f;
                UpdateProgress();
            }
        }

        if (closeWindow) {
            CloseCardOnTop();
            closeWindow = false;
        }

        SubUpdate();
    }

    public void CloseCardOnTop() {
        csController.CloseCardOnTop();
    }

    protected abstract void SubUpdate();

    internal void UpdateProgress() {
        float progress = DataHandler.instance.GetProgress();
        if (progressBar != null) {
            progressBar.SetProgress(progress);
        }
    }

    internal void ToggleDim() {
        dim = !dim;

        if (dim) {
            Color newColor = dimScreen.color;
            newColor.a += 0.1f;
            dimScreen.color = newColor;
        }
    }

    internal void PlayButtonSFX() {
        buttonSfx.PlayOneShot(buttonSfx.clip);
    }

    public void LoadScene(string sceneName) {
        if (openWindowId != 0) {
            return;
        }

        if (buttonSfx != null) {
            PlayButtonSFX();
        }

        if (openHexMenuId != 0) {
            hexMenu.ToggleMenu(openHexMenuId);
            openHexMenuId = 0;
        }
        else if (gc.IsUIElementActive && openWindowId == 0 && openHexMenuId == 0) {
            Debug.Log("being called from loadscene");
            ClearUIElements();
        }
        else {
            ToggleDim();
        }

        loadingIndicator.Display();
        gc.LoadScene(sceneName);
        gc.dataHandler.OnCardAddReady -= OnAddCardReady;
    }

    internal abstract void ClearUIElements();

    public void ToggleHexMenu(int menuId) {
        if (buttonSfx != null) {
            PlayButtonSFX();
        }

        if (openWindowId == 0) {
            if (gc.IsUIElementActive && openHexMenuId != 0 ||
                !gc.IsUIElementActive) {
                if (openHexMenuId != 0 && openHexMenuId != menuId) {
                    hexMenu.ToggleMenu(openHexMenuId);
                }
                else {
                    ToggleDim();
                    gc.IsUIElementActive = !gc.IsUIElementActive;
                }

                hexMenu.ToggleMenu(menuId);

                if (openHexMenuId == 0 || openHexMenuId != menuId) {
                    openHexMenuId = menuId;
                }
                else {
                    openHexMenuId = 0;
                }
            }
        }
    }

    public void ToggleWindow(int id) {
        if (buttonSfx != null) {
            PlayButtonSFX();
        }

        Debug.Log("Toggle window called, id: " + id);

        if (openHexMenuId != 0 && openWindowId == 0) {
            gc.IsUIElementActive = false;
            hexMenu.ToggleMenu(openHexMenuId);
        }

        if (!gc.IsUIElementActive || openWindowId == id) {
            gc.IsUIElementActive = !gc.IsUIElementActive;
            uiWindow.ToggleWindow(id);

            if (openWindowId == 0) {
                openWindowId = id;
                dimScreen.transform.SetSiblingIndex(5);
            }
            else {
                openWindowId = 0;
                openHexMenuId = 0;
                ToggleDim();
                dimScreen.transform.SetSiblingIndex(0);
            }
        }
    }

    internal void OnAddCardReady() {
        cardsAddedTimer += Mathf.Epsilon;
    }
}

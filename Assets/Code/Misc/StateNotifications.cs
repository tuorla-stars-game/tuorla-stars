﻿public struct StateNotifications {
    public string STATE_ENTRY;

    public string STATE_EXIT;

    public string[] STATE_PROGRESS;
}

public struct StateNotificationsRaw {
    public string STATE_ENTRY_FI;
    public string STATE_ENTRY_EN;
    public string STATE_ENTRY_SV;

    public string STATE_EXIT_FI;
    public string STATE_EXIT_EN;
    public string STATE_EXIT_SV;

    public string[] STATE_PROGRESS_FI;
    public string[] STATE_PROGRESS_EN;
    public string[] STATE_PROGRESS_SV;
}
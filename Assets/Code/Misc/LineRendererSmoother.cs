﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererSmoother : MonoBehaviour {
    int circleSegments = 0;

    LineRenderer myLineRenderer;

    internal int OrbitSegments {
        get {
            return circleSegments;
        }
        set {
            circleSegments = value;
        }
    }

	void Awake () {
        myLineRenderer = GetComponent<LineRenderer>();
    }
	

    Vector3[] CalculateSegmentPositions(float zLeft, float xTop) {
        Vector3[] segPoses = new Vector3[circleSegments];
        myLineRenderer.positionCount = circleSegments;
        /*
        float zLeft = myOrbitBP[0].z;
        float zRight = myOrbitBP[2].z;
        float zDist = Mathf.Abs(zRight) + Mathf.Abs(zLeft);

        float xTop = myOrbitBP[1].x;
        float xBot = myOrbitBP[3].x;
        float xDist = Mathf.Abs(xTop) + Mathf.Abs(xBot);
        */

        for (int i = 0; i < segPoses.Length; i++) {
            float _currentArc = ((float)i + 1f) / ((float)circleSegments / 4f);
            int currentArc = Mathf.CeilToInt(_currentArc - 1f);
            //Debug.Log("currentArc: " + currentArc);

            /*float xMin = myOrbitBP[currentArc].x;
            float zMin = myOrbitBP[currentArc].z;
            float xMax;
            float zMax;

            if (currentArc == 3) {
                xMax = myOrbitBP[0].x;
                zMax = myOrbitBP[0].z;
            }
            else {
                xMax = myOrbitBP[currentArc + 1].x;
                zMax = myOrbitBP[currentArc + 1].z;

            }

            float xFactor = 0f;
            float zFactor = 0f;
            xFactor = xMax - xMin;
            zFactor = zMax - zMin;*/

            float posOnPerim = (float)i / (float)circleSegments;
            Debug.Log("posonperim: " + posOnPerim);

            Vector3 segPos = new Vector3(Mathf.Sin(posOnPerim * 2f * Mathf.PI) * xTop,
                                         0f,
                                         Mathf.Sin(posOnPerim * 2f * Mathf.PI + Mathf.PI / 2f) * zLeft);

            segPoses[i] = segPos;
        }

        return segPoses;
    }

    internal void SetupOrbitLine(float semimajorAxis, float semiminorAxis, int orbitSegments) {
        circleSegments = orbitSegments;
        myLineRenderer.SetPositions(CalculateSegmentPositions(semimajorAxis, semiminorAxis));
    }
}

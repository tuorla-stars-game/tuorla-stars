﻿using System;

[Serializable]
public struct SharedStrings {
    public string TUTORIAL_CARD_LIBRARY_FI;
    public string TUTORIAL_CARD_LIBRARY_EN;
    public string TUTORIAL_CARD_LIBRARY_SV;

    public string TUTORIAL_MAIN_MENU_FI;
    public string TUTORIAL_MAIN_MENU_EN;
    public string TUTORIAL_MAIN_MENU_SV;

    public string TUTORIAL_NOTIFICATIONS_FI;
    public string TUTORIAL_NOTIFICATIONS_EN;
    public string TUTORIAL_NOTIFICATIONS_SV;

    public string TUTORIAL_CAMERA_RESET_FI;
    public string TUTORIAL_CAMERA_RESET_EN;
    public string TUTORIAL_CAMERA_RESET_SV;

    public string TUTORIAL_SCENE_MENU_FI;
    public string TUTORIAL_SCENE_MENU_EN;
    public string TUTORIAL_SCENE_MENU_SV;



    public string INFO_WINDOW_HEADER_FI;
    public string INFO_WINDOW_HEADER_EN;
    public string INFO_WINDOW_HEADER_SV;

    public string INFO_GAME_DESIGN_FI;
    public string INFO_GAME_DESIGN_EN;
    public string INFO_GAME_DESIGN_SV;

    public string INFO_GRAPHICS_FI;
    public string INFO_GRAPHICS_EN;
    public string INFO_GRAPHICS_SV;

    public string INFO_PROGRAMMING_FI;
    public string INFO_PROGRAMMING_EN;
    public string INFO_PROGRAMMING_SV;

    public string INFO_CONTENT_N_SOUND_FI;
    public string INFO_CONTENT_N_SOUND_EN;
    public string INFO_CONTENT_N_SOUND_SV;

    public string INFO_PRODUCER_FI;
    public string INFO_PRODUCER_EN;
    public string INFO_PRODUCER_SV;

    public string INFO_DIRECTOR_FI;
    public string INFO_DIRECTOR_EN;
    public string INFO_DIRECTOR_SV;



    public string SETTINGS_WINDOW_HEADER_FI;
    public string SETTINGS_WINDOW_HEADER_EN;
    public string SETTINGS_WINDOW_HEADER_SV;

    public string SETTINGS_VOLUME_FI;
    public string SETTINGS_VOLUME_EN;
    public string SETTINGS_VOLUME_SV;

    public string SETTINGS_VOLUME_MUSIC_FI;
    public string SETTINGS_VOLUME_MUSIC_EN;
    public string SETTINGS_VOLUME_MUSIC_SV;

    public string SETTINGS_VOLUME_SFX_FI;
    public string SETTINGS_VOLUME_SFX_EN;
    public string SETTINGS_VOLUME_SFX_SV;

    public string SETTINGS_LANGUAGE_FI;
    public string SETTINGS_LANGUAGE_EN;
    public string SETTINGS_LANGUAGE_SV;

    public string SETTINGS_GAME_FI;
    public string SETTINGS_GAME_EN;
    public string SETTINGS_GAME_SV;

    public string SETTINGS_GRAPHICS_FI;
    public string SETTINGS_GRAPHICS_EN;
    public string SETTINGS_GRAPHICS_SV;

    public string SETTINGS_GPS_RESET_FI;
    public string SETTINGS_GPS_RESET_EN;
    public string SETTINGS_GPS_RESET_SV;

    public string SETTINGS_GAME_RESET_FI;
    public string SETTINGS_GAME_RESET_EN;
    public string SETTINGS_GAME_RESET_SV;
}

[Serializable]
public struct GameSceneStrings {
    public string TUTORIAL_SAMPLE_NOTIFICATION_FI;
    public string TUTORIAL_SAMPLE_NOTIFICATION_EN;
    public string TUTORIAL_SAMPLE_NOTIFICATION_SV;

    public string TUTORIAL_BUILDING_HEXAGON_FI;
    public string TUTORIAL_BUILDING_HEXAGON_EN;
    public string TUTORIAL_BUILDING_HEXAGON_SV;

    public string TUTORIAL_TELESCOPE_PART_FI;
    public string TUTORIAL_TELESCOPE_PART_EN;
    public string TUTORIAL_TELESCOPE_PART_SV;

    public string TUTORIAL_HEX_MENU_FI;
    public string TUTORIAL_HEX_MENU_EN;
    public string TUTORIAL_HEX_MENU_SV;



    public string INITIALS_ENTER_FI;
    public string INITIALS_ENTER_EN;
    public string INITIALS_ENTER_SV;

    public string INITIALS_TYPE_HERE_FI;
    public string INITIALS_TYPE_HERE_EN;
    public string INITIALS_TYPE_HERE_SV;



    public string UNLOCK_TELESCOPE_FI;
    public string UNLOCK_TELESCOPE_EN;
    public string UNLOCK_TELESCOPE_SV;

    public string UNLOCK_TELESCOPE_DIRECTIONS_FI;
    public string UNLOCK_TELESCOPE_DIRECTIONS_EN;
    public string UNLOCK_TELESCOPE_DIRECTIONS_SV;



    public string CAVE_MAP_MATERIALSTORAGE_FI;
    public string CAVE_MAP_MATERIALSTORAGE_EN;
    public string CAVE_MAP_MATERIALSTORAGE_SV;

    public string CAVE_MAP_MIRROR_COATING_FI;
    public string CAVE_MAP_MIRROR_COATING_EN;
    public string CAVE_MAP_MIRROR_COATING_SV;

    public string CAVE_MAP_MIRROR_POLISHING_FI;
    public string CAVE_MAP_MIRROR_POLISHING_EN;
    public string CAVE_MAP_MIRROR_POLISHING_SV;

    public string CAVE_MAP_TEST_TUNNEL_FI;
    public string CAVE_MAP_TEST_TUNNEL_EN;
    public string CAVE_MAP_TEST_TUNNEL_SV;

    public string CAVE_MAP_MEASURING_TUNNEL_FI;
    public string CAVE_MAP_MEASURING_TUNNEL_EN;
    public string CAVE_MAP_MEASURING_TUNNEL_SV;
}

[Serializable]
public struct TelescopeStrings {
    public string TUTORIAL_SAMPLE_NOTIFICATION_FI;
    public string TUTORIAL_SAMPLE_NOTIFICATION_EN;
    public string TUTORIAL_SAMPLE_NOTIFICATION_SV;

    public string TUTORIAL_NOTIFICATION_EXPLANATION_FI;
    public string TUTORIAL_NOTIFICATION_EXPLANATION_EN;
    public string TUTORIAL_NOTIFICATION_EXPLANATION_SV;

    public string TUTORIAL_DRAG_PART_EXPLANATION_FI;
    public string TUTORIAL_DRAG_PART_EXPLANATION_EN;
    public string TUTORIAL_DRAG_PART_EXPLANATION_SV;



    public string UNLOCK_SOLAR_SYSTEM_FI;
    public string UNLOCK_SOLAR_SYSTEM_EN;
    public string UNLOCK_SOLAR_SYSTEM_SV;

    public string UNLOCK_SOLAR_SYSTEM_DIRECTIONS_FI;
    public string UNLOCK_SOLAR_SYSTEM_DIRECTIONS_EN;
    public string UNLOCK_SOLAR_SYSTEM_DIRECTIONS_SV;
}

[Serializable]
public struct SolarStrings {
    public string TUTORIAL_MOON_BROWSER_FI;
    public string TUTORIAL_MOON_BROWSER_EN;
    public string TUTORIAL_MOON_BROWSER_SV;

    public string TUTORIAL_PLANET_BROWSER_FI;
    public string TUTORIAL_PLANET_BROWSER_EN;
    public string TUTORIAL_PLANET_BROWSER_SV;

    public string TUTORIAL_TIME_CONTROLS_FI;
    public string TUTORIAL_TIME_CONTROLS_EN;
    public string TUTORIAL_TIME_CONTROLS_SV;

    public string TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_FI;
    public string TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_EN;
    public string TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_SV;
}

[Serializable]
public struct QuizStrings {
    public string QUESTION_ANSWER_CORRECT_FI;
    public string QUESTION_ANSWER_CORRECT_EN;
    public string QUESTION_ANSWER_CORRECT_SV;

    public string QUESTION_ANSWER_INCORRECT_FI;
    public string QUESTION_ANSWER_INCORRECT_EN;
    public string QUESTION_ANSWER_INCORRECT_SV;



    public string FINISH_SCREEN_CONGRATULATIONS_FI;
    public string FINISH_SCREEN_CONGRATULATIONS_EN;
    public string FINISH_SCREEN_CONGRATULATIONS_SV;

    public string FINISH_SCREEN_START_FI;
    public string FINISH_SCREEN_START_EN;
    public string FINISH_SCREEN_START_SV;

    public string FINISH_SCREEN_REST_FI;
    public string FINISH_SCREEN_REST_EN;
    public string FINISH_SCREEN_REST_SV;
}
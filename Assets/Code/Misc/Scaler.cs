﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour {
    Vector3 assignedScale;

    [SerializeField]
    internal float scalingSpeed = 1f;

    internal Vector3 TargetScale {
        get {
            return assignedScale;
        }
        set {
            assignedScale = value;
        }
    }

    void Start() {
        assignedScale = transform.localScale;    
    }

    void Update() {
        //Debug.Log("sun scaler start frame#:" + Time.frameCount);
        if (transform.localScale != assignedScale) {
            transform.localScale = Vector3.Lerp(transform.localScale, assignedScale, Time.deltaTime * scalingSpeed);

            float dist = Vector3.Distance(transform.localScale, assignedScale);

            if (dist < 0.05f) {
                transform.localScale = assignedScale;
            }
        }

        //Debug.Log("sun scaler end frame#:" + Time.frameCount);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChanger : MonoBehaviour {
    [SerializeField]
    AudioClip[] audioClips;

    AudioSource myAudioSource;

    int currentSong = 0;

    void Awake() {
        myAudioSource = GetComponent<AudioSource>();
    }

    public void NextSong() {
        if (audioClips.Length > 0) {
            currentSong++;

            if (currentSong >= audioClips.Length) {
                currentSong = 0;
            }

            myAudioSource.clip = audioClips[currentSong];
            myAudioSource.Play();
            GCMap.instance.uiController.buttonSfx.PlayOneShot(GCMap.instance.uiController.buttonSfx.clip);
            GCMap.instance.notifs.AddNotification("Now playing " + audioClips[currentSong].ToString());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICardAddGraphicAnimator : MonoBehaviour {
    [SerializeField]
    Transform animTargetPos;

    CanvasGroup cg;
    float animTimer = 0f;

    [SerializeField]
    float movementStartTime = 2f;
    [SerializeField]
    float animDuration = 4f;
    [SerializeField]
    float peakOpacity = 0.3f;

    float baseOpacity = 0f;

    float startScale = 0.8f;

    void Awake() {
        cg = GetComponent<CanvasGroup>();
        startScale = transform.localScale.x;
        baseOpacity = cg.alpha;
    }

    void OnEnable() {
        animTimer = 0f;
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one * startScale;
    }

    void Update () {
        animTimer += Time.deltaTime;

        if (animTimer < movementStartTime) {
            float animProgress = animTimer / movementStartTime;
            transform.localScale = Vector3.one * startScale + animProgress * Vector3.one * (1f - startScale);
            cg.alpha = baseOpacity + peakOpacity * animProgress;
        }
        else {
            float animProgress = (animTimer - movementStartTime) / (animDuration - movementStartTime);

            Vector3 startPos = transform.position;
            Vector3 newPos = Vector3.Lerp(startPos, animTargetPos.position, animProgress * 0.5f);
            transform.position = newPos;
            transform.localScale = transform.localScale * (1f - animProgress);
            cg.alpha = baseOpacity + peakOpacity * (1f - animProgress);

            if (animProgress > 1f) {
                enabled = false;
                Destroy(gameObject);
            }
        }
	}
}

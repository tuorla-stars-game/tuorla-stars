﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToCamRotator : MonoBehaviour {
    [SerializeField]
    Camera mainCam;
	
	void Update () {
        transform.rotation = Quaternion.LookRotation(mainCam.transform.position - transform.position);
	}
}

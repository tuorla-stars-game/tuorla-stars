﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationSetter : MonoBehaviour {
    [SerializeField]
    ScreenOrientation orientation;

	void Start () {
        Screen.orientation = orientation;
	}
}

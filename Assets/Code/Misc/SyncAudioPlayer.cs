﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncAudioPlayer : MonoBehaviour {
    AudioSource audioSource;

	void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
	void Update () {
	}

    public void ToggleMusicPlayback() {
        if (!audioSource.isPlaying) {
            float audioTime = (float)DateTime.Now.TimeOfDay.TotalMilliseconds / 1000f;
            audioTime = audioTime % audioSource.clip.length;
            audioSource.Play();
            audioSource.time = audioTime;
        }
        else {
            audioSource.Stop();
        }
    }
}

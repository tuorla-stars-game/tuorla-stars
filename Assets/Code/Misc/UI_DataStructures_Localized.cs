﻿using System;

internal struct LocalizedSharedStrings {
    internal string TUTORIAL_CARD_LIBRARY;
    internal string TUTORIAL_MAIN_MENU;
    internal string TUTORIAL_NOTIFICATIONS;
    internal string TUTORIAL_CAMERA_RESET;
    internal string TUTORIAL_SCENE_MENU;

    internal string INFO_WINDOW_HEADER;
    internal string INFO_GAME_DESIGN;
    internal string INFO_GRAPHICS;
    internal string INFO_PROGRAMMING;
    internal string INFO_CONTENT_N_SOUND;
    internal string INFO_PRODUCER;
    internal string INFO_DIRECTOR;

    internal string SETTINGS_WINDOW_HEADER;
    internal string SETTINGS_VOLUME;
    internal string SETTINGS_VOLUME_MUSIC;
    internal string SETTINGS_VOLUME_SFX;
    internal string SETTINGS_LANGUAGE;
    internal string SETTINGS_GAME;
    internal string SETTINGS_GRAPHICS;
    internal string SETTINGS_GPS_RESET;
    internal string SETTINGS_GAME_RESET;
}

internal struct LocalizedGameSceneStrings {
    internal string TUTORIAL_SAMPLE_NOTIFICATION;
    internal string TUTORIAL_BUILDING_HEXAGON;
    internal string TUTORIAL_TELESCOPE_PART;
    internal string TUTORIAL_HEX_MENU;

    internal string INITIALS_ENTER;
    internal string INITIALS_TYPE_HERE;

    internal string UNLOCK_TELESCOPE;
    internal string UNLOCK_TELESCOPE_DIRECTIONS;

    internal string CAVE_MAP_MATERIALSTORAGE;
    internal string CAVE_MAP_MIRROR_COATING;
    internal string CAVE_MAP_MIRROR_POLISHING;
    internal string CAVE_MAP_TEST_TUNNEL;
    internal string CAVE_MAP_MEASURING_TUNNEL;

}

internal struct LocalizedTelescopeStrings {
    internal string TUTORIAL_SAMPLE_NOTIFICATION;
    internal string TUTORIAL_NOTIFICATION_EXPLANATION;
    internal string TUTORIAL_DRAG_PART_EXPLANATION;

    internal string UNLOCK_SOLAR_SYSTEM;
    internal string UNLOCK_SOLAR_SYSTEM_DIRECTIONS;
}

internal struct LocalizedSolarStrings {
    internal string TUTORIAL_MOON_BROWSER;
    internal string TUTORIAL_PLANET_BROWSER;
    internal string TUTORIAL_TIME_CONTROLS;
    internal string TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS;
}

internal struct LocalizedQuizStrings {
    internal string QUESTION_ANSWER_CORRECT;
    internal string QUESTION_ANSWER_INCORRECT;

    internal string FINISH_SCREEN_CONGRATULATIONS;
    internal string FINISH_SCREEN_START;
    internal string FINISH_SCREEN_REST;
}
﻿using System.Collections;
using UnityEngine;

internal static class LocationHelper {
    const string putUri = "http://tigsune.eu:8080/api/players/";
    const string postUri = "http://tigsune.eu:8080/api/players";

    internal delegate void LocStartupDone(bool status);
    internal static event LocStartupDone OnLocReady;

    static internal Vector2 RealCoordsToRelativeCoords(Vector2 playerCoords, MapBoundaries bounds) {
        Vector2 relativeCoords = new Vector2();
        relativeCoords.x = playerCoords.x - bounds.leftBoundary;
        relativeCoords.x /= bounds.rightBoundary - bounds.leftBoundary;

        relativeCoords.y = playerCoords.y - bounds.topBoundary;
        relativeCoords.y /= bounds.bottomBoundary - bounds.topBoundary;

        return relativeCoords;
    }

    static internal IEnumerator StartLocServices() {
        if (!Input.location.isEnabledByUser) {
            //notifs.AddNotification("Error: Location is not enabled!");
            LocDoneHelper(false);
            yield break;
        }

        Input.location.Start();
        int timeoutTimer = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && timeoutTimer > 0) {
            yield return new WaitForSecondsRealtime(1f);
            timeoutTimer--;
        }

        if (timeoutTimer < 1) {
            //notifs.AddNotification("Location service failed to start before timeout duration.");
            LocDoneHelper(false);
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed) {
            //notifs.AddNotification("Unable to determine location, location service failed.");
            LocDoneHelper(false);
            yield break;
        }

        LocDoneHelper(true);
    }

    static void LocDoneHelper(bool status) {
        if (OnLocReady != null) {
            OnLocReady(status);
        }
    }

    static internal LocationInfo GetLocationData() {
        LocationInfo locData = Input.location.lastData;
        float longitude = locData.longitude;
        float latitude = locData.latitude;

        return locData;
    }

    static internal Vector2 GetRelativeLocationData(MapBoundaries bounds) {
        LocationInfo loc = GetLocationData();
        Vector2 playerCoords = new Vector2(loc.longitude, loc.latitude);
        return RealCoordsToRelativeCoords(playerCoords, bounds);
    }
}
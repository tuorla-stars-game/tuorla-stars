﻿using UnityEngine;

internal static class CameraHelper {
    internal static Vector3 CalculateQuadraticBezierPoint(Vector3[] positions, float time) {
        Vector3 pos0 = positions[0];
        Vector3 pos1 = positions[1];
        Vector3 pos2 = positions[2];

        Vector3 point = Mathf.Pow(1f - time, 2f) * pos0;
        point += 2f * (1f - time) * time * pos1;
        point += Mathf.Pow(time, 2f) * pos2;

        return point;
    }

    internal static Vector3 CalculateCubicBezierPoint(Vector3[] positions, float time) {
        Vector3 pos0 = positions[0];
        Vector3 pos1 = positions[1];
        Vector3 pos2 = positions[2];
        Vector3 pos3 = positions[3];

        Vector3 point1 = CalculateQuadraticBezierPoint(new Vector3[3] { pos0, pos1, pos2 }, time);
        Vector3 point2 = CalculateQuadraticBezierPoint(new Vector3[3] { pos1, pos2, pos3 }, time);
        Vector3 point = (1f - time) * point1 + time * point2;

        return point;
    }
}

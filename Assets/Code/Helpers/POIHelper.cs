﻿using UnityEngine;

internal static class POIHelper {
    internal static Sprite LoadSprite(string path) {
        Sprite textureFile = Resources.Load<Sprite>(path);
        return textureFile;
    }

    internal static POIData LoadPOIData(string name) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/PointsOfInterest/json/" + name);
        POIData POIDataObject = JsonUtility.FromJson<POIData>(jsonFile.text);

        if (POIDataObject.imagePath == "") {
            POIDataObject.imagePath = "Resources/Content/PointsOfInterest/Images/nature";
        }

        return POIDataObject;
    }
}

public struct POIData {
    public string header;
    public string imagePath;
    public string body;

    internal POIData(string header, string imagePath, string body) {
        this.header = header;
        this.imagePath = imagePath;
        this.body = body;
    }
}
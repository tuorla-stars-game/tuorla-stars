﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LocalizationDataLoader {

    // ####################################################
    /* This code is for loading the shared data strings  */
    // ####################################################

    internal static LocalizedSharedStrings LoadLocalizedSharedData(Loc loc = Loc.English) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Localization/UI/shared");
        SharedStrings sharedStringsRaw = JsonUtility.FromJson<SharedStrings>(jsonFile.text);

        LocalizedSharedStrings locSharedStrings;
        switch (loc) {
            case Loc.Finnish:
                locSharedStrings = LoadSharedFinnish(sharedStringsRaw);
                break;
            case Loc.English:
                locSharedStrings = LoadSharedEnglish(sharedStringsRaw);
                break;
            case Loc.Swedish:
                locSharedStrings = LoadSharedSwedish(sharedStringsRaw);
                break;
            default:
                locSharedStrings = LoadSharedEnglish(sharedStringsRaw);
                break;
        }

        return locSharedStrings;
    }

    static LocalizedSharedStrings LoadSharedFinnish(SharedStrings ss) {
        LocalizedSharedStrings lss = new LocalizedSharedStrings();
        lss.TUTORIAL_CARD_LIBRARY = ss.TUTORIAL_CARD_LIBRARY_FI;
        lss.TUTORIAL_MAIN_MENU = ss.TUTORIAL_MAIN_MENU_FI;
        lss.TUTORIAL_NOTIFICATIONS = ss.TUTORIAL_NOTIFICATIONS_FI;
        lss.TUTORIAL_CAMERA_RESET = ss.TUTORIAL_CAMERA_RESET_FI;
        lss.TUTORIAL_SCENE_MENU = ss.TUTORIAL_SCENE_MENU_FI;

        lss.INFO_WINDOW_HEADER = ss.INFO_WINDOW_HEADER_FI;
        lss.INFO_GAME_DESIGN = ss.INFO_GAME_DESIGN_FI;
        lss.INFO_GRAPHICS = ss.INFO_GRAPHICS_FI;
        lss.INFO_PROGRAMMING = ss.INFO_PROGRAMMING_FI;
        lss.INFO_CONTENT_N_SOUND = ss.INFO_CONTENT_N_SOUND_FI;
        lss.INFO_PRODUCER = ss.INFO_PRODUCER_FI;
        lss.INFO_DIRECTOR = ss.INFO_DIRECTOR_FI;

        lss.SETTINGS_WINDOW_HEADER = ss.SETTINGS_WINDOW_HEADER_FI;
        lss.SETTINGS_VOLUME = ss.SETTINGS_VOLUME_FI;
        lss.SETTINGS_VOLUME_MUSIC = ss.SETTINGS_VOLUME_MUSIC_FI;
        lss.SETTINGS_VOLUME_SFX = ss.SETTINGS_VOLUME_SFX_FI;
        lss.SETTINGS_LANGUAGE = ss.SETTINGS_LANGUAGE_FI;
        lss.SETTINGS_GAME = ss.SETTINGS_GAME_FI;
        lss.SETTINGS_GRAPHICS = ss.SETTINGS_GRAPHICS_FI;
        lss.SETTINGS_GPS_RESET = ss.SETTINGS_GPS_RESET_FI;
        lss.SETTINGS_GAME_RESET = ss.SETTINGS_GAME_RESET_FI;
        return lss;
    }

    static LocalizedSharedStrings LoadSharedEnglish(SharedStrings ss) {
        LocalizedSharedStrings lss = new LocalizedSharedStrings();
        lss.TUTORIAL_CARD_LIBRARY = ss.TUTORIAL_CARD_LIBRARY_EN;
        lss.TUTORIAL_MAIN_MENU = ss.TUTORIAL_MAIN_MENU_EN;
        lss.TUTORIAL_NOTIFICATIONS = ss.TUTORIAL_NOTIFICATIONS_EN;
        lss.TUTORIAL_CAMERA_RESET = ss.TUTORIAL_CAMERA_RESET_EN;
        lss.TUTORIAL_SCENE_MENU = ss.TUTORIAL_SCENE_MENU_EN;

        lss.INFO_WINDOW_HEADER = ss.INFO_WINDOW_HEADER_EN;
        lss.INFO_GAME_DESIGN = ss.INFO_GAME_DESIGN_EN;
        lss.INFO_GRAPHICS = ss.INFO_GRAPHICS_EN;
        lss.INFO_PROGRAMMING = ss.INFO_PROGRAMMING_EN;
        lss.INFO_CONTENT_N_SOUND = ss.INFO_CONTENT_N_SOUND_EN;
        lss.INFO_PRODUCER = ss.INFO_PRODUCER_EN;
        lss.INFO_DIRECTOR = ss.INFO_DIRECTOR_EN;

        lss.SETTINGS_WINDOW_HEADER = ss.SETTINGS_WINDOW_HEADER_EN;
        lss.SETTINGS_VOLUME = ss.SETTINGS_VOLUME_EN;
        lss.SETTINGS_VOLUME_MUSIC = ss.SETTINGS_VOLUME_MUSIC_EN;
        lss.SETTINGS_VOLUME_SFX = ss.SETTINGS_VOLUME_SFX_EN;
        lss.SETTINGS_LANGUAGE = ss.SETTINGS_LANGUAGE_EN;
        lss.SETTINGS_GAME = ss.SETTINGS_GAME_EN;
        lss.SETTINGS_GRAPHICS = ss.SETTINGS_GRAPHICS_EN;
        lss.SETTINGS_GPS_RESET = ss.SETTINGS_GPS_RESET_EN;
        lss.SETTINGS_GAME_RESET = ss.SETTINGS_GAME_RESET_EN;
        return lss;
    }

    static LocalizedSharedStrings LoadSharedSwedish(SharedStrings ss) {
        LocalizedSharedStrings lss = new LocalizedSharedStrings();
        lss.TUTORIAL_CARD_LIBRARY = ss.TUTORIAL_CARD_LIBRARY_SV;
        lss.TUTORIAL_MAIN_MENU = ss.TUTORIAL_MAIN_MENU_SV;
        lss.TUTORIAL_NOTIFICATIONS = ss.TUTORIAL_NOTIFICATIONS_SV;
        lss.TUTORIAL_CAMERA_RESET = ss.TUTORIAL_CAMERA_RESET_SV;
        lss.TUTORIAL_SCENE_MENU = ss.TUTORIAL_SCENE_MENU_SV;

        lss.INFO_WINDOW_HEADER = ss.INFO_WINDOW_HEADER_SV;
        lss.INFO_GAME_DESIGN = ss.INFO_GAME_DESIGN_SV;
        lss.INFO_GRAPHICS = ss.INFO_GRAPHICS_SV;
        lss.INFO_PROGRAMMING = ss.INFO_PROGRAMMING_SV;
        lss.INFO_CONTENT_N_SOUND = ss.INFO_CONTENT_N_SOUND_SV;
        lss.INFO_PRODUCER = ss.INFO_PRODUCER_SV;
        lss.INFO_DIRECTOR = ss.INFO_DIRECTOR_SV;

        lss.SETTINGS_WINDOW_HEADER = ss.SETTINGS_WINDOW_HEADER_SV;
        lss.SETTINGS_VOLUME = ss.SETTINGS_VOLUME_SV;
        lss.SETTINGS_VOLUME_MUSIC = ss.SETTINGS_VOLUME_MUSIC_SV;
        lss.SETTINGS_VOLUME_SFX = ss.SETTINGS_VOLUME_SFX_SV;
        lss.SETTINGS_LANGUAGE = ss.SETTINGS_LANGUAGE_SV;
        lss.SETTINGS_GAME = ss.SETTINGS_GAME_SV;
        lss.SETTINGS_GRAPHICS = ss.SETTINGS_GRAPHICS_SV;
        lss.SETTINGS_GPS_RESET = ss.SETTINGS_GPS_RESET_SV;
        lss.SETTINGS_GAME_RESET = ss.SETTINGS_GAME_RESET_SV;
        return lss;
    }

    // ####################################################
    /* This code is for loading the game scene data 
     * strings                                           */
    // ####################################################

    internal static LocalizedGameSceneStrings LoadLocalizedGameSceneData(Loc loc = Loc.English) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Localization/UI/GameScene");
        GameSceneStrings gameSceneStrings = JsonUtility.FromJson<GameSceneStrings>(jsonFile.text);

        LocalizedGameSceneStrings locGameSceneStrings = new LocalizedGameSceneStrings();

        switch (loc) {
            case Loc.Finnish:
                locGameSceneStrings = LoadGameSceneFinnish(gameSceneStrings);
                break;
            case Loc.English:
                locGameSceneStrings = LoadGameSceneEnglish(gameSceneStrings);
                break;
            case Loc.Swedish:
                locGameSceneStrings = LoadGameSceneSwedish(gameSceneStrings);
                break;
            default:
                break;
        }

        return locGameSceneStrings;
    }

    static LocalizedGameSceneStrings LoadGameSceneFinnish(GameSceneStrings gss) {
        LocalizedGameSceneStrings lgss = new LocalizedGameSceneStrings();
        lgss.TUTORIAL_SAMPLE_NOTIFICATION = gss.TUTORIAL_SAMPLE_NOTIFICATION_FI;
        lgss.TUTORIAL_BUILDING_HEXAGON = gss.TUTORIAL_BUILDING_HEXAGON_FI;
        lgss.TUTORIAL_TELESCOPE_PART = gss.TUTORIAL_TELESCOPE_PART_FI;
        lgss.TUTORIAL_HEX_MENU = gss.TUTORIAL_HEX_MENU_FI;
        lgss.UNLOCK_TELESCOPE = gss.UNLOCK_TELESCOPE_FI;
        lgss.UNLOCK_TELESCOPE_DIRECTIONS = gss.UNLOCK_TELESCOPE_DIRECTIONS_FI;

        lgss.INITIALS_ENTER = gss.INITIALS_ENTER_FI;
        lgss.INITIALS_TYPE_HERE = gss.INITIALS_TYPE_HERE_FI;

        lgss.CAVE_MAP_MATERIALSTORAGE = gss.CAVE_MAP_MATERIALSTORAGE_FI;
        lgss.CAVE_MAP_MIRROR_COATING = gss.CAVE_MAP_MIRROR_COATING_FI;
        lgss.CAVE_MAP_MIRROR_POLISHING = gss.CAVE_MAP_MIRROR_POLISHING_FI;
        lgss.CAVE_MAP_TEST_TUNNEL = gss.CAVE_MAP_TEST_TUNNEL_FI;
        lgss.CAVE_MAP_MEASURING_TUNNEL = gss.CAVE_MAP_MEASURING_TUNNEL_FI;

        return lgss;
    }

    static LocalizedGameSceneStrings LoadGameSceneEnglish(GameSceneStrings gss) {
        LocalizedGameSceneStrings lgss = new LocalizedGameSceneStrings();
        lgss.TUTORIAL_SAMPLE_NOTIFICATION = gss.TUTORIAL_SAMPLE_NOTIFICATION_EN;
        lgss.TUTORIAL_BUILDING_HEXAGON = gss.TUTORIAL_BUILDING_HEXAGON_EN;
        lgss.TUTORIAL_TELESCOPE_PART = gss.TUTORIAL_TELESCOPE_PART_EN;
        lgss.TUTORIAL_HEX_MENU = gss.TUTORIAL_HEX_MENU_EN;

        lgss.INITIALS_ENTER = gss.INITIALS_ENTER_EN;
        lgss.INITIALS_TYPE_HERE = gss.INITIALS_TYPE_HERE_EN;

        lgss.UNLOCK_TELESCOPE = gss.UNLOCK_TELESCOPE_EN;
        lgss.UNLOCK_TELESCOPE_DIRECTIONS = gss.UNLOCK_TELESCOPE_DIRECTIONS_EN;

        lgss.CAVE_MAP_MATERIALSTORAGE = gss.CAVE_MAP_MATERIALSTORAGE_EN;
        lgss.CAVE_MAP_MIRROR_COATING = gss.CAVE_MAP_MIRROR_COATING_EN;
        lgss.CAVE_MAP_MIRROR_POLISHING = gss.CAVE_MAP_MIRROR_POLISHING_EN;
        lgss.CAVE_MAP_TEST_TUNNEL = gss.CAVE_MAP_TEST_TUNNEL_EN;
        lgss.CAVE_MAP_MEASURING_TUNNEL = gss.CAVE_MAP_MEASURING_TUNNEL_EN;

        return lgss;
    }

    static LocalizedGameSceneStrings LoadGameSceneSwedish(GameSceneStrings gss) {
        LocalizedGameSceneStrings lgss = new LocalizedGameSceneStrings();
        lgss.TUTORIAL_SAMPLE_NOTIFICATION = gss.TUTORIAL_SAMPLE_NOTIFICATION_SV;
        lgss.TUTORIAL_BUILDING_HEXAGON = gss.TUTORIAL_BUILDING_HEXAGON_SV;
        lgss.TUTORIAL_TELESCOPE_PART = gss.TUTORIAL_TELESCOPE_PART_SV;
        lgss.TUTORIAL_HEX_MENU = gss.TUTORIAL_HEX_MENU_SV;

        lgss.INITIALS_ENTER = gss.INITIALS_ENTER_SV;
        lgss.INITIALS_TYPE_HERE = gss.INITIALS_TYPE_HERE_SV;

        lgss.UNLOCK_TELESCOPE = gss.UNLOCK_TELESCOPE_SV;
        lgss.UNLOCK_TELESCOPE_DIRECTIONS = gss.UNLOCK_TELESCOPE_DIRECTIONS_SV;

        lgss.CAVE_MAP_MATERIALSTORAGE = gss.CAVE_MAP_MATERIALSTORAGE_SV;
        lgss.CAVE_MAP_MIRROR_COATING = gss.CAVE_MAP_MIRROR_COATING_SV;
        lgss.CAVE_MAP_MIRROR_POLISHING = gss.CAVE_MAP_MIRROR_POLISHING_SV;
        lgss.CAVE_MAP_TEST_TUNNEL = gss.CAVE_MAP_TEST_TUNNEL_SV;
        lgss.CAVE_MAP_MEASURING_TUNNEL = gss.CAVE_MAP_MEASURING_TUNNEL_SV;

        return lgss;
    }

    // ####################################################
    /* This code is for loading the telescope scene data 
     * strings                                           */
    // ####################################################

    internal static LocalizedTelescopeStrings LoadLocalizedTelescopeData(Loc loc = Loc.English) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Localization/UI/Telescope_game");
        TelescopeStrings telescopeStrings = JsonUtility.FromJson<TelescopeStrings>(jsonFile.text);

        LocalizedTelescopeStrings locTelescopeStrings = new LocalizedTelescopeStrings();

        switch (loc) {
            case Loc.Finnish:
                locTelescopeStrings = LoadTelescopeFinnish(telescopeStrings);
                break;
            case Loc.English:
                locTelescopeStrings = LoadTelescopeEnglish(telescopeStrings);
                break;
            case Loc.Swedish:
                locTelescopeStrings = LoadTelescopeSwedish(telescopeStrings);
                break;
            default:
                break;
        }

        return locTelescopeStrings;
    }

    static LocalizedTelescopeStrings LoadTelescopeFinnish(TelescopeStrings ts) {
        LocalizedTelescopeStrings lts = new LocalizedTelescopeStrings();
        lts.TUTORIAL_SAMPLE_NOTIFICATION = ts.TUTORIAL_SAMPLE_NOTIFICATION_FI;
        lts.TUTORIAL_NOTIFICATION_EXPLANATION = ts.TUTORIAL_NOTIFICATION_EXPLANATION_FI;
        lts.TUTORIAL_DRAG_PART_EXPLANATION = ts.TUTORIAL_DRAG_PART_EXPLANATION_FI;
        lts.UNLOCK_SOLAR_SYSTEM = ts.UNLOCK_SOLAR_SYSTEM_FI;
        lts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS = ts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS_FI;

        return lts;
    }

    static LocalizedTelescopeStrings LoadTelescopeEnglish(TelescopeStrings ts) {
        LocalizedTelescopeStrings lts = new LocalizedTelescopeStrings();
        lts.TUTORIAL_SAMPLE_NOTIFICATION = ts.TUTORIAL_SAMPLE_NOTIFICATION_EN;
        lts.TUTORIAL_NOTIFICATION_EXPLANATION = ts.TUTORIAL_NOTIFICATION_EXPLANATION_EN;
        lts.TUTORIAL_DRAG_PART_EXPLANATION = ts.TUTORIAL_DRAG_PART_EXPLANATION_EN;
        lts.UNLOCK_SOLAR_SYSTEM = ts.UNLOCK_SOLAR_SYSTEM_EN;
        lts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS = ts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS_EN;

        return lts;
    }

    static LocalizedTelescopeStrings LoadTelescopeSwedish(TelescopeStrings ts) {
        LocalizedTelescopeStrings lts = new LocalizedTelescopeStrings();
        lts.TUTORIAL_SAMPLE_NOTIFICATION = ts.TUTORIAL_SAMPLE_NOTIFICATION_SV;
        lts.TUTORIAL_NOTIFICATION_EXPLANATION = ts.TUTORIAL_NOTIFICATION_EXPLANATION_SV;
        lts.TUTORIAL_DRAG_PART_EXPLANATION = ts.TUTORIAL_DRAG_PART_EXPLANATION_SV;
        lts.UNLOCK_SOLAR_SYSTEM = ts.UNLOCK_SOLAR_SYSTEM_SV;
        lts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS = ts.UNLOCK_SOLAR_SYSTEM_DIRECTIONS_SV;

        return lts;
    }

    internal static LocalizedSolarStrings LoadLocalizedSolarData(Loc loc = Loc.English) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Localization/UI/Solar_system");
        SolarStrings solarStrings = JsonUtility.FromJson<SolarStrings>(jsonFile.text);

        LocalizedSolarStrings locSolarStrings = new LocalizedSolarStrings();

        switch (loc) {
            case Loc.Finnish:
                locSolarStrings = LoadSolarFinnish(solarStrings);
                break;
            case Loc.English:
                locSolarStrings = LoadSolarEnglish(solarStrings);
                break;
            case Loc.Swedish:
                locSolarStrings = LoadSolarSwedish(solarStrings);
                break;
            default:
                break;
        }

        return locSolarStrings;
    }

    static LocalizedSolarStrings LoadSolarFinnish(SolarStrings ss) {
        LocalizedSolarStrings lss = new LocalizedSolarStrings();
        lss.TUTORIAL_MOON_BROWSER = ss.TUTORIAL_MOON_BROWSER_FI;
        lss.TUTORIAL_PLANET_BROWSER = ss.TUTORIAL_PLANET_BROWSER_FI;
        lss.TUTORIAL_TIME_CONTROLS = ss.TUTORIAL_TIME_CONTROLS_FI;
        lss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS = ss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_FI;

        return lss;
    }

    static LocalizedSolarStrings LoadSolarEnglish(SolarStrings ss) {
        LocalizedSolarStrings lss = new LocalizedSolarStrings();
        lss.TUTORIAL_MOON_BROWSER = ss.TUTORIAL_MOON_BROWSER_EN;
        lss.TUTORIAL_PLANET_BROWSER = ss.TUTORIAL_PLANET_BROWSER_EN;
        lss.TUTORIAL_TIME_CONTROLS = ss.TUTORIAL_TIME_CONTROLS_EN;
        lss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS = ss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_EN;

        return lss;
    }

    static LocalizedSolarStrings LoadSolarSwedish(SolarStrings ss) {
        LocalizedSolarStrings lss = new LocalizedSolarStrings();
        lss.TUTORIAL_MOON_BROWSER = ss.TUTORIAL_MOON_BROWSER_SV;
        lss.TUTORIAL_PLANET_BROWSER = ss.TUTORIAL_PLANET_BROWSER_SV;
        lss.TUTORIAL_TIME_CONTROLS = ss.TUTORIAL_TIME_CONTROLS_SV;
        lss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS = ss.TUTORIAL_SOLAR_BROWSER_INSTRUCTIONS_SV;

        return lss;
    }

    internal static LocalizedQuizStrings LoadLocalizedQuizData(Loc loc = Loc.English) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Localization/UI/Quiz");
        QuizStrings quizStrings = JsonUtility.FromJson<QuizStrings>(jsonFile.text);

        LocalizedQuizStrings locQuizStrings = new LocalizedQuizStrings();

        switch (loc) {
            case Loc.Finnish:
                locQuizStrings = LoadQuizFinnish(quizStrings);
                break;
            case Loc.English:
                locQuizStrings = LoadQuizEnglish(quizStrings);
                break;
            case Loc.Swedish:
                locQuizStrings = LoadQuizSwedish(quizStrings);
                break;
            default:
                break;
        }

        return locQuizStrings;
    }

    static LocalizedQuizStrings LoadQuizFinnish(QuizStrings qs) {
        LocalizedQuizStrings lqs = new LocalizedQuizStrings();

        lqs.QUESTION_ANSWER_CORRECT = qs.QUESTION_ANSWER_CORRECT_FI;
        lqs.QUESTION_ANSWER_INCORRECT = qs.QUESTION_ANSWER_INCORRECT_FI;

        lqs.FINISH_SCREEN_CONGRATULATIONS = qs.FINISH_SCREEN_CONGRATULATIONS_FI;
        lqs.FINISH_SCREEN_START = qs.FINISH_SCREEN_START_FI;
        lqs.FINISH_SCREEN_REST = qs.FINISH_SCREEN_REST_FI;

        return lqs;
    }

    static LocalizedQuizStrings LoadQuizEnglish(QuizStrings qs) {
        LocalizedQuizStrings lqs = new LocalizedQuizStrings();


        lqs.QUESTION_ANSWER_CORRECT = qs.QUESTION_ANSWER_CORRECT_EN;
        lqs.QUESTION_ANSWER_INCORRECT = qs.QUESTION_ANSWER_INCORRECT_EN;

        lqs.FINISH_SCREEN_CONGRATULATIONS = qs.FINISH_SCREEN_CONGRATULATIONS_EN;
        lqs.FINISH_SCREEN_START = qs.FINISH_SCREEN_START_EN;
        lqs.FINISH_SCREEN_REST = qs.FINISH_SCREEN_REST_EN;



        return lqs;
    }

    static LocalizedQuizStrings LoadQuizSwedish(QuizStrings qs) {
        LocalizedQuizStrings lqs = new LocalizedQuizStrings();


        lqs.QUESTION_ANSWER_CORRECT = qs.QUESTION_ANSWER_CORRECT_SV;
        lqs.QUESTION_ANSWER_INCORRECT = qs.QUESTION_ANSWER_INCORRECT_SV;

        lqs.FINISH_SCREEN_CONGRATULATIONS = qs.FINISH_SCREEN_CONGRATULATIONS_SV;
        lqs.FINISH_SCREEN_START = qs.FINISH_SCREEN_START_SV;
        lqs.FINISH_SCREEN_REST = qs.FINISH_SCREEN_REST_SV;



        return lqs;
    }
}


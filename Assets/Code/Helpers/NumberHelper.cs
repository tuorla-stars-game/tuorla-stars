﻿using UnityEngine;

public static class NumberHelper {
    internal static MapBoundaries TuorlaMapBoundaries = new MapBoundaries(
                                                            60.417855f,
                                                            60.4140245f,
                                                            22.4409515f,
                                                            22.4488725f);
}

internal struct MapBoundaries {
    internal float topBoundary, bottomBoundary, leftBoundary, rightBoundary;

    internal MapBoundaries(float top, float bottom, float left, float right) {
        topBoundary = top;
        bottomBoundary = bottom;
        leftBoundary = left;
        rightBoundary = right;
    }

    internal MapBoundaries(Vector2 topLeft, Vector2 bottomRight) {
        topBoundary = topLeft.y;
        leftBoundary = topLeft.x;
        bottomBoundary = bottomRight.y;
        rightBoundary = bottomRight.x;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal static class StateNotifsHelper  {
    internal static StateNotifications LoadNotifData(string name, Scenes scene = Scenes.Telescope, Loc loc = Loc.English) {
        string folder = "";
        switch (scene) {
            case Scenes.Telescope:
                folder = "Telescope";
                break;
            case Scenes.Solar:
                folder = "Solar";
                break;
            default:
                break;
        }

        TextAsset jsonFile = Resources.Load<TextAsset>("Content/" + folder + "/Notifications/json/" + name);
        StateNotificationsRaw notifTextsRaw = JsonUtility.FromJson<StateNotificationsRaw>(jsonFile.text);

        StateNotifications notifTexts = new StateNotifications();

        switch (loc) {
            case Loc.Finnish:
                notifTexts.STATE_ENTRY = notifTextsRaw.STATE_ENTRY_FI;
                notifTexts.STATE_PROGRESS = notifTextsRaw.STATE_PROGRESS_FI;
                notifTexts.STATE_EXIT = notifTextsRaw.STATE_EXIT_FI;
                break;
            case Loc.English:
                notifTexts.STATE_ENTRY = notifTextsRaw.STATE_ENTRY_EN;
                notifTexts.STATE_PROGRESS = notifTextsRaw.STATE_PROGRESS_EN;
                notifTexts.STATE_EXIT = notifTextsRaw.STATE_EXIT_EN;
                break;
            case Loc.Swedish:
                notifTexts.STATE_ENTRY = notifTextsRaw.STATE_ENTRY_EN;
                notifTexts.STATE_PROGRESS = notifTextsRaw.STATE_PROGRESS_EN;
                notifTexts.STATE_EXIT = notifTextsRaw.STATE_EXIT_EN;
                break;
            default:
                break;
        }

        return notifTexts;
    }
}

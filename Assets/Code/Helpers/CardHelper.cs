﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CardHelper {
    internal static int CardNumberToIndex(int number, int indexMax) {
        int index = indexMax - 2 * Mathf.Abs(number);
        return index;
    }

    internal static int IndexToCardNumber(int index, int indexMax) {
        int cardNumber = indexMax / 2;
        cardNumber -= index / 2;

        if (index % 2 == 0) {
            cardNumber = -cardNumber;
        }

        return cardNumber;
    }

    internal static float CalculateCardGap(float width, int indexMax, float multiplier) {
        float gap = width / (multiplier * indexMax);
        return gap;
    }

    internal static T[] loadMetadataCards<T>(string name) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Cards/json/" + name);
        T[] CardMetadataObject = JSONHelper.getJsonArray<T>(jsonFile.text);
        return CardMetadataObject;
    }

    internal static T[] loadData<T>(string name) {
        TextAsset jsonFile = Resources.Load<TextAsset>("Content/Cards/json/" + name + "_loc");
        T[] cards = JSONHelper.getJsonArray<T>(jsonFile.text);
        return cards;
    }
}
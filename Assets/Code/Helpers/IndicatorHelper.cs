﻿using UnityEngine;

internal static class IndicatorHelper {

    internal static Vector3 CalculatePosOnGround(Vector3 pos) {
        RaycastHit rayHit;

        if (Physics.Raycast(pos, Vector3.down, out rayHit, 50f, 1 << LayerMask.NameToLayer("ground"))) {
            return rayHit.point + Vector3.up * 0.025f;
        }

        return pos;
    }
}

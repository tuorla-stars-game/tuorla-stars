﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class InputHelper {

    internal static float TouchTwoDeltaDeltaCalc(Touch touch1, Touch touch2) {
        Vector2 touch1Prev = touch1.position - touch1.deltaPosition;
        Vector2 touch2Prev = touch2.position - touch2.deltaPosition;

        float prevTouchDelta = (touch1Prev - touch2Prev).magnitude;
        float touchDelta = (touch1.position - touch2.position).magnitude;

        float deltaDeltaTouch = prevTouchDelta - touchDelta;

        return deltaDeltaTouch;
    }

    internal static bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        return results.Count > 0;
    }
}

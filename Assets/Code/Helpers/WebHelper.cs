﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

internal static class WebHelper {
    const string getLocUri = "http://tigsune.eu:8080/api/players";
    const string putLocUri = "http://tigsune.eu:8080/api/players/";
    const string postLocUri = "http://tigsune.eu:8080/api/players";

    const string putMsgUri = "http://tigsune.eu:8080/api/messages";
    const string getMsgUri = "http://tigsune.eu:8080/api/messages";

    const string authUri = "http://tigsune.eu:8080/api/auth/";

    internal delegate void AfterPost(string id);
    internal static event AfterPost OnPostReady;

    internal delegate void AfterPut();
    internal static event AfterPut OnPutReady;

    internal delegate void AfterMsgPut();
    internal static event AfterMsgPut OnMsgPutReady;

    internal delegate void AfterGet(PlayerData[] players);
    internal static event AfterGet OnGetReady;

    internal delegate void AfterMsgGet(MsgData[] messages);
    internal static event AfterMsgGet OnMsgGetReady;

    internal delegate void AfterAuth(bool authenticated);
    internal static event AfterAuth OnAuthReady;

    static UnityWebRequest SetUpWebRequest(string uri, string method, string data, string contentType) {
        UnityWebRequest request = new UnityWebRequest();
        request.method = method;
        request.url = uri;

        byte[] bytes = Encoding.UTF8.GetBytes(data);
        UploadHandler uH = new UploadHandlerRaw(bytes);
        uH.contentType = contentType;
        request.uploadHandler = uH;

        DownloadHandlerBuffer dH = new DownloadHandlerBuffer();
        request.downloadHandler = dH;

        return request;
    }

    internal static IEnumerator GetPlayerDataFromServer() {
        NotificationsController notifs = GameObject.FindGameObjectWithTag("Notifications")
                                                   .GetComponent<NotificationsController>();

        using (UnityWebRequest www = UnityWebRequest.Get(getLocUri)) {
            yield return www.Send();

            if (www.isError) {
                //notifs.AddNotification(www.error);
            }
            else {
                string response = Encoding.UTF8.GetString(www.downloadHandler.data);
                PlayerData[] players = JSONHelper.getJsonArray<PlayerData>(response);

                if (OnGetReady != null) {
                    OnGetReady(players);
                }
            }
        }
    }

    internal static IEnumerator PutLocData(string id, float longitude, float latitude, bool isTeacher, string initials) {
        string data = "{\"longitude\": " + longitude +
                     ", \"latitude\": " + latitude +
                     ", \"isTeacher\": " + isTeacher.ToString().ToLower() +
                     ", \"initials\": \"" + initials + 
                    "\" }";

        using (UnityWebRequest www = UnityWebRequest.Put(putLocUri + id, data)) {
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.Send();

            if (www.isError) {
                // error handling
            }
            else {
                if (OnPutReady != null) {
                    OnPutReady();
                }
            }
        }
    }

    internal static IEnumerator PostLocData(string name, float longitude, float latitude, string initials, bool isTeacher) {
        string data = "{ \"name\": \"" + name +
                    "\", \"longitude\": " + longitude +
                      ", \"latitude\": " + latitude +
                      ", \"initials\": \"" + initials +
                    "\", \"isTeacher\": " + isTeacher.ToString().ToLower() +
                       " }";

        using (UnityWebRequest www = SetUpWebRequest(postLocUri, "POST", data, "application/json")) {

            yield return www.Send();

            if (www.isError) {
                // error handling
            }
            else {
                string response = Encoding.UTF8.GetString(www.downloadHandler.data);
                response = response.Trim(new char[] { '"' });

                if (OnPostReady != null) {
                    OnPostReady(response);
                }
            }
        }
    }

    internal static IEnumerator GetMsgData() {
        NotificationsController notifs = GameObject.FindGameObjectWithTag("Notifications")
                                                   .GetComponent<NotificationsController>();

        using (UnityWebRequest www = UnityWebRequest.Get(getMsgUri)) {
            yield return www.Send();

            if (www.isError) {
                //notifs.AddNotification(www.error);
            }
            else {
                string response = Encoding.UTF8.GetString(www.downloadHandler.data);
                //Debug.Log(response);
                MsgData[] messages = JSONHelper.getJsonArray<MsgData>(response);
                //Debug.Log(messages);
                if (OnMsgGetReady != null) {
                    OnMsgGetReady(messages);
                }
            }
        }
    }

    internal static IEnumerator PutMsgData(string msg, string ID) {
        string data = "{\"msgString\": \"" + msg +
                   "\", \"senderID\": \"" + ID +
                    "\" }";

        using (UnityWebRequest www = UnityWebRequest.Put(putMsgUri, data)) {
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.Send();

            if (www.isError) {
                // error handling
            }
            else {
                if (OnMsgPutReady != null) {
                    OnMsgPutReady();
                }
            }
        }
    }

    internal static IEnumerator AuthenticateUser(string hash) {
        using (UnityWebRequest www = UnityWebRequest.Get(authUri + hash)) {
            yield return www.Send();

            bool authed = false;

            if (www.isError) {
                Debug.Log("Error");
            }
            else {
                if (www.responseCode == 200) {
                    authed = true;
                }
            }

            if (OnAuthReady != null) {
                OnAuthReady(authed);
            }
        }

    }
}

[System.Serializable]
public struct PlayerData {
    public string _id;
    public float[] loc;
    public string name;
    public string initials;
    public bool isTeacher;
    public string __v;
}

[System.Serializable]
public struct MsgData {
    public string msgString;
    public string senderID;
    public float time;
}